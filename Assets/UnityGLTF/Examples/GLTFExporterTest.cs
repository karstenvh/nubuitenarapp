﻿using System.IO;
using UnityEngine;

namespace UnityGLTF.Examples
{
	public class GLTFExporterTest : MonoBehaviour
	{
        public GLTFComponent gltfToWaitFor;
        public string fileName = "TestScene";

        public string RetrieveTexturePath(UnityEngine.Texture texture)
		{
			return texture.name;
		}

        private void Awake()
        {
            if (enabled)
            {
                if (gltfToWaitFor != null)
                {
                    gltfToWaitFor.onLoaded += Save;
                }
                else
                {
                    Save(null);
                }
            }
        }

        private void Update()
        {
            
        }

        // Use this for initialization
        public void Save(GameObject go)
		{
            MeshFilter[] filters = FindObjectsOfType<MeshFilter>();
            int verts = 0;

            for (int i = 0; i < filters.Length; i++)
            {
                verts += filters[i].mesh.vertexCount;
            }

            Debug.Log(verts);

			var exporter = new GLTFSceneExporter(new[] {transform}, RetrieveTexturePath);
			var appPath = Application.dataPath;
			var wwwPath = appPath.Substring(0, appPath.LastIndexOf("Assets")) + "www";
			exporter.SaveGLTF(Path.Combine(wwwPath, "TestScene"), fileName);
		}
	}
}
