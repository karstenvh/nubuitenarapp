using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using GLTF;
using GLTF.Schema;
using UnityEngine;
using UnityGLTF.Loader;

namespace UnityGLTF
{

    /// <summary>
    /// Component to load a GLTF scene with
    /// </summary>
    public class GLTFComponent : MonoBehaviour
    {
        public string GLTFUri = null;
        public bool isLocalUri = true;
        public bool Multithreaded = true;
        public bool UseStream = false;

        [SerializeField]
        private bool loadOnStart = true;

        public int MaximumLod = 300;
        public GLTFSceneImporter.ColliderType Collider = GLTFSceneImporter.ColliderType.None;

        [SerializeField]
        private Shader shaderOverride = null;
		int i = 0;
		bool running;

        [SerializeField]
        private bool disableOnLoad = true;

		public Action<GameObject> onLoaded;

		private Stack<IEnumerator> ienumeratorStack;

		IEnumerator Start()
        {
            if (loadOnStart)
            {
				running = true;
                if (isLocalUri)
                {
                    var e = Load();
                    ienumeratorStack = new Stack<IEnumerator>();
                    ienumeratorStack.Push(e);
                    StartCoroutine(RunStack());
                    //RunIEnumerator(e);
                    //Debug.Log(i);
                }
                else
                {
                    yield return Load();
                }
			}

			yield return 0;
        }

		private void Update()
		{
			if (!running)
			{

			}
		}

		private IEnumerator RunStack()
		{
			bool webrequested = false;
			while (ienumeratorStack.Count > 0)
			{
				var e = ienumeratorStack.Peek();

				while (e.MoveNext())
				{
					/*if (e.Current != null)
					{
						Debug.Log(e.Current.GetType());
					}*/

					if (!webrequested && e.Current != null && e.Current.GetType().ToString().Contains("UnityWebRequestAsyncOperation"))
					{
						yield return e.Current;// new WaitForSeconds(1);
						webrequested = true;
					}

					if (e.Current is IEnumerator)
					{
						IEnumerator tmp = (IEnumerator)e.Current;
						ienumeratorStack.Push(tmp);
						e = tmp;
					}

				}

				ienumeratorStack.Pop();
			}

			yield return 0;
		}

		public void RunIEnumerator(IEnumerator e)
		{
			while (e.MoveNext())
			{
				i++;
				//Debug.Log(e.Current.GetType());

				//if (e.Current.GetType().ToString().ToLower().Contains("load"))
				{
				}

				if (e.Current is IEnumerator)
				{
					RunIEnumerator((IEnumerator)e.Current);
				}
			}
		}

        public IEnumerator Load()
        {
            GLTFSceneImporter sceneImporter = null;
            ILoader loader = null;

            if (UseStream)
            {
                // Path.Combine treats paths that start with the separator character
                // as absolute paths, ignoring the first path passed in. This removes
                // that character to properly handle a filename written with it.
                GLTFUri = GLTFUri.TrimStart(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
                string fullPath = Path.Combine(Application.streamingAssetsPath, GLTFUri);
                string directoryPath = URIHelper.GetDirectoryName(fullPath);
                loader = new FileLoader(directoryPath);
                sceneImporter = new GLTFSceneImporter(
                    Path.GetFileName(GLTFUri),
                    loader
                    );
            }
            else
            {
				Debug.Log (File.Exists (GLTFUri) + ": " + GLTFUri);

                string directoryPath;
				//#if !UNITY_IOS
                if (isLocalUri)
                {
                    directoryPath = Path.GetDirectoryName(GLTFUri);
                }
                else
				//#endif
                {
                    directoryPath = URIHelper.GetDirectoryName(GLTFUri);
                }

				loader = new FileLoader(directoryPath);

                string fileName;
			//	#if !UNITY_IOS
                if (isLocalUri)
                {
                    fileName = Path.GetFileName(GLTFUri);
                }
                else
				//#endif
                {
                    fileName = URIHelper.GetFileFromUri(new Uri(GLTFUri));
                }

                sceneImporter = new GLTFSceneImporter(
                    fileName,
                    loader
                    );

            }

            sceneImporter.SceneParent = gameObject.transform;
            sceneImporter.Collider = Collider;
            sceneImporter.MaximumLod = MaximumLod;
            sceneImporter.CustomShaderName = shaderOverride ? shaderOverride.name : null;
            yield return sceneImporter.LoadScene(-1, Multithreaded, onLoaded);

			GameObject[] objs = FindObjectsOfType<GameObject>();
			Debug.Log("GO tally: " + objs.Length);
            MeshFilter[] filters = FindObjectsOfType<MeshFilter>();
            int verts = 0;

            for (int i = 0; i < filters.Length; i++)
            {
                verts += filters[i].mesh.vertexCount;
            }

            Debug.Log("verts: " + verts);
            // Override the shaders on all materials if a shader is provided
            if (shaderOverride != null)
            {
                Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer renderer in renderers)
                {
                    renderer.sharedMaterial.shader = shaderOverride;
                }
            }

            MeshRenderer[] rends = FindObjectsOfType<MeshRenderer>();
            for (int i = 0; i < rends.Length; i++)
            {
                rends[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }

            if (disableOnLoad)
            {
                enabled = false;
            }
        }
    }
}
