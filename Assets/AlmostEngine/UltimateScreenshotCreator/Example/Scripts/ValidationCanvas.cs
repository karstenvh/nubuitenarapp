﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using AlmostEngine.Screenshot;

namespace AlmostEngine.Examples
{
	/// <summary>
	/// Use this script to display a validation canvas before to export the screenshot textures.
	/// </summary>
	public class ValidationCanvas : MonoBehaviour
	{
		ScreenshotManager m_ScreenshotManager;
		public Canvas m_Canvas;
		public RectTransform m_ImageContainer;
		public Image m_Texture;

		void Awake ()
		{
			m_ScreenshotManager = GameObject.FindObjectOfType<ScreenshotManager> ();
		}

		/// <summary>
		/// Call this method to start a screenshot capture process and display the validation canvas when the capture is completed.
		/// </summary>
		public void Capture ()
		{
		    // Start listening to end capture event
		    ScreenshotManager.onCaptureEndDelegate += OnCaptureEndDelegate;

		    // Call update to only capture the texture without exporting
		    m_ScreenshotManager.UpdateAll ();
		}

		#region Event callbacks

		public void OnCaptureEndDelegate ()
	    {
			// Stop listening the callback
			ScreenshotManager.onCaptureEndDelegate -= OnCaptureEndDelegate;

            // Update the texture image
            Texture2D texture = m_ScreenshotManager.m_Config.GetFirstActiveResolution().m_Texture;
            m_Texture.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);

			// Show canvas
			m_Canvas.enabled = true;
		}

		#endregion

		#region UI callbacks

		public void OnDiscardCallback ()
		{
			// Hide canvas
			m_Canvas.enabled = false;
		}

		public void OnSaveCallback ()
		{
			// Export the screenshots to files
			m_ScreenshotManager.m_Config.ExportAllToFiles ();

			// Hide canvas
			m_Canvas.enabled = false;
		}

		#endregion
	}
}
