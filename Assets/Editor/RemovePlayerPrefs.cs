﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class RemovePlayerPrefs {
    [MenuItem("VROwl/Remove Player Prefs")]
    private static void _RemovePlayerPrefs() {
        if (PlayerPrefs.HasKey("User")) {
            PlayerPrefs.DeleteAll();
        }
    }
}
