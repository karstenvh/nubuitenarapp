﻿/*
 * Copyright VR Owl 2017
 * Author: Joey Deiman
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Centers a GameObject using the average of the centers of the bounds of all meshes. Works using right-click context menu in the hierarchy window.
/// Also resets its rotation to 0, so some collider primitives attached to the GameObject might need to be rotated manually.
/// Useful when you have meshes with their root at (0, 0, 0).
/// </summary>
public class SetParentToCenter : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("GameObject/Center To Children/Trans+Rot", false, 47)]
    private static void CenterTR(MenuCommand command)
    {
        SetCenter((GameObject)command.context, true, true);
    }

    [MenuItem("GameObject/Center To Children/Trans", false, 47)]
    private static void CenterT(MenuCommand command)
    {
        SetCenter((GameObject)command.context, true, false);
    }
#endif

    public static void SetCenter(GameObject go, bool translate, bool rotate)
    {
        // Calculate world position center of all child mesh renderers
        MeshRenderer[] renderers = go.GetComponentsInChildren<MeshRenderer>();

        if (renderers == null || renderers.Length == 0)
        {
            Debug.Log("No MeshRenderers in children");
            return;
        }

        //Vector3 cumWorld = Vector3.zero;
        Bounds total = new Bounds();

        for (int i = 0; i < renderers.Length; i++)
        {
            // Note that MeshRenderer.bounds is in world space already. Mesh.bounds would be in local space however.
            //Vector3 boundsWorld = /*renderers[i].transform.localToWorldMatrix */ renderers[i].bounds.center;
            //cumWorld += boundsWorld;

            total.Encapsulate(renderers[i].bounds);
        }

        // Take the average center in world space
        // cumWorld = /= renderers.Length;

        Vector3 cumWorld = total.center;
        Debug.DrawLine(Vector3.zero, cumWorld, Color.green, 10);
        Debug.DrawLine(Vector3.zero, cumWorld, Color.green, 10);


        // Now we need to know the position of the center in local space of the selected Transform's parent. 
        /* Vector3 cumLocal;
         if (go.transform.parent != null)
         {
             cumLocal = go.transform.parent.worldToLocalMatrix * cumWorld;
         }
         else
         {
             // No parent, world == local.
             cumLocal = cumWorld;
         }

         Debug.DrawLine(Vector3.zero, go.transform.parent.localToWorldMatrix * cumLocal, Color.red, 10);*/


        // Cache children when we unparent them.
        List<Transform> children = new List<Transform>(go.transform.childCount);

        // Temporarily unparent the children.
        for (int i = 0; i < go.transform.childCount; i++)
        {
            Transform t = go.transform.GetChild(i);
            children.Add(t);
#if UNITY_EDITOR
            Undo.RecordObject(t, "Recentered Transform To Children");
#endif
            t.SetParent(null, true);
            i--;
        }

        // Cache the change in position to apply to any colliders.
        Vector3 posDiff = go.transform.position - cumWorld;
        Vector3 posDiffLocal = Vector3.Scale(go.transform.localScale, posDiff);
        Quaternion rotDiff = go.transform.localRotation; // We assume that we rotate towards Quaternion.identity, so the rotation difference equals the original rotation.

#if UNITY_EDITOR
        Undo.RecordObject(go.transform, "Recentered Transform To Children");
#endif

        if (translate)
        {
            go.transform.position = cumWorld;
        }

        if (rotate)
        {
            go.transform.rotation = Quaternion.identity;
        }

        // Re-parent children
        for (int i = 0; i < children.Count; i++)
        {
            children[i].SetParent(go.transform, true);
        }

        // Apply transformation to any attached non-child colliders where applicable.
        Collider[] colliders = go.GetComponents<Collider>();
        for (int i = 0; i < colliders.Length; i++)
        {
            Collider c = colliders[i];

#if UNITY_EDITOR
            Undo.RecordObject(c, "Recentered Transform To Children");
#endif

            ReorientCollider(c, go.transform.localScale, posDiffLocal, translate, rotDiff, rotate);
        }
    }

    private static void ReorientCollider(Collider c, Vector3 scale, Vector3 posDiff, bool translate, Quaternion rotDiff, bool rotate)
    {
        if (c is BoxCollider)
        {
            BoxCollider b = (BoxCollider)c;
            b.center += posDiff; //RotateAndTranslateVector(b.center, posDiff, scale, translate, rotDiff, rotate);
        }
        else if (c is SphereCollider)
        {
            SphereCollider s = (SphereCollider)c;
            s.center += posDiff;
        }
        else if (c is CapsuleCollider)
        {
            CapsuleCollider cps = (CapsuleCollider)c;
            cps.center += posDiff;
        }
        else if (c is WheelCollider)
        {
            WheelCollider w = (WheelCollider)c;
            w.center += posDiff;
        }
    }
    /*
    private static Vector3 RotateAndTranslateVector(Vector3 v, Vector3 scale, Vector3 trans, bool translate, Quaternion rot, bool rotate)
    {
        Vector3 result = v;
        if (rotate)
        {
            result = rot * result;
        }

        if (translate)
        {
            float xScale = 1.0f / (scale.x);// * scale.x);
            float yScale = 1.0f / (scale.y);// * scale.y);
            float zScale = 1.0f / (scale.z);// * scale.z);
            Vector3 newScale = new Vector3(xScale, yScale, zScale);
            //trans.Scale(newScale);
            result += trans;
        }

        return result;
    }*/
}