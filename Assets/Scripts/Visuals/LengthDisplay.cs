﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LengthDisplay : MonoBehaviour
{
    [SerializeField]
    private Text text;
	
	// Update is called once per frame
	void Update ()
    {
        text.text = transform.localScale.z.ToString();
        text.canvas.transform.LookAt(Camera.main.transform);
	}
}
