﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRenderQueue : MonoBehaviour {
    public int queue = 2002;
	void Start ()
    {
        var renders = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renders)
        {
            r.material.renderQueue = queue;
        }
    }
}
