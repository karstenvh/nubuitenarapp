﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTiles : MonoBehaviour {

    public FallingTile tilePrefab;

    public FallingTile.Settings tileSettings;

    public int xCount;
    public int yCount;
    public float delayBetweenTiles;

    public AnimationCurve pulseCurve;

    [HideInInspector]
    public Transform transToFollow;

    private float timeSinceLastTile;

    private Queue<Vector2> tileQueue;

    private List<FallingTile> tiles;

	// Use this for initialization
	IEnumerator Start ()
    {
        BuildQueue();

        tiles = new List<FallingTile>(xCount * yCount);

        while (tileQueue.Count > 0)
        {
            float timeLeft = Time.deltaTime;
            while (timeLeft > 0 && tileQueue.Count > 0)
            {
                timeLeft -= delayBetweenTiles;
                Vector2 p = tileQueue.Dequeue();
                FallingTile tile = Instantiate(tilePrefab, new Vector3(p.x, 0, p.y), Quaternion.identity);
                tile.transform.SetParent(transform, false);
                tile.Init(tileSettings);
                tiles.Add(tile);
            }

            yield return 0;
        }

        yield return new WaitForSeconds(tileSettings.curve.keys[tileSettings.curve.length - 1].time);

        StartCoroutine(FinalPulse());

        /*
        for (int x = 0; x < xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                FallingTile tile = Instantiate(tilePrefab, new Vector3(x - xCount/2f, 0, y - yCount/2f), Quaternion.identity);
                tile.transform.SetParent(transform, false);
                tile.Init(tileSettings);
                yield return new WaitForSeconds(delayBetweenTiles);
            }
        }*/
    }

    private void BuildQueue()
    {
        List<Vector2> allPositions = new List<Vector2>(xCount * yCount);
        tileQueue = new Queue<Vector2>(allPositions.Count);

        for (int x = 0; x < xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                allPositions.Add(new Vector2(x - xCount/2f, y - yCount/2f));
            }
        }

        while (allPositions.Count > 0)
        {
            int randomIndex = Random.Range(0, allPositions.Count);
            tileQueue.Enqueue(allPositions[randomIndex]);
            allPositions.RemoveAt(randomIndex);
        }
    }

    private IEnumerator FinalPulse()
    {
        float timer = 0;

        while (timer < pulseCurve.keys[pulseCurve.keys.Length - 1].time)
        {
            timer += Time.deltaTime;

            float eval = pulseCurve.Evaluate(timer);
            for (int i = 0; i < tiles.Count; i++)
            {
                for (int j = 0; j < tiles[i].meshRenderers.Length; j++)
                {
                    float alpha = eval;// Mathf.Clamp(eval, 0.3f, 1);

                    Color col1 = tiles[i].meshRenderers[j].material.GetColor("_EmissionColor");
                    col1 = new Color(col1.r, col1.g, col1.b, alpha);
                    tiles[i].meshRenderers[j].material.SetColor("_EmissionColor", col1);

                    Color col = tiles[i].meshRenderers[j].material.GetColor("_Color");
                    col = new Color(col.r, col.g, col.b, alpha);

                    tiles[i].meshRenderers[j].material.SetColor("_Color", col);
                }
            }

            yield return 0;
        }

        StartCoroutine(Cleanup());
    }

    private IEnumerator Cleanup()
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            Destroy(tiles[i].gameObject);

            if (i % 10 == 0)
            {
                yield return 0;
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (transToFollow != null)
        {
            transform.position = new Vector3(transform.position.x, transToFollow.position.y, transform.position.z);
        }
	}
}
