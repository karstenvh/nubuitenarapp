﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DuplicateAndFlipNormals
{
    public static void ApplyToGameObject(GameObject g)
    {
        MeshFilter[] filters = g.GetComponentsInChildren<MeshFilter>();

        for (int f = 0; f < filters.Length; f++)
        {
            // Duplicate the gameobject
            GameObject g2 = GameObject.Instantiate(filters[f].gameObject);
            g2.transform.SetParent(filters[f].gameObject.transform, false);

            MeshFilter filter2 = g2.GetComponent<MeshFilter>();

            //List<int> verts = new List<int>(filter2.mesh.triangles.Length);
            int[] tris = filter2.mesh.GetTriangles(0);
            for (int t = 0; t < tris.Length; t +=3)
            {
                int tplus1 = tris[t + 1];
                tris[t + 1] = tris[t];
                tris[t] = tplus1;
            }

            filter2.mesh.SetTriangles(tris,0);

            List<Vector3> normals = new List<Vector3>(filter2.mesh.normals.Length);
            filter2.mesh.GetNormals(normals);

            for (int n = 0; n < normals.Count; n++)
            {
                normals[n] = -normals[n];
            }

            filter2.mesh.SetNormals(normals);
        }
    }
}
