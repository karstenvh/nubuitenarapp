﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using VROwl.UI;

/// <summary>
/// A drawn line.
/// </summary>
[ExecuteInEditMode]
public class MeasuringTape : MonoBehaviour, iFlexibleLengthObject
{
    public float distanceForward = 0.03f;

    public GameObject graphicsRoot;
    public MeshRenderer graphicsMeshRenderer;

    [SerializeField]
    private GameObject endPlacingMarker;

    [SerializeField]
    private GameObject endNormalMarker;

    /// <summary>
    /// The start of the line.
    /// </summary>
    public Transform StartPoint;

    /// <summary>
    /// The end of the line.
    /// </summary>
    public Transform EndPoint;

    public float? DisplayLengthOverride = null;

    [SerializeField]
    private Text text;

    /// <summary>
    /// Whether this line is horizontal or vertical.
    /// </summary>
    protected bool isHorizontal;

    private Quaternion originalTextLocalRotation;

    public void Start()
    {
        endPlacingMarker.SetActive(true);
        endNormalMarker.SetActive(false);

        if (UI_Manager.instance != null)
        {
            text.color = Color.white;// UI_Manager.instance.Stylesheet.MainColor;
        }
    }

    public void EnableEndMarker()
    {
        endPlacingMarker.SetActive(false);
        endNormalMarker.SetActive(true);

        Color c;

        if (UI_Manager.instance != null)
        {
            c = UI_Manager.instance.Stylesheet.MainColor;
        }
        else
        {
            c = Color.blue;
        }

        ChangeColor (c);
    }

    /// <summary>
    /// The length of this line.
    /// </summary>
    public float Length
    {
        get { return Vector3.Distance(StartPoint.transform.position, EndPoint.transform.position); }
    }

    /// <summary>
    /// The center of this line.
    /// </summary>
    public Vector3 Center
    {
        get { return StartPoint.transform.position + ((EndPoint.transform.position - StartPoint.transform.position) / 2f); }
    }

    /*
    /// <summary>
    /// Calculate the distance of a point to this line.
    /// </summary>
    /// <param name="point">The point.</param>
    /// <returns>The distance of a point to this line.</returns>
    public LineDistanceResult DistanceToPoint(Vector3 point)
    {
        Vector3 startPos = StartPoint.transform.position;
        Vector3 endPos = EndPoint.transform.position;

        float lengthSq = Length * Length;

        if (lengthSq == 0)
        {
            float distance_ = Vector3.Distance(startPos, point);
            return new LineDistanceResult(this, distance_, startPos);
        }

        float t = Vector3.Dot(point - startPos, endPos - startPos) / lengthSq;
        t = Mathf.Clamp01(t); // Stay on the line segment.
        Vector3 projection = startPos + t * (endPos - startPos);

        float distance = Vector3.Distance(point, projection);
        return new LineDistanceResult(this, distance, projection);
    }*/

    public void SetMaterial(Material material)
    {
        graphicsMeshRenderer.material = material;
    }

    public void Selected()
    {
		ChangeColor (Color.white);
    }

    public void Deselected()
    {
		Color c;

		if (UI_Manager.instance != null) {
			c = UI_Manager.instance.Stylesheet.MainColor;
		} else {
			c = Color.blue;
		}

		ChangeColor (c);
    }

	private void ChangeColor(Color c)
	{
		Image[] imgs = GetComponentsInChildren<Image> ();
		for (int i = 0; i < imgs.Length; i++) {
			imgs [i].color = c;
		}
	}

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public Transform GetStartPoint()
    {
        return StartPoint;
    }

    public Transform GetEndPoint()
    {
        return EndPoint;
    }

    public virtual void Update()
    {
        if (StartPoint == null || EndPoint == null)
        {
            //Debug.LogError("A line must have a start and end point.");
            return;
        }

        if (graphicsRoot != null)
        {
            Vector3 a = StartPoint.transform.position;
            Vector3 b = EndPoint.transform.position;

            float halfdistance = Vector3.Distance(a, b);
            Vector3 center = (a + b) / 2;

            graphicsRoot.transform.position = center;
            graphicsRoot.transform.localScale = new Vector3(graphicsRoot.transform.localScale.x, graphicsRoot.transform.localScale.y, halfdistance / 2);
            if (a - center != Vector3.zero)
            {
                graphicsRoot.transform.forward = a - center;
            }

            text.text = (graphicsRoot.transform.localScale.z * 2).ToString("0.00");
            text.canvas.transform.position = graphicsRoot.transform.position;
            if (Camera.main != null)
            {
                text.canvas.transform.LookAt(Camera.main.transform);
            }
        }
    }

    public static float UnsignedAngle(MeasuringTape line, MeasuringTape l2)
    {
        Vector3 l1Dir = line.EndPoint.transform.position - line.StartPoint.transform.position;
        Vector3 l2Dir = l2.EndPoint.transform.position - l2.StartPoint.transform.position;

        return Mathf.Abs(Vector3.Angle(l1Dir, l2Dir));
    }
}
