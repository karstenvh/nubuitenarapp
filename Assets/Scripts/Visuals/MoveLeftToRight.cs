﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeftToRight : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float speedModifier = 0.3f;

    public AnimationCurve curve;

    private float timer;
    private RectTransform rectTransform;
    bool goingForward;

	void Start ()
    {
        timer = curve.keys[0].time;
        goingForward = false;
        rectTransform = (RectTransform)transform;
    }
	
	void Update ()
    {
        if (goingForward)
        {
            timer += Time.deltaTime * speedModifier;
        }
        else
        {
            timer -= Time.deltaTime * speedModifier;
        }

        float x = minX + curve.Evaluate(timer) * Mathf.Abs(maxX - minX);
        rectTransform.localPosition = new Vector3(x, rectTransform.localPosition.y, rectTransform.localPosition.z);

        if (timer < curve.keys[0].time)
        {
            timer = curve.keys[0].time;
            goingForward = true;
        }

        if (timer > curve.keys[curve.keys.Length-1].time)
        {
            timer = curve.keys[curve.keys.Length-1].time;
            goingForward = false;
        }
    }
}
