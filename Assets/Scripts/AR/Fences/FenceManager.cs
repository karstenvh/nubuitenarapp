﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.AR.Fences
{
    public class FenceManager : FlexibleLengthManager
    {
        public Fence fencePrefab;

        protected override iFlexibleLengthObject InstantiateFlexibleLengthObject()
        {
            return Instantiate(fencePrefab);
        }
    }
}