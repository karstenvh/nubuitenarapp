﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.AR.Fences
{
    public class Fence : MonoBehaviour, iFlexibleLengthObject
    {
        [SerializeField]
        private GameObject graphicsRoot;

        [SerializeField]
        private GameObject polePrefab;
        [SerializeField]
        private GameObject bottomPrefab;
        [SerializeField]
        private GameObject topPrefab;

        private GameObject polePrefabInstance;
        private FenceTopBottom bottomAndTopPrefabInstance;

        [SerializeField]
        private MeshRenderer graphicsMeshRenderer;

        [SerializeField]
        private GameObject endPlacingMarker;

        [SerializeField]
        private GameObject endNormalMarker;

        /// <summary>
        /// The start of the line.
        /// </summary>
        [SerializeField]
        private Transform StartPoint;

        /// <summary>
        /// The end of the line.
        /// </summary>
        [SerializeField]
        private Transform EndPoint;

        private float poleSize;
        private float bottomAndTopSize;

        private const int MAXFENCEPARTS = 200;

        private const float BLOCKERSIZEDELTA = 0.01f;

        private List<GameObject> partInstances;

        public void Start()
        {
            endPlacingMarker.SetActive(true);
            endNormalMarker.SetActive(false);

            GeneratePrefabInstances();

            partInstances = new List<GameObject>();
        }

        private void GeneratePrefabInstances()
        {
            DestroyImmediate(polePrefabInstance);
            polePrefabInstance = Instantiate(polePrefab);
            poleSize = polePrefabInstance.CalculateLocalBoundsFromMeshes(new MeshRenderer[] { }).size.x;
            polePrefabInstance.SetActive(false);

            MergeBottomAndTop();
        }

        private void MergeBottomAndTop()
        {
            if (bottomAndTopPrefabInstance != null)
            {
                DestroyImmediate(bottomAndTopPrefabInstance.gameObject);
            }

            // Instantiate top and bottom. We make a root GameObject to make sure scale is 1 on the root.
            GameObject bottomRoot = new GameObject();
            GameObject bottomInstance = Instantiate(bottomPrefab, bottomRoot.transform);
            bottomInstance.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

            GameObject topRoot = new GameObject();
            GameObject topInstance = Instantiate(topPrefab, topRoot.transform);
            topInstance.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

            // Calculate bounds
            Bounds bottomBounds = bottomRoot.CalculateLocalBoundsFromMeshes(new MeshRenderer[] { });
            Debug.Log(bottomBounds.center.y);
            Debug.Log(bottomBounds.size.y);
            Bounds topBounds = topRoot.CalculateLocalBoundsFromMeshes(new MeshRenderer[] { });

            // Set them on the same XZ position
            bottomRoot.transform.localPosition = new Vector3(bottomRoot.transform.localPosition.x - bottomBounds.center.x, bottomRoot.transform.localPosition.y, bottomRoot.transform.localPosition.z - bottomBounds.center.z);
            topRoot.transform.localPosition = new Vector3(topRoot.transform.localPosition.x - topBounds.center.x, topRoot.transform.localPosition.y, topRoot.transform.localPosition.z - topBounds.center.z);

            // Stack them in terms of y-position.
            bottomRoot.transform.localPosition = new Vector3(bottomRoot.transform.localPosition.x, bottomRoot.transform.localPosition.y - bottomBounds.center.y + bottomBounds.extents.y, bottomRoot.transform.localPosition.z);
            topRoot.transform.localPosition = new Vector3(topRoot.transform.localPosition.x, topRoot.transform.localPosition.y - topBounds.center.y + topBounds.extents.y + bottomBounds.size.y, topRoot.transform.localPosition.z);

            GameObject container = new GameObject();
            bottomAndTopPrefabInstance = container.AddComponent<FenceTopBottom>();
            bottomRoot.transform.SetParent(bottomAndTopPrefabInstance.transform, true);
            topRoot.transform.SetParent(bottomAndTopPrefabInstance.transform, true);

            Bounds bottomTopBounds = bottomAndTopPrefabInstance.gameObject.CalculateLocalBoundsFromMeshes(new MeshRenderer[] { });
            bottomAndTopPrefabInstance.blocker = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Destroy(bottomAndTopPrefabInstance.blocker.GetComponent<BoxCollider>());
            bottomAndTopPrefabInstance.blocker.GetComponent<MeshRenderer>().material = Globals.instance.mat_FenceHider;
            bottomAndTopPrefabInstance.blocker.transform.position = bottomTopBounds.center;
            bottomAndTopPrefabInstance.blocker.transform.localScale = bottomTopBounds.size + Vector3.one * BLOCKERSIZEDELTA;
            bottomAndTopPrefabInstance.blocker.transform.SetParent(bottomAndTopPrefabInstance.transform, true);

            bottomAndTopPrefabInstance.gameObject.SetActive(false);
            bottomAndTopSize = Mathf.Max(bottomBounds.size.x, topBounds.size.x);
        }

        public void EnableEndMarker()
        {
        }

        /// <summary>
        /// The length of this line.
        /// </summary>
        public float Length
        {
            get { return Vector3.Distance(StartPoint.transform.position, EndPoint.transform.position); }
        }

        /// <summary>
        /// The center of this line.
        /// </summary>
        public Vector3 Center
        {
            get { return StartPoint.transform.position + ((EndPoint.transform.position - StartPoint.transform.position) / 2f); }
        }

        /*
        /// <summary>
        /// Calculate the distance of a point to this line.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The distance of a point to this line.</returns>
        public LineDistanceResult DistanceToPoint(Vector3 point)
        {
            Vector3 startPos = StartPoint.transform.position;
            Vector3 endPos = EndPoint.transform.position;

            float lengthSq = Length * Length;

            if (lengthSq == 0)
            {
                float distance_ = Vector3.Distance(startPos, point);
                return new LineDistanceResult(this, distance_, startPos);
            }

            float t = Vector3.Dot(point - startPos, endPos - startPos) / lengthSq;
            t = Mathf.Clamp01(t); // Stay on the line segment.
            Vector3 projection = startPos + t * (endPos - startPos);

            float distance = Vector3.Distance(point, projection);
            return new LineDistanceResult(this, distance, projection);
        }*/

        public void SetMaterial(Material material)
        {
            graphicsMeshRenderer.material = material;
        }

        public void Selected()
        {
        }

        public void Deselected()
        {
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public Transform GetStartPoint()
        {
            return StartPoint;
        }

        public Transform GetEndPoint()
        {
            return EndPoint;
        }

        public virtual void Update()
        {
            if (StartPoint == null || EndPoint == null)
            {
                //Debug.LogError("A line must have a start and end point.");
                return;
            }

            if (graphicsRoot != null)
            {
                Vector3 a = StartPoint.transform.position;
                Vector3 b = EndPoint.transform.position;

                float fullDistance = Vector3.Distance(a, b);
                float halfdistance = fullDistance / 2f;
                Vector3 center = (a + b) / 2;

                graphicsRoot.transform.position = center;
                //graphicsRoot.transform.localScale = new Vector3(graphicsRoot.transform.localScale.x, graphicsRoot.transform.localScale.y, halfdistance / 2);
                if (a - center != Vector3.zero)
                {
                    graphicsRoot.transform.right = b - center;
                }

                if (bottomAndTopPrefabInstance == null || polePrefabInstance == null)
                {
                    GeneratePrefabInstances();
                }

                int count = 0;
                float totalPartSize = 0;
                PartType currentPartType = 0;

                for (int i = 0; i < partInstances.Count; i++)
                {
                    DestroyImmediate(partInstances[i]);
                }

                partInstances.Clear();

                while (count <= MAXFENCEPARTS)
                {
                    float distanceLeft = fullDistance - totalPartSize;
                    float partSize = GetPartSize(currentPartType);
                    if (partSize <= distanceLeft)
                    {
                        GameObject partInstance = InstantiatePart(currentPartType);
                        partInstance.SetActive(true);

                        float localX = -halfdistance + totalPartSize + partSize / 2f;
                        partInstance.transform.localPosition = new Vector3(localX, 0, 0);

                        if (currentPartType == PartType.BottomAndTop)
                        {
                            FenceTopBottom s = partInstance.GetComponent<FenceTopBottom>();
                            s.blocker.SetActive(false);
                        }

                        partInstances.Add(partInstance);

                        currentPartType++;
                        if (currentPartType == PartType.Count)
                        {
                            currentPartType = 0;
                        }
                        totalPartSize += partSize;
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }

                // Finally, add the last fence piece and pole
                float finalDistanceLeft = fullDistance - totalPartSize;

                // fence
                if (finalDistanceLeft > poleSize)
                {
                    GameObject finalFence = InstantiatePart(PartType.BottomAndTop);
                    finalFence.SetActive(true);

                    float fenceLocalX = -halfdistance + totalPartSize + bottomAndTopSize / 2f;
                    finalFence.transform.localPosition = new Vector3(fenceLocalX, 0, 0);

                    FenceTopBottom script = finalFence.GetComponent<FenceTopBottom>();
                    float blockerSize = bottomAndTopSize - finalDistanceLeft + BLOCKERSIZEDELTA;
                    script.blocker.transform.localPosition = new Vector3(BLOCKERSIZEDELTA / 2f + bottomAndTopSize / 2f - blockerSize / 2f, script.blocker.transform.localPosition.y, script.blocker.transform.localPosition.z);
                    script.blocker.transform.localScale = new Vector3(blockerSize, script.blocker.transform.localScale.y, script.blocker.transform.localScale.z);

                    totalPartSize += finalDistanceLeft;

                    partInstances.Add(finalFence);
                }

                // pole
                GameObject finalPole = InstantiatePart(PartType.Pole);
                finalPole.SetActive(true);

                float poleLocalX = -halfdistance + totalPartSize + poleSize / 2f;
                finalPole.transform.localPosition = new Vector3(poleLocalX, 0, 0);

                partInstances.Add(finalPole);
            }
        }

        private float GetPartSize(PartType partType)
        {
            switch (partType)
            {
                case PartType.Pole:
                    return poleSize;
                case PartType.BottomAndTop:
                    return bottomAndTopSize;
            }

            return 0;
        }

        private GameObject InstantiatePart(PartType partType)
        {
            switch (partType)
            {
                case PartType.Pole:
                    return Instantiate(polePrefabInstance, graphicsRoot.transform);
                case PartType.BottomAndTop:
                    return Instantiate(bottomAndTopPrefabInstance.gameObject, graphicsRoot.transform);
            }

            return new GameObject();
        }

        private enum PartType
        {
            Pole = 0,
            BottomAndTop = 1,
            Count = 2
        }

        public static float UnsignedAngle(MeasuringTape line, MeasuringTape l2)
        {
            Vector3 l1Dir = line.EndPoint.transform.position - line.StartPoint.transform.position;
            Vector3 l2Dir = l2.EndPoint.transform.position - l2.StartPoint.transform.position;

            return Mathf.Abs(Vector3.Angle(l1Dir, l2Dir));
        }
    }
}