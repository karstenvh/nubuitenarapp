﻿// copyright Uil VR Solutions B.V.

using UnityEngine;
using UnityEngine.XR.iOS;

public class StaticARKitBridge : MonoBehaviour
{
    public void Pause()
    {
        UnityARSessionNativeInterface.GetARSessionNativeInterface().Pause();
    }

    public void Resume()
    {
        UnityARSessionNativeInterface.GetARSessionNativeInterface().Run();
    }

	public static void ResetScene()
	{
		ARKitWorldTrackingSessionConfiguration sessionConfig = new ARKitWorldTrackingSessionConfiguration(UnityARAlignment.UnityARAlignmentGravity, UnityARPlaneDetection.Horizontal);
		UnityARSessionNativeInterface.GetARSessionNativeInterface().RunWithConfigAndOptions(sessionConfig, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking);
	}
}