﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.AR.Placement;

public class DuplicateButton : MonoBehaviour
{
    public Collider addButton;
    public Collider removeButton;

    [HideInInspector]
    public ARPlaceable parent;
    [HideInInspector]
    public Direction direction; 
	
	public void OnClicked(Collider col)
    {
        if (col == addButton)
        {
            parent.Expand(direction);
        }
        else if (col == removeButton)
        {
            parent.Reduce(direction);
        }
    }

    public enum Direction
    {
        Left,
        Right
    }
}
