﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace VROwl.AR.Models
{
    public class DefaultModelLoader : MonoBehaviour
    {
        [Serializable]
        public class DefaultModelInfo
        {
            public int productId;
            public string modelPath;
            public string hash;
        }

        [SerializeField]
        private List<DefaultModelInfo> defaultModelInfo;

        public void UpdateDefaultModels(ModelCache cache)
        {
            foreach(var info in defaultModelInfo)
            {
                if (!cache.HasModel(info.productId))
                {
                    cache.SaveModel(info.productId, SetRelativeToStreamingAssets(info.modelPath), info.hash);
                }
            }
        }

        private static string SetRelativeToStreamingAssets(string file)
        {
#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_ANDROID
            return Application.dataPath + "/StreamingAssets/" + file;
#elif UNITY_IOS
            return Application.dataPath + "/Raw/" + file;
#endif

        }
    }
}
