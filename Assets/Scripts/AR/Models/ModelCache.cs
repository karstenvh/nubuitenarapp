﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FullSerializer;
using UnityEngine;
using VROwl.Network;

namespace VROwl.AR.Models
{
    public class ModelCache
    {
        private class ModelMetafile
        {
            public int ProductId { get; set; }

            public string Hash { get; set; }
            public string RemoteSource { get; set; }
            public string ModelPath { get; set; }
        }

        private static readonly string MODEL_CACHE_FOLDER = Path.Combine(Application.persistentDataPath, "ModelCache");
        private static readonly string META_TAG = ".meta";

        private static readonly fsSerializer serializer = new fsSerializer();

        private Dictionary<int, string> productPaths = new Dictionary<int, string>();

        public static string CacheFolder
        {
            get { return MODEL_CACHE_FOLDER; }
        }

        public ModelCache()
        {
            LoadCache();
        }

        /// <summary>
        /// Determines whether the a model for the specified product ID is in the cache.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns>
        ///   <c>true</c> if the model is in the cache; otherwise, <c>false</c>.
        /// </returns>
        public bool HasModel(int productId)
        {
            return productPaths.ContainsKey(productId);
        }

        /// <summary>
        /// Determines whether the model for the specified product ID is up to date.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="hash">The hash.</param>
        /// <returns>
        ///   <c>true</c> if the model is up to date; otherwise, <c>false</c>.
        /// </returns>
        public bool IsModelUpToDate(int productId, string hash)
        {
            if (HasModel(productId))
			{				
				var path = productPaths[productId];
				var metaPath = Path.Combine(MODEL_CACHE_FOLDER, Path.GetFileName(path)) + META_TAG;

                ModelMetafile result;
				if (TryReadMetafile(metaPath, out result))
                {
                    return result.Hash.Equals(hash, StringComparison.InvariantCultureIgnoreCase);
                }
            }

            return false;
        }

        /// <summary>
        /// Check if model is in cache or if it is a base model.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public bool IsModelInCache(int productId)
        {
            if (HasModel(productId))
			{				
				var path = productPaths[productId];
				var metaPath = Path.Combine(MODEL_CACHE_FOLDER, Path.GetFileName(path)) + META_TAG;

                ModelMetafile result;
                if (TryReadMetafile(metaPath, out result))
                {
                    return !string.IsNullOrEmpty(result.RemoteSource);
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the path for the model for the specified product ID.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        public string GetModelPath(int productId)
        {
            if (productPaths.ContainsKey(productId))
            {
                return productPaths[productId];
            }

            return null;
        }

        /// <summary>
        /// Saves the downloaded model.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="downloadResult">The download result.</param>
        /// <param name="hash">The model hash.</param>
        public void SaveDownloadedModel(int productId, DownloadResult downloadResult, string hash)
        {
            if (File.Exists(downloadResult.OriginalRequest.DestinationFilePath))
            {
                var metafile = new ModelMetafile
                {
                    ModelPath = downloadResult.OriginalRequest.DestinationFilePath,
                    RemoteSource = downloadResult.OriginalRequest.DownloadUrl,
                    ProductId = productId,
                    Hash = hash
                };

                WriteMetafile(metafile);

                productPaths.Add(productId, metafile.ModelPath);
            }
        }

        /// <summary>
        /// Saves the model to the cache.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="modelPath">The model path.</param>
        /// <param name="modelHash">The model hash.</param>
        public void SaveModel(int productId, string modelPath, string modelHash)
        {
            if (!HasModel(productId))
            {
                if (File.Exists(modelPath))
                {
                    var metafile = new ModelMetafile
                    {
                        ModelPath = modelPath,
                        ProductId = productId,
                        Hash = modelHash
                    };

                    WriteMetafile(metafile);

                    productPaths.Add(productId, metafile.ModelPath);
                }
            }
        }

        public void ClearCache()
        {
            // Delete files by metadata.
            foreach (int productId in productPaths.Keys.ToList())
			{
                var path = productPaths[productId];
				var metaPath = Path.Combine(MODEL_CACHE_FOLDER, Path.GetFileName(path)) + META_TAG;

                ModelMetafile metafile = null;
                if (TryReadMetafile(metaPath, out metafile))
                {
                    var fileInfo = new FileInfo(metafile.ModelPath);
                    if (fileInfo.Exists)
                    {
                        if (!string.IsNullOrEmpty(metafile.RemoteSource))
                        {
                            File.Delete(metaPath);
                            File.Delete(path);

                            productPaths.Remove(productId);
                        }
                    }
                    else
                    {
                        Debug.LogWarningFormat("Metafile found for non existing model: {0}", metafile.ModelPath);
                        File.Delete(path);
                        File.Delete(metaPath);

                        productPaths.Remove(productId);
                    }
                }
                else
                {
                    Debug.LogWarningFormat("Could not read metafile at {0}", metaPath);
                    File.Delete(path);
                    File.Delete(metaPath);

                    productPaths.Remove(productId);
                }
            }
        }

        /// <summary>
        /// Deletes the model from the cache.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        public void DeleteModel(int productId)
        {
            if (HasModel(productId))
            {
                var path = productPaths[productId];
                var metaPath = path + META_TAG;

                File.Delete(path);
                File.Delete(metaPath);

                productPaths.Remove(productId);
            }
        }

        private void LoadCache()
        {
            Directory.CreateDirectory(MODEL_CACHE_FOLDER);

            var metafilePaths = Directory.GetFiles(MODEL_CACHE_FOLDER, "*" + META_TAG, SearchOption.AllDirectories);
            foreach(var metafilePath in metafilePaths.ToList())
            {
                ModelMetafile metafile;
				if (TryReadMetafile (metafilePath, out metafile)) {
					var fileInfo = new FileInfo (metafile.ModelPath);
					if (fileInfo.Exists) {
						productPaths [metafile.ProductId] = metafile.ModelPath;
					} else {
						Debug.LogWarningFormat ("Metafile found for non existing model: {0}", metafile.ModelPath);
						File.Delete (metafilePath);
					}
				} 
				else 
				{
					Debug.LogWarningFormat("Could not read metafile at {0}", metafilePath);
					File.Delete(metafilePath);
				}
            }

            Debug.LogFormat("Loaded {0} models in cache.", productPaths.Count);
        }

        private bool TryReadMetafile(string metafilePath, out ModelMetafile result)
        {
            string metadata = File.ReadAllText(metafilePath);

            fsData data;
            result = new ModelMetafile();
            return fsJsonParser.Parse(metadata, out data).Succeeded && serializer.TryDeserialize(data, ref result).Succeeded;
        }

        private void WriteMetafile(ModelMetafile metafile)
        {
            fsData data;
            if (serializer.TrySerialize(metafile, out data).Succeeded)
			{				
				var metaPath = Path.Combine (MODEL_CACHE_FOLDER, Path.GetFileName (metafile.ModelPath)) + META_TAG;

				File.WriteAllText(metaPath, fsJsonPrinter.CompressedJson(data));

            }
        }
    }
}
