﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iFlexibleLengthObject
{
    void Selected();
    void Deselected();
    Transform GetStartPoint();
    Transform GetEndPoint();
    void EnableEndMarker();
    GameObject GetGameObject();
}
