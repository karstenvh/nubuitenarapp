﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.AR
{
    /// <summary>
    /// Manager for the 3D measuring tape.
    /// </summary>
    public abstract class FlexibleLengthManager : MonoBehaviour
    {
        /// <summary>
        /// Have started a measuring tape?
        /// </summary>
        public bool FlexibleChangeInProgress { get; private set; }

        /// <summary>
        /// The measuring tape prefab
        /// </summary>
        [SerializeField]
        private iFlexibleLengthObject flexibleLengthObjectPrefab;

        private List<iFlexibleLengthObject> flexibleLengthObjects;

        public bool IsEditing { get; private set; }

        public bool IsDraggingStart { get; private set; }

        private iFlexibleLengthObject currentFlexibleLengthObject;

        private Vector3 editOriginalPosition;

        private Vector2 ScreenCenter
        {
            get
            {
                return new Vector2(Screen.width / 2f, Screen.height / 2f);
            }
        }

        private void Start()
        {
            FlexibleChangeInProgress = false;
            IsEditing = false;
            flexibleLengthObjects = new List<iFlexibleLengthObject>();
        }

        private void Update()
        {
            if (FlexibleChangeInProgress)
            {
                UpdateTapeEndpoints();
            }
        }

        /// <summary>
        /// Start a measurement.
        /// </summary>
        /// <param name="screenLocation">The screen location to start from </param>
        public void StartMeasuring()
        {
            // Don't start a measurement if we are already measuring.
            if (FlexibleChangeInProgress)
            {
                Debug.LogError("Can't start flexible change, already changing");
                return;
            }

            // Check if we hit a plane to start a measurement from.
            RaycastHit hit;
            if (Globals.instance.HitTestFloorPlanes(ScreenCenter, out hit))
            {
                FlexibleChangeInProgress = true;
                IsEditing = false;
                IsDraggingStart = false;
                currentFlexibleLengthObject = InstantiateFlexibleLengthObject();
                currentFlexibleLengthObject.GetStartPoint().position = hit.point;
                currentFlexibleLengthObject.GetEndPoint().position = hit.point;
            }
            else
            {
                // No hit, no measurement.
                return;
            }
        }

        protected abstract iFlexibleLengthObject InstantiateFlexibleLengthObject();

        /// <summary>
        /// End the current measurement.
        /// </summary>
        /// <param name="screenLocation">The screen location to end on.</param>
        public void StopMeasuring()
        {
            // Don't end if we were not measuring in the first place.
            if (!FlexibleChangeInProgress)
            {
                return;
            }

            // UpdateTapeEndpoints();

            if (!IsEditing)
            {
                currentFlexibleLengthObject.EnableEndMarker();
                flexibleLengthObjects.Add(currentFlexibleLengthObject);
            }

            FlexibleChangeInProgress = false;
            IsEditing = false;
        }

        public void CancelMeasuring()
        {
            if (FlexibleChangeInProgress)
            {
                if (!IsEditing)
                {
                    flexibleLengthObjects.Remove(currentFlexibleLengthObject);
                    Destroy(currentFlexibleLengthObject.GetGameObject());
                }
                else
                {

                    if (IsDraggingStart)
                    {
                        currentFlexibleLengthObject.GetStartPoint().transform.position = editOriginalPosition;
                    }
                    else
                    {
                        currentFlexibleLengthObject.GetEndPoint().transform.position = editOriginalPosition;
                    }
                }
            }

            FlexibleChangeInProgress = false;
            IsEditing = false;
        }

        public void EditMeasurement(iFlexibleLengthObject flexibleLengthObject, bool start)
        {
            if (FlexibleChangeInProgress)
            {
                StopMeasuring();
            }

            currentFlexibleLengthObject = flexibleLengthObject;

            IsDraggingStart = start;
            FlexibleChangeInProgress = true;
            IsEditing = true;

            if (start)
            {
                editOriginalPosition = flexibleLengthObject.GetStartPoint().transform.position;
            }
            else
            {
                editOriginalPosition = flexibleLengthObject.GetEndPoint().transform.position;
            }
        }

        private void UpdateTapeEndpoints()
        {
            RaycastHit hit;
            if (Globals.instance.HitTestFloorPlanes(ScreenCenter, out hit))
            {
                if (IsEditing && IsDraggingStart)
                {
                    currentFlexibleLengthObject.GetStartPoint().position = hit.point;
                }
                else
                {
                    currentFlexibleLengthObject.GetEndPoint().position = hit.point;
                }
            }
        }

        public iFlexibleLengthObject Select(Vector2 screenLocation)
        {
            RaycastHit hit;

            return Select(screenLocation, out hit);
        }

        public iFlexibleLengthObject Select(Vector2 screenLocation, out RaycastHit hit)
        {
            RaycastHit[] hits;

            // Check if we clicked on a placeable.
            if (Globals.instance.HitTestMeasuringTapes_All(screenLocation, out hits))
            {
                float minFullObject = float.MaxValue;
                float minEndpoint = float.MaxValue;

                iFlexibleLengthObject closestFullObject = null;
                RaycastHit closestFullHit = new RaycastHit();
                iFlexibleLengthObject closestEndpoint = null;
                RaycastHit closestEndpointHit = new RaycastHit();

                // Find closest tape endpoint, or otherwise just closes tape.
                for (int i = 0; i < hits.Length; i++)
                {
                    iFlexibleLengthObject flexibleLengthObject = hits[i].collider.gameObject.GetComponentInParent<iFlexibleLengthObject>();

                    if (flexibleLengthObject != null)
                    {
                        float distance = hits[i].distance;

                        if (hits[i].transform == flexibleLengthObject.GetStartPoint().transform || hits[i].transform == flexibleLengthObject.GetEndPoint().transform)
                        {
                            // If transform of tape is not the same as the collider, it is an endpoint. We don't care which endpoint at this stage.
                            if (distance < minEndpoint && flexibleLengthObject.GetGameObject().transform != hits[i].transform)
                            {
                                closestEndpoint = flexibleLengthObject;
                                closestEndpointHit = hits[i];
                                minEndpoint = distance;
                            }
                        }
                        else
                        {
                            if (distance < minFullObject)
                            {
                                closestFullObject = flexibleLengthObject;
                                closestFullHit = hits[i];
                                minFullObject = distance;
                            }
                        }
                    }
                }

                if (closestEndpoint != null)
                {
                    hit = closestEndpointHit;
                    return closestEndpoint;
                }
                else
                {
                    hit = closestFullHit;
                    return closestFullObject;
                }
            }
            else
            {
                hit = new RaycastHit();
                // No selection.
                return null;
            }
        }

        public bool Remove(iFlexibleLengthObject flexibleLengthObject)
        {
            if (flexibleLengthObjects.Contains(flexibleLengthObject))
            {
                flexibleLengthObjects.Remove(flexibleLengthObject);
                Destroy(flexibleLengthObject.GetGameObject());
                return true;
            }
            else
            {
                return false;
            }
        }
    }
} // namespace VROwl.AR