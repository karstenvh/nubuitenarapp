﻿// copyright Uil VR Solutions B.V.

using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

namespace VROwl.AR
{
    /// <summary>
    /// Interaction with the measuring tape.
    /// </summary>
    public class FlexibleLengthInteraction : MonoBehaviour
    {
        /// <summary>
        /// The measuring tape.
        /// </summary>
        [SerializeField]
        private FlexibleLengthManager flexibleLengthManager;

        [SerializeField]
        private BasePlaneManager basePlaneManager;

        /// <summary>
        /// Whether any interaction is allowed at all.
        /// </summary>
        [SerializeField]
        private bool allowInteraction = true;

        /// <summary>
        /// Whether or not UI blocks single-finger AR interaction.
        /// </summary>
        [SerializeField]
        private bool uiIsBlocking = true;

        public delegate void SelectionChanged();
        public SelectionChanged OnSelected;
        public SelectionChanged OnDeselected;

        public delegate void StartEdit();
        public StartEdit OnEdit;

        /// <summary>
        /// The current selection, or null if nothing was selected.
        /// </summary>
        private iFlexibleLengthObject selection;

        private bool wantToMeasureAfterBasePlane;

        public Image indicator;

        /// <summary>
        /// Whether any interaction is allowed at all.
        /// </summary>
        public bool AllowInteraction
        {
            get
            {
                return allowInteraction;
            }
            set
            {
                allowInteraction = value;
            }
        }

        /// <summary>
        /// Whether or not UI blocks single-finger AR interaction.
        /// </summary>
        public bool UIIsBlocking
        {
            get
            {
                return uiIsBlocking;
            }
            set
            {
                uiIsBlocking = value;
            }
        }

        private bool HasSelection
        {
            get { return selection != null; }
        }

        public bool FlexibleChangeInProgress
        {
            get { return indicator.isActiveAndEnabled || flexibleLengthManager.FlexibleChangeInProgress; }
        }

        private void Start()
        {
            indicator.gameObject.SetActive(false);
            basePlaneManager.OnFirstPlaneFound += OnBasePlaneFound;
        }

        public bool OnFingerTap(LeanFinger finger)
        {
            if (!enabled)
            {
                return false;
            }

            // Check if we want to use the event or not.
            if (!AllowInteraction                        // Interaction allowed?
             || (finger.StartedOverGui && UIIsBlocking)  // No blocking UI?
             || LeanTouch.Fingers.Count > 1
             || FlexibleChangeInProgress)             // First finger to touch the screen?
            {
                return false;
            }

            iFlexibleLengthObject previousSelection = selection;

            // First try to select an object.
            RaycastHit hit;
            iFlexibleLengthObject newSelection = flexibleLengthManager.Select(finger.ScreenPosition, out hit);

            bool hadSelection = selection != null;

            // Check if we selected an endpoint
            bool anyEndPointSelected = false;
            bool startEndpointSelected = false;

            if (newSelection != null && hit.collider.transform == newSelection.GetStartPoint())
            {
                anyEndPointSelected = true;
                startEndpointSelected = true;
            }
            else if (newSelection != null && hit.collider.transform == newSelection.GetEndPoint())
            {
                anyEndPointSelected = true;
                startEndpointSelected = false;
            }
            else
            {
                anyEndPointSelected = false;
            }

            // Check if the new selection is the same as the old selection
            bool selectedSameObject = newSelection == selection;

            // Selected endpoint
            if (anyEndPointSelected)
            {
                if (!HasSelection)
                {
                    Select(newSelection, hit);
                }
                EditMeasurement(newSelection, startEndpointSelected);
            }
            else
            // Selected a tape on its own
            if (newSelection != null && !selectedSameObject)
            {
                Select(newSelection, hit);
            }
            else
            // Deselect
            if (newSelection == null || selectedSameObject)
            {
                if (!flexibleLengthManager.FlexibleChangeInProgress)
                {
                    Deselect();
                }
            }

            return newSelection != null;
        }

        private void Select(iFlexibleLengthObject newSelection, RaycastHit hit)
        {
            if (selection != null)
            {
                Deselect();
            }

            selection = newSelection;
            selection.Selected();

            if (OnSelected != null)
            {
                OnSelected();
            }
        }

        private void Deselect()
        {
            if (selection != null)
            {
                selection.Deselected();
                selection = null;

                if (OnDeselected != null)
                {
                    OnDeselected();
                }
            }
        }

        /// <summary>
        /// Delete the current selection, if any.
        /// </summary>
        public void DeleteSelection()
        {
            if (selection != null)
            {
                flexibleLengthManager.StopMeasuring();
                flexibleLengthManager.Remove(selection);
                selection = null;
            }
        }

        public void EnableMeasuring()
        {
            if (basePlaneManager.HasBasePlane)
            {
                indicator.gameObject.SetActive(true);
            }
            else
            {
                wantToMeasureAfterBasePlane = true;
            }
        }

        private void OnBasePlaneFound()
        {
            if (wantToMeasureAfterBasePlane)
            {
                wantToMeasureAfterBasePlane = false;
                EnableMeasuring();
            }
        }

        public void StartMeasuring()
        {
            Deselect();

            indicator.gameObject.SetActive(false);

            flexibleLengthManager.StartMeasuring();
        }

        public void EditMeasurement(iFlexibleLengthObject flexibleLengthObject, bool start)
        {
            OnEdit();
            flexibleLengthManager.EditMeasurement(flexibleLengthObject, start);
        }

        public void StopMeasuring()
        {
            wantToMeasureAfterBasePlane = false;
            Deselect();
            indicator.gameObject.SetActive(false);
            flexibleLengthManager.StopMeasuring();
        }

        public void CancelMeasuring()
        {
            wantToMeasureAfterBasePlane = false;
            Deselect();
            indicator.gameObject.SetActive(false);
            flexibleLengthManager.CancelMeasuring();
        }

        private void OnDestroy()
        {
            basePlaneManager.OnFirstPlaneFound -= OnBasePlaneFound;
        }
    }
} // namespace VROwl.AR