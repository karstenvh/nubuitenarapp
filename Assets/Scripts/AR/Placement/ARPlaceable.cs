﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using UnityGLTF;
using VROwl.UI;
using VROwl.UI.Models;

namespace VROwl.AR.Placement
{
    /// <summary>
    /// An object that can be placed in AR.
    /// </summary>
    public class ARPlaceable : MonoBehaviour
    {
        public UI_Model_Product productInfo;

        /// <summary>
        /// Should a collider be generated from the mesh bounds?
        /// </summary>
        [SerializeField]
        private bool generateCollider = true;

        /// <summary>
        /// Should a fake shadow sprite be generated from the mesh bounds?
        /// </summary>
        [SerializeField]
        private bool generateFakeShadow = true;

        [SerializeField]
        private bool generateRotateQuad = true;

        [SerializeField]
        private bool generateDuplicateButtons = true;

        [SerializeField]
        private bool generateFlippedNormals = false;

        [SerializeField]
        private bool noGLTF = false;

        /// <summary>
        /// The layer to assign on Awake.
        /// </summary>
        private readonly int placeableLayer = 11;

        [HideInInspector]
        public GameObject shadowQuad;

        [HideInInspector]
        public Collider rotateCollider;

        private bool selected = false;

        private LinkedList<GameObject> models;

        private BoxCollider boxCollider;

        private Vector3 originalBoxSize;

        public DuplicateButton leftDupeButton;
        public DuplicateButton rightDupeButton;

        public delegate void ComplexityChanged();
        public ComplexityChanged OnComplexityChanged;

        private int expandCounter;

        private ulong singleModelVertexCount;

        public int NrOfProducts
        {
            get { return models.Count; }
        }

        public ulong GetVertexCount()
        {
            return singleModelVertexCount * (uint)models.Count;
        }

        private void Start()
        {
            singleModelVertexCount = 0;

            models = new LinkedList<GameObject>();

            if (GetComponentInChildren<MeshRenderer>() != null)
            {
                if (GetComponent<UnityGLTF.GLTFComponent>() != null)
                {
                    GetComponent<UnityGLTF.GLTFComponent>().enabled = false;
                }
            }

            if (!noGLTF)
            {
                GLTFComponent gltf = gameObject.AddComponent<GLTFComponent>();
                gltf.onLoaded += OnModelLoaded;

                //string modelPath = Globals.instance.gltfs[Globals.instance.currentGLTF];
                //Globals.instance.currentGLTF = (Globals.instance.currentGLTF + 1) % Globals.instance.gltfs.Length;
                //gltf.GLTFUri = SetRelativeToStreamingAssets(modelPath);

                gltf.GLTFUri = UI_Manager.instance.GetModelPath(productInfo);
            }
            else
            {
                GameObject modelRoot = new GameObject();
                modelRoot.transform.SetParent(transform, false);
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i) != modelRoot.transform)
                    {
                        transform.GetChild(i).SetParent(modelRoot.transform, true);
                        i--;
                    }
                }
                OnModelLoaded(modelRoot);
            }
        }

        public void Expand(DuplicateButton.Direction direction)
        {
            expandCounter++;

            Vector3 translation = new Vector3(originalBoxSize.x, 0, 0);
            Vector3 halfTranslation = translation / 2f;

            switch (direction)
            {
                case DuplicateButton.Direction.Left:
                    {
                        GameObject expansion = Expand_Aux(models.First.Value, -translation);
                        leftDupeButton.transform.localPosition -= translation;

                        models.AddFirst(expansion);

                        boxCollider.center = new Vector3(boxCollider.center.x - originalBoxSize.x / 2f, boxCollider.center.y, boxCollider.center.z);
                    }
                    break;

                case DuplicateButton.Direction.Right:
                    {
                        GameObject expansion = Expand_Aux(models.Last.Value, translation);
                        rightDupeButton.transform.localPosition += translation;

                        models.AddLast(expansion);

                        boxCollider.center = new Vector3(boxCollider.center.x + originalBoxSize.x / 2f, boxCollider.center.y, boxCollider.center.z);
                        break;
                    }
            }

            boxCollider.size = new Vector3(boxCollider.size.x + originalBoxSize.x, boxCollider.size.y, boxCollider.size.z);
            //rotateCollider.transform.localPosition = new Vector3(boxCollider.center.x, rotateCollider.transform.localPosition.y, rotateCollider.transform.localPosition.z);
            //shadowQuad.transform.localPosition = new Vector3(boxCollider.center.x, shadowQuad.transform.localPosition.y, shadowQuad.transform.localPosition.z);
            shadowQuad.transform.localScale = new Vector3(boxCollider.size.x * 2f, boxCollider.size.z * 2f, 1);

            // Adjust all positions so that local (0,0,0) is the center again.
            switch (direction)
            {
                case DuplicateButton.Direction.Left:
                    {
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            if (transform.GetChild(i) != rotateCollider.transform && transform.GetChild(i) != shadowQuad.transform)
                            {
                                transform.GetChild(i).localPosition += halfTranslation;
                            }
                        }
                        transform.Translate(-halfTranslation, Space.Self);
                        boxCollider.center += halfTranslation;
                        break;
                    }
                case DuplicateButton.Direction.Right:
                    {
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            if (transform.GetChild(i) != rotateCollider.transform && transform.GetChild(i) != shadowQuad.transform)
                            {
                                transform.GetChild(i).localPosition -= halfTranslation;
                            }
                        }
                        transform.Translate(halfTranslation, Space.Self);
                        boxCollider.center -= halfTranslation;
                        break;
                    }
            }

            // Set minus button availability
            UpdateDuplicateButtonAvailability();

            if (OnComplexityChanged != null)
            {
                OnComplexityChanged();
            }
        }

        private GameObject Expand_Aux(GameObject neighbour, Vector3 translation)
        {
            GameObject instance = Instantiate(neighbour);
            instance.transform.SetParent(transform);
            instance.transform.localPosition = neighbour.transform.localPosition;
            instance.transform.localRotation = neighbour.transform.localRotation;
            instance.transform.localScale = neighbour.transform.localScale;
            instance.transform.localPosition += translation;

            return instance;
        }

        public void Reduce(DuplicateButton.Direction direction)
        {
            // Always keep one model.
            if (models.Count <= 1)
            {
                return;
            }

            GameObject markedForDeath = null;
            Vector3 translation = new Vector3(originalBoxSize.x, 0, 0);
            Vector3 halfTranslation = translation / 2f;

            switch (direction)
            {
                case DuplicateButton.Direction.Left:
                    {
                        markedForDeath = models.First.Value;
                        models.RemoveFirst();
                        boxCollider.center = new Vector3(boxCollider.center.x + originalBoxSize.x / 2f, boxCollider.center.y, boxCollider.center.z);

                        leftDupeButton.transform.localPosition += translation;
                    }
                    break;

                case DuplicateButton.Direction.Right:
                    {
                        markedForDeath = models.Last.Value;
                        models.RemoveLast();
                        boxCollider.center = new Vector3(boxCollider.center.x - originalBoxSize.x / 2f, boxCollider.center.y, boxCollider.center.z);

                        rightDupeButton.transform.localPosition -= translation;
                    }
                    break;
            }

            Destroy(markedForDeath);
            boxCollider.size = new Vector3(boxCollider.size.x - originalBoxSize.x, boxCollider.size.y, boxCollider.size.z);
            //rotateCollider.transform.localPosition = new Vector3(boxCollider.center.x, rotateCollider.transform.localPosition.y, rotateCollider.transform.localPosition.z);
            //shadowQuad.transform.localPosition = new Vector3(boxCollider.center.x, shadowQuad.transform.localPosition.y, shadowQuad.transform.localPosition.z);
            shadowQuad.transform.localScale = new Vector3(boxCollider.size.x * 2f, boxCollider.size.z * 2f, 1);

            translation = -translation;
            halfTranslation = -halfTranslation;
            // Adjust all positions so that local (0,0,0) is the center again.
            switch (direction)
            {
                case DuplicateButton.Direction.Left:
                    {
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            if (transform.GetChild(i) != rotateCollider.transform && transform.GetChild(i) != shadowQuad.transform)
                            {
                                transform.GetChild(i).localPosition += halfTranslation;
                            }
                        }
                        transform.Translate(-halfTranslation, Space.Self);
                        boxCollider.center += halfTranslation;
                        break;
                    }
                case DuplicateButton.Direction.Right:
                    {
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            if (transform.GetChild(i) != rotateCollider.transform && transform.GetChild(i) != shadowQuad.transform)
                            {
                                transform.GetChild(i).localPosition -= halfTranslation;
                            }
                        }
                        transform.Translate(halfTranslation, Space.Self);
                        boxCollider.center -= halfTranslation;
                        break;
                    }
            }

            UpdateDuplicateButtonAvailability();
            if (OnComplexityChanged != null)
            {
                OnComplexityChanged();
            }
        }

        private void OnModelLoaded(GameObject model)
        {
            expandCounter = 1;

            models.AddFirst(model);

            Vector3 oldPos = transform.position;
            Quaternion oldRot = transform.rotation;

            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;

            GameObject instance;
            if (!noGLTF)
            {
                instance = model.GetComponentInChildren<InstantiatedGLTFObject>().gameObject;
            }
            else
            {
                instance = model;
            }

            SetCenter(instance, true, true, false);

            instance.transform.localPosition = new Vector3(0, 0, 0);

            MeshRenderer[] rends = model.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < rends.Length; i++)
            {
                rends[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                /*if (rends[i].sharedMaterial.name == "Material1")
                {
                    rends[i].sharedMaterial = Globals.instance.dummyDoorMaterial;
                }
                else if (rends[i].sharedMaterial.name == "Material2")
                {
                    rends[i].sharedMaterial = Globals.instance.dummyFrameMaterial;
                }*/
            }

            MeshFilter[] filters = model.GetComponentsInChildren<MeshFilter>();
            for (int i = 0; i < filters.Length; i++)
            {
                singleModelVertexCount += (uint)filters[i].mesh.vertexCount;
            }

            // Assign proper layer.
            gameObject.layer = placeableLayer;

            Bounds b = CalculateGOBounds.CalculateLocalBoundsFromMeshes(gameObject, new MeshRenderer[0], false);

            if (generateCollider)
            {
                GenerateCollider(b);
            }

            if (generateFlippedNormals)
            {
                DuplicateAndFlipNormals.ApplyToGameObject(gameObject);
            }

            if (generateFakeShadow)
            {
                GenerateFakeShadow(b);
            }

            if (generateRotateQuad)
            {
                GenerateRotateCollider(b);
            }

            boxCollider = GetComponent<BoxCollider>();
            if (boxCollider != null)
            {
                originalBoxSize = boxCollider.size;
            }

            if (generateDuplicateButtons)
            {
                GenerateDuplicateButtons(b);
            }

            transform.position = oldPos;
            transform.rotation = oldRot;

            if (shadowQuad != null && selected)
            {
                shadowQuad.transform.Translate(Vector3.down * Globals.instance.selectedHoverHeight, Space.World);
            }

            gameObject.AddComponent<UnityARUserAnchorComponent>();

            // Move back so that the front of the model sits on the original position.
            if (generateCollider)
            {
                transform.position -= transform.forward * (boxCollider.size.z / 2f);
            }

            UpdateColors(productInfo.Colors);

            if (OnComplexityChanged != null)
            {
                OnComplexityChanged();
            }
        }

        private void GenerateCollider(Bounds b)
        {
            BoxCollider box = gameObject.AddComponent<BoxCollider>();
            box.center = b.center;
            box.size = b.size;
            box.isTrigger = true;
        }

        private void GenerateFakeShadow(Bounds b)
        {
            shadowQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
            Destroy(shadowQuad.GetComponent<Collider>());

            shadowQuad.transform.SetParent(transform, false);
            shadowQuad.transform.Rotate(Vector3.right, 90, Space.Self);
            shadowQuad.transform.localScale = new Vector3(b.size.x * 2f, b.size.z * 2f, 1);

            shadowQuad.GetComponent<MeshRenderer>().material = Globals.instance.mat_fakeShadow;
        }

        private void GenerateRotateCollider(Bounds b)
        {
            rotateCollider = GameObject.CreatePrimitive(PrimitiveType.Quad).GetComponent<Collider>();
            rotateCollider.gameObject.layer = placeableLayer;

            rotateCollider.transform.SetParent(transform, true);
            rotateCollider.transform.position = new Vector3(transform.position.x, transform.position.y + Globals.instance.rotateQuadYOffset, transform.position.z);
            rotateCollider.transform.Rotate(Vector3.right, 90);

            float xzScale = Mathf.Min(b.size.x, b.size.z) * Globals.instance.rotateQuadOverflowFactor;
            rotateCollider.transform.localScale = new Vector3(xzScale, xzScale, 1);

            rotateCollider.GetComponent<MeshRenderer>().material = Globals.instance.mat_rotateQuad;
        }

        private void GenerateDuplicateButtons(Bounds b)
        {
            leftDupeButton = Instantiate(Globals.instance.duplicationButtonPrefab);
            leftDupeButton.parent = this;
            leftDupeButton.direction = DuplicateButton.Direction.Left;
            rightDupeButton = Instantiate(Globals.instance.duplicationButtonPrefab);
            rightDupeButton.parent = this;
            rightDupeButton.direction = DuplicateButton.Direction.Right;

            leftDupeButton.transform.SetParent(transform);
            leftDupeButton.transform.localPosition = new Vector3(b.center.x - b.size.x / 2f - Globals.instance.duplicateButtonOffset.x, b.center.y /*- b.size.y / 2f + Globals.instance.duplicateButtonOffset.y*/, b.center.z);

            rightDupeButton.transform.SetParent(transform);
            rightDupeButton.transform.localPosition = new Vector3(b.center.x + b.size.x / 2f + Globals.instance.duplicateButtonOffset.x, b.center.y /*- b.size.y / 2f + Globals.instance.duplicateButtonOffset.y*/, b.center.z);

            leftDupeButton.gameObject.SetActive(selected);
            rightDupeButton.gameObject.SetActive(selected);

            // Disable minus buttons at the start since we always start with one model.
            UpdateDuplicateButtonAvailability();
        }

        private void UpdateDuplicateButtonAvailability()
        {
            // Set minus button availability
            if (models.Count <= 1)
            {
                leftDupeButton.removeButton.gameObject.SetActive(false);
                rightDupeButton.removeButton.gameObject.SetActive(false);
            }
            else
            {
                leftDupeButton.removeButton.gameObject.SetActive(true);
                rightDupeButton.removeButton.gameObject.SetActive(true);
            }
        }

        public void UpdateColorsFromCurrentInfo()
        {
            if (productInfo != null && productInfo.Colors != null)
            {
                UpdateColors(productInfo.Colors);
            }
        }

        public void UpdateColors(List<UI_ColorOption> uiColors)
        {
            Dictionary<string, Color> matLookup = new Dictionary<string, Color>();

            // Build a lookup table for material names and their randomly generate colors.
            for (int i = 0; i < uiColors.Count; i++)
            {
                // We need to make a pseudo-random distribution. Rules:
                // 1. If (cols.Length > mats.Length), then every color may appear AT MOST once.
                // 2. If (cols.Length <= mats.Length), then every color must appear AT LEAST
                // (cols.Length / mats.Length) * 0.6 times.

                // Define the distributions
                List<UI_ColorSelection> availableColors = uiColors[i].GetSelectedColors();

                // Shuffle the colors
                List<UI_ColorSelection> availableColorsCpy = new List<UI_ColorSelection>(availableColors);
                for (int m = 0; m < availableColorsCpy.Count; m++)
                {
                    int randomIndex = Random.Range(0, availableColorsCpy.Count);
                    // Swap
                    UI_ColorSelection tmp = availableColorsCpy[m];
                    availableColorsCpy[m] = availableColorsCpy[randomIndex];
                    availableColorsCpy[randomIndex] = tmp;
                }

                int colCount = availableColorsCpy.Count;
                int matCount = uiColors[i].MaterialNames.Count;
                int minimumMatsPerCol = Mathf.Max(1, Mathf.FloorToInt((matCount / (float)colCount) * 0.6f));
                int remainingMats = Mathf.Max(0, matCount - minimumMatsPerCol * colCount);

                Dictionary<Color, int> maxNrOfColorAppearances = new Dictionary<Color, int>(availableColorsCpy.Count);
                for (int c = 0; c < colCount; c++)
                {
                    int randomExtra;
                    if (c < colCount - 1)
                    {
                        randomExtra = Random.Range(0, remainingMats);
                    }
                    else
                    {
                        randomExtra = remainingMats;
                    }

                    int finalCount = minimumMatsPerCol + randomExtra;
                    remainingMats -= randomExtra;
                    maxNrOfColorAppearances.Add(availableColorsCpy[c].Color, finalCount);
                }

                // Shuffle the material names
                List<string> matNamesCpy = new List<string>(uiColors[i].MaterialNames);
                for (int m = 0; m < matNamesCpy.Count; m++)
                {
                    int randomIndex = Random.Range(0, matNamesCpy.Count);
                    // Swap
                    string tmp = matNamesCpy[m];
                    matNamesCpy[m] = matNamesCpy[randomIndex];
                    matNamesCpy[randomIndex] = tmp;
                }

                // Assign the random list of materials sequentially.
                int currentMat = 0;

                foreach (KeyValuePair<Color, int> pair in maxNrOfColorAppearances)
                {
                    for (int cc = 0; cc < pair.Value && currentMat < matNamesCpy.Count; cc++)
                    {
                        string matNameInstaceTrain = matNamesCpy[currentMat];

                        for (int ac = 0; ac <= expandCounter; ac++)
                        {
                            if (matLookup.ContainsKey(matNameInstaceTrain))
                            {
                                break;
                            }

                            matLookup.Add(matNameInstaceTrain, pair.Key);
                            matNameInstaceTrain += " (Instance)";
                        }

                        currentMat++;
                    }
                }
            }

            // Assign colors.
            MeshRenderer[] rends = GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < rends.Length; i++)
            {
                string matName = rends[i].material.name;
                if (matLookup.ContainsKey(matName))
                {
                    rends[i].material.color = matLookup[matName];
                }
            }
        }

        public static void SetCenter(GameObject go, bool translate, bool onBase, bool rotate)
        {
            // Calculate world position center of all child mesh renderers
            MeshRenderer[] renderers = go.GetComponentsInChildren<MeshRenderer>();

            if (renderers == null || renderers.Length == 0)
            {
                Debug.Log("No MeshRenderers in children");
                return;
            }

            float lowestY;
            if (onBase)
            {
                lowestY = float.MaxValue;
            }
            else
            {
                lowestY = 0;
            }

            Bounds total = new Bounds();

            for (int i = 0; i < renderers.Length; i++)
            {
                // Note that MeshRenderer.bounds is in world space already. Mesh.bounds would be in
                // local space however.
                if (onBase)
                {
                    lowestY = Mathf.Min(lowestY, renderers[i].bounds.center.y - renderers[i].bounds.extents.y);
                }

                total.Encapsulate(renderers[i].bounds);
            }

            Vector3 cumulativeWorld = total.center;
            Debug.DrawLine(Vector3.zero, cumulativeWorld, Color.green, 10);

            // Cache children when we unparent them.
            List<Transform> children = new List<Transform>(go.transform.childCount);

            // Temporarily unparent the children.
            for (int i = 0; i < go.transform.childCount; i++)
            {
                Transform t = go.transform.GetChild(i);
                children.Add(t);

                t.SetParent(null, true);
                i--;
            }

            // Cache the change in position to apply to any colliders.
            Vector3 posDiff = go.transform.position - cumulativeWorld;
            Vector3 posDiffLocal = Vector3.Scale(go.transform.localScale, posDiff);
            Quaternion rotDiff = go.transform.localRotation; // We assume that we rotate towards Quaternion.identity, so the rotation difference equals the original rotation.

            if (translate)
            {
                go.transform.position = cumulativeWorld - new Vector3(0, lowestY + cumulativeWorld.y, 0);
            }

            if (rotate)
            {
                go.transform.rotation = Quaternion.identity;
            }

            // Re-parent children
            for (int i = 0; i < children.Count; i++)
            {
                children[i].SetParent(go.transform, true);
            }

            // Apply transformation to any attached non-child colliders where applicable.
            Collider[] colliders = go.GetComponents<Collider>();
            for (int i = 0; i < colliders.Length; i++)
            {
                Collider c = colliders[i];

                ReorientCollider(c, go.transform.localScale, posDiffLocal, translate, rotDiff, rotate);
            }
        }

        private static void ReorientCollider(Collider c, Vector3 scale, Vector3 posDiff, bool translate, Quaternion rotDiff, bool rotate)
        {
            if (c is BoxCollider)
            {
                BoxCollider b = (BoxCollider)c;
                b.center += posDiff; //RotateAndTranslateVector(b.center, posDiff, scale, translate, rotDiff, rotate);
            }
            else if (c is SphereCollider)
            {
                SphereCollider s = (SphereCollider)c;
                s.center += posDiff;
            }
            else if (c is CapsuleCollider)
            {
                CapsuleCollider cps = (CapsuleCollider)c;
                cps.center += posDiff;
            }
            else if (c is WheelCollider)
            {
                WheelCollider w = (WheelCollider)c;
                w.center += posDiff;
            }
        }

        public static string SetRelativeToStreamingAssets(string file)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            return Application.dataPath + "/StreamingAssets/" + file;
#elif UNITY_IOS || UNITY_ANDROID
            return Application.dataPath + "/Raw/" + file;
#endif
        }

        public void Selected()
        {
            selected = true;
            transform.Translate(Vector3.up * Globals.instance.selectedHoverHeight, Space.World);

            if (shadowQuad != null)
            {
                shadowQuad.transform.Translate(Vector3.down * Globals.instance.selectedHoverHeight, Space.World);
            }

            if (rotateCollider != null)
            {
                rotateCollider.gameObject.SetActive(true);
            }
            if (leftDupeButton != null)
            {
                leftDupeButton.gameObject.SetActive(true);
            }
            if (rightDupeButton != null)
            {
                rightDupeButton.gameObject.SetActive(true);
            }
        }

        public void Deselected()
        {
            selected = false;
            transform.Translate(Vector3.down * Globals.instance.selectedHoverHeight, Space.World);

            if (shadowQuad != null)
            {
                shadowQuad.transform.Translate(Vector3.up * Globals.instance.selectedHoverHeight, Space.World);
            }

            if (rotateCollider != null)
            {
                rotateCollider.gameObject.SetActive(false);
            }
            if (leftDupeButton != null)
            {
                leftDupeButton.gameObject.SetActive(false);
            }
            if (rightDupeButton != null)
            {
                rightDupeButton.gameObject.SetActive(false);
            }
        }
    }
} // namespace VROwl.AR.Placement