﻿// copyright Uil VR Solutions B.V.

using UnityEngine;
using System.Collections.Generic;

namespace VROwl.AR.Placement
{
    /// <summary>
    /// Abstract class for interaction with 3D object and an AR engine.
    /// </summary>
    public abstract class ARPlacementManager : MonoBehaviour
    {
        protected List<ARPlaceable> placedObjects;

        protected virtual void Start()
        {
            placedObjects = new List<ARPlaceable>();
        }

        /// <summary>
        /// Try to place an instance of a given ARPlaceable.
        /// </summary>
        /// <param name="placeablePrefab">The object to place.</param>
        /// <param name="screenLocation">The on-screen location to place at.</param>
        /// <param name="placedObject">The instance of the placed object if placement was succesful.</param>
        /// <returns>Whether placement was succesful.</returns>
		public abstract bool TryPlace(ARPlaceable placeablePrefab, Vector2 screenLocation, UI.Models.UI_Model_Product product, out ARPlaceable placedObject, bool inFrontOfCamera);

        public virtual ulong GetVertexCount()
        {
            ulong result = 0;

            for (int i = 0; i < placedObjects.Count; i++)
            {
                result += placedObjects[i].GetVertexCount();
            }

            return result;
        }

        /// <summary>
        /// Instantiate an ARPlaceable.
        /// </summary>
        /// <param name="placeablePrefab">The prefab to make an instance of.</param>
        /// <returns>The created ARPlaceable instance.</returns>
        protected virtual GameObject InstantiatePlaceableContainer()
        {
            GameObject instance = new GameObject();
            instance.transform.SetParent(transform, true);
            return instance;
        }

        /// <summary>
        /// Try to select an object using a screen location.
        /// </summary>
        /// <param name="screenLocation">The location on-screen to try and select on.</param>
        /// <returns>The selected object, or null otherwise.</returns>
        public abstract ARPlaceable Select(Vector2 screenLocation);

        public abstract ARPlaceable Select(Vector2 screenLocation, out RaycastHit hit);

        /// <summary>
        /// Try to remove an ARPlaceable.
        /// </summary>
        /// <param name="placeable">The placeable to remove.</param>
        /// <returns>Whether the placeable was succesfully removed.</returns>
        public abstract bool Remove(ARPlaceable placeable);

        /// <summary>
        /// Rotate a placed object around an axis.
        /// </summary>
        /// <param name="obj">The object to rotate.</param>
        /// <param name="axis">The axis to rotate around.</param>
        /// <param name="angle">The angle to rotate.</param>
        /// <returns>Whether the rotation was succesful.</returns>
        public abstract bool Rotate(ARPlaceable obj, Vector3 axis, float angle);

        public abstract void Drag(ARPlaceable obj, Vector2 screenLocation, Vector2 offset, float originalYRot, ARPlacementInteraction.DragType dragType);
    }
} // namespace VROwl.AR.Placement