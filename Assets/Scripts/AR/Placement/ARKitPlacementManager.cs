﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace VROwl.AR.Placement
{
    /// <summary>
    /// Handles 3D object interaction using AR Kit.
    /// </summary>
    public class ARKitPlacementManager : ARPlacementManager
    {
        [SerializeField]
        private Camera arCamera;

        public override bool TryPlace(ARPlaceable placeablePrefab, Vector2 screenLocation, UI.Models.UI_Model_Product product, out ARPlaceable placedInstance, bool inFrontOfCamera = false)
        {
            RaycastHit hit;

            // We try to hit one of the plane collider gameobjects that were generated by the ARKit
            // plugin effectively similar to calling HitTest with ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent
            //List<ARHitTestResult> hits = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(new ARPoint() { x = screenLocation.x, y = screenLocation.y }, ARHitTestResultType.ARHitTestResultTypeExistingPlane);
            if (Globals.instance.HitTestFloorPlanes(screenLocation, out hit))
            {
                GameObject container = InstantiatePlaceableContainer();
                placedInstance = container.AddComponent<ARPlaceable>();
                placedInstance.productInfo = product;

                //we're going to get the position from the contact point
                placedInstance.transform.position = hit.point;

                // Rotate around the y-axis until we are facing the camera.
                Vector2 forwardXZ = new Vector2(placedInstance.transform.forward.x, placedInstance.transform.forward.z);
                Vector3 obj2cam = Camera.main.transform.position - placedInstance.transform.position;
                Vector2 obj2camXZ = new Vector2(obj2cam.x, obj2cam.z);

                float obj2camAngle = Vector2.SignedAngle(forwardXZ, obj2camXZ);
				Rotate(placedInstance, Vector3.up, -obj2camAngle);

                // Apply company-specific rotation.
                placedInstance.transform.Rotate(Vector3.right, Globals.instance.modelOrientationCompensation.x, Space.Self);
                placedInstance.transform.Rotate(Vector3.up, Globals.instance.modelOrientationCompensation.y, Space.Self);
                placedInstance.transform.Rotate(Vector3.forward, Globals.instance.modelOrientationCompensation.z, Space.Self);

				// Offset the object in front of the user
                if (inFrontOfCamera)
                {
                    Vector3 normCam = arCamera.transform.forward.normalized;
                    Vector2 normCam2D = new Vector2(normCam.x, normCam.z).normalized;
                    Vector2 offset = normCam2D * Globals.instance.minimumDistanceInFrontOfCamera;

                    placedInstance.transform.position = new Vector3(arCamera.transform.position.x + offset.x, placedInstance.transform.position.y, arCamera.transform.position.z + offset.y);
                }

                placedObjects.Add(placedInstance);
                return true;
            }
            else
            {
                placedInstance = null;
                return false;
            }
        }

        public override ARPlaceable Select(Vector2 screenLocation)
        {
            RaycastHit hit;

            return Select(screenLocation, out hit);
        }

        public override ARPlaceable Select(Vector2 screenLocation, out RaycastHit hit)
        {
            // Check if we clicked on a placeable.
            if (Globals.instance.HitTestPlaceables(screenLocation, out hit))
            {
                ARPlaceable selection = hit.collider.gameObject.GetComponentInParent<ARPlaceable>();
                return selection;
            }
            else
            {
                // No selection.
                return null;
            }
        }

        public override bool Remove(ARPlaceable placeable)
        {
            if (placedObjects.Contains(placeable))
            {
                placedObjects.Remove(placeable);
                Destroy(placeable.gameObject);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool Rotate(ARPlaceable obj, Vector3 axis, float angle)
        {
            // We currently simply allow rotation for any non-null object.
            if (obj != null)
            {
                obj.transform.Rotate(axis, angle);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void Drag(ARPlaceable obj, Vector2 screenLocation, Vector2 dragOffset, float originalYRot, ARPlacementInteraction.DragType dragType)
        {
            RaycastHit hit;

            // We try to hit one of the plane collider gameobjects that were generated by the ARKit
            // plugin effectively similar to calling HitTest with ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent
            //List<ARHitTestResult> hits = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(new ARPoint() { x = screenLocation.x, y = screenLocation.y }, ARHitTestResultType.ARHitTestResultTypeExistingPlane);
            if (Globals.instance.HitTestDragPlanes(screenLocation, out hit))
            {
                switch (dragType)
                {
                    case ARPlacementInteraction.DragType.Position:
                        obj.transform.position = new Vector3(hit.point.x + dragOffset.x, obj.transform.position.y, hit.point.z + dragOffset.y);
                        return;
                    case ARPlacementInteraction.DragType.Rotation:
                        Vector2 hitPt2 = new Vector2(hit.point.x - obj.transform.position.x, hit.point.z - obj.transform.position.z);
                        float signedAngle = Vector2.SignedAngle(-dragOffset, hitPt2);
                        obj.transform.eulerAngles = new Vector3(obj.transform.rotation.x, originalYRot - (signedAngle), obj.transform.rotation.z);
                        break;
                }
            }
        }
    }
} // namespace VROwl.AR.Placement