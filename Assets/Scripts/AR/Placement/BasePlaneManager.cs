﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

/// <summary>
/// Ensure that a proper large base plane is available for interaction as long as one AR plane has been found.
/// </summary>
public class BasePlaneManager : MonoBehaviour
{
	public GameObject particles;

    /// <summary>
    /// What kind of base plane to use.
    /// </summary>
    [SerializeField]
    private BasePlaneType basePlaneType;

    /// <summary>
    /// The base plane.
    /// </summary>
    [SerializeField]
    private GameObject basePlane;

    [SerializeField]
    private GameObject dragPlane;

    [SerializeField]
    private FallingTiles fallingTilesPrefab;

    /// <summary>
    /// All AR planes.
    /// </summary>
    private List<ARPlaneAnchor> planes;

    public delegate void FirstPlaneFound();
	public FirstPlaneFound OnFirstPlaneFound;

    public bool HasBasePlane
    {
		get { return planes != null && planes.Count > 0; }
    }

    private void Start()
    {
        planes = new List<ARPlaneAnchor>();
        basePlane.SetActive(false);

        // Subscribe to the plane events of ARKit.
        UnityARSessionNativeInterface.ARAnchorAddedEvent += AddAnchor;
        UnityARSessionNativeInterface.ARAnchorUpdatedEvent += UpdateAnchor;
        UnityARSessionNativeInterface.ARAnchorRemovedEvent += RemoveAnchor;
    }

    private void Update()
    {
        dragPlane.transform.position = new Vector3(basePlane.transform.position.x, dragPlane.transform.position.y, basePlane.transform.position.z);

#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.F11))
        {
            OnFirstPlaneFound();
        }
#endif
    }

    /// <summary>
    /// Add an ARKit plane.
    /// </summary>
    /// <param name="anchorData">The plane to add.</param>
    private void AddAnchor(ARPlaneAnchor anchorData)
    {
		bool first = false;
        if (planes.Count == 0)
        {
			first = true;
            FallingTiles tiles = Instantiate(fallingTilesPrefab, UnityARMatrixOps.GetPosition(anchorData.transform), Quaternion.identity);
            tiles.transToFollow = basePlane.transform;
			particles.SetActive (false);
			FindObjectOfType<ParticleSystem> ().gameObject.SetActive (false);
        }

        planes.Add(anchorData);
        UpdateBasePlane();

		if (first)
        {
			if (OnFirstPlaneFound != null)
            {
				OnFirstPlaneFound ();
			}
		}
    }

    /// <summary>
    /// Remove an ARKit plane.
    /// </summary>
    /// <param name="anchorData">The plane to remove.</param>
    private void RemoveAnchor(ARPlaneAnchor anchorData)
    {
        planes.RemoveAll(a => a.identifier == anchorData.identifier);
        UpdateBasePlane();
    }

    /// <summary>
    /// Update an ARKit plane.
    /// </summary>
    /// <param name="anchorData">The plane to update.</param>
    private void UpdateAnchor(ARPlaneAnchor anchorData)
    {
        // Find the plane in our list and update it (ARKitAnchor is a value type so we need to replace it manually).
        for (int i = 0; i < planes.Count; i++)
        {
            if (planes[i].identifier == anchorData.identifier)
            {
                planes[i] = anchorData;
            }
        }

        UpdateBasePlane();
    }

    /// <summary>
    /// Update the base plane using the current set of ARKit planes.
    /// </summary>
    private void UpdateBasePlane()
    {
        switch (basePlaneType)
        {
            case BasePlaneType.Lowest:
                UpdateBasePlane_Lowest();
                break;

            case BasePlaneType.Largest:
                UpdateBasePlane_Largest();
                break;
        }
    }

    /// <summary>
    /// Update the base plane using the largest ARKit plane.
    /// </summary>
    private void UpdateBasePlane_Largest()
    {
        int largestIndex = -1;
        double largestSurfaceArea = double.MinValue;

        for (int i = 0; i < planes.Count; i++)
        {
            double size = planes[i].extent.x * planes[i].extent.z;
            if (size > largestSurfaceArea)
            {
                largestIndex = i;
                largestSurfaceArea = size;
            }
        }

        if (largestIndex >= 0)
        {
            basePlane.SetActive(true);
            basePlane.transform.position = UnityARMatrixOps.GetPosition(planes[largestIndex].transform);
        }
        else
        {
            basePlane.SetActive(false);
        }
    }

    /// <summary>
    /// Update the base plane using the lowest ARKit plane.
    /// </summary>
    private void UpdateBasePlane_Lowest()
    {
        int lowestIndex = -1;
        float lowest = float.MaxValue;

        for (int i = 0; i < planes.Count; i++)
        {
            float y = UnityARMatrixOps.GetPosition(planes[i].transform).y;
            if (y < lowest)
            {
                lowestIndex = i;
                lowest = y;
            }
        }

        if (lowestIndex >= 0)
        {
            basePlane.SetActive(true);
            basePlane.transform.position = UnityARMatrixOps.GetPosition(planes[lowestIndex].transform);
        }
        else
        {
            basePlane.SetActive(false);
        }
    }

    public void SetDragPlaneHeight(float height)
    {
        dragPlane.transform.position = new Vector3(dragPlane.transform.position.x, height, dragPlane.transform.position.z);
    }

    private void OnDestroy()
    {
        UnityARSessionNativeInterface.ARAnchorAddedEvent -= AddAnchor;
        UnityARSessionNativeInterface.ARAnchorUpdatedEvent -= UpdateAnchor;
        UnityARSessionNativeInterface.ARAnchorRemovedEvent -= RemoveAnchor;
    }

    private enum BasePlaneType
    {
        Lowest = 0,
        Largest = 1
    }
}