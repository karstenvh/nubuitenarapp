﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using VROwl.AR.Placement;
using VROwl.AR.Measurements;

/// <summary>
/// Handles finger tap events to properly select objects.
/// </summary>
public class SelectionManager : MonoBehaviour
{
    [SerializeField]
    private ARPlacementInteraction placementInteraction;

    [SerializeField]
    private MeasuringTapeInteraction measuringInteraction;

	private void Start ()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
    }

    private void OnFingerTap(LeanFinger finger)
    {
        RaycastHit duplicateButtonHit;
        RaycastHit duplicateButtonHitDummy;
        if (Globals.instance.HitTestDuplicateButtons(finger.ScreenPosition, out duplicateButtonHit) && !Globals.instance.HitTestPlaceables(finger.ScreenPosition, out duplicateButtonHitDummy))
        {
            duplicateButtonHit.collider.gameObject.GetComponentInParent<DuplicateButton>().OnClicked(duplicateButtonHit.collider);
            return;
        }

        if (placementInteraction.OnFingerTap(finger))
        {
            return;
        }

        if (measuringInteraction.OnFingerTap(finger))
        {
            return;
        }
    }

    private void Update ()
    {
		
	}

	private void OnDestroy()
	{
		LeanTouch.OnFingerTap -= OnFingerTap;
	}
}
