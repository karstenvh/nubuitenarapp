﻿using UnityEngine;

public class MeasuringTapeRepeatTexture : MonoBehaviour
{
    public Renderer Renderer;

    void Start()
    {
        if (Renderer == null)
        {
            Renderer = GetComponent<Renderer>();
        }
    }

    void Update()
    {
        if (Renderer != null)
        {
            Renderer.material.mainTextureScale = new Vector2(1, transform.lossyScale.y * 10);
        }
    }
}
