﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.AR.Measurements
{
    /// <summary>
    /// Manager for the 3D measuring tape.
    /// </summary>
    public class MeasuringTapeManager : FlexibleLengthManager
    {
        public MeasuringTape measuringTapePrefab;

        protected override iFlexibleLengthObject InstantiateFlexibleLengthObject()
        {
            return Instantiate(measuringTapePrefab);
        }
    }
} // namespace VROwl.AR.Measurements