﻿// copyright Uil VR Solutions B.V.

using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

namespace VROwl.AR.Measurements
{
    /// <summary>
    /// Interaction with the measuring tape.
    /// </summary>
    public class MeasuringTapeInteraction : FlexibleLengthInteraction
    {
    }
} // namespace VROwl.AR.Measurements