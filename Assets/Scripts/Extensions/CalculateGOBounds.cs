﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class CalculateGOBounds
{
    public static Bounds CalculateLocalBoundsFromMeshes(this GameObject go, MeshRenderer[] exclusions, bool includeInactive = false)
    {
        Quaternion currentRotation = go.transform.rotation;
        Vector3 currentLocalScale = go.transform.localScale;
        go.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        go.transform.localScale = Vector3.one;

        // Init bounds because its a struct, but we replace it on the first renderer.
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bool first = true;

        foreach (Renderer renderer in go.GetComponentsInChildren<Renderer>(includeInactive))
        {
            if (exclusions.Contains(renderer))
            {
                continue;
            }

            if (first)
            {
                first = false;
                bounds = renderer.bounds;
            }
            else
            {
                bounds.Encapsulate(renderer.bounds);
            }
        }

        Vector3 localCenter = bounds.center - go.transform.position;
        bounds.center = localCenter;
        go.transform.rotation = currentRotation;
        go.transform.localScale = currentLocalScale;

        return bounds;
    }

    public static Bounds CalculateLocalBoundsFromColliders(this GameObject go, Collider[] exclusions, bool includeInactive = false)
    {
        Quaternion currentRotation = go.transform.rotation;
        Vector3 currentLocalScale = go.transform.localScale;
        go.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        go.transform.localScale = Vector3.one;

        // Init bounds because its a struct, but we replace it on the first renderer.
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bool first = true;

        foreach (Collider collider in go.GetComponentsInChildren<Collider>(includeInactive))
        {
            if (exclusions.Contains(collider))
            {
                continue;
            }

            if (first)
            {
                first = false;
                bounds = collider.bounds;
            }
            else
            {
                bounds.Encapsulate(collider.bounds);
            }
        }

        Vector3 localCenter = bounds.center - go.transform.position;
        bounds.center = localCenter;
        go.transform.rotation = currentRotation;
        go.transform.localScale = currentLocalScale;

        return bounds;
    }
}
