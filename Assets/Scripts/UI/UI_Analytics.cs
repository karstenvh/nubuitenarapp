﻿using Firebase.Analytics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;

namespace VROwl.UI.Analytics
{
    public class UI_Analytics : MonoBehaviour
    {
        [HideInInspector]
        public UI_Manager UI;

        private bool firebaseReady = false;

        public void Initiate(UI_Manager ui)
        {
            UI = ui;
            Initialize();
        }

        public void Initialize()
        {
            if (UI.Modules.HasFirebase)
            {
                Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                    var dependencyStatus = task.Result;
                    if (dependencyStatus == Firebase.DependencyStatus.Available)
                    {
                        print("Ready to use firebase");
                        firebaseReady = true;
                    }
                    else
                    {
                        Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    }
                });
            }
        }

        void OnApplicationQuit()
        {
            StopMeasuringUsage();
        }

        private bool FirebaseReady()
        {
            return firebaseReady && UI.Modules.HasFirebase;
        }

        public void StopMeasuringUsage()
        {
            if (FirebaseReady() && UI.Modules.LogUsageDuration)
            {
                FirebaseAnalytics.LogEvent("app_usage", "duration", Time.time);
            }
        }

        public void ProductAddedToWishlist(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAddToWishlist, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void ProductRemovedFromWishlist(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAddToWishlist, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void ProductAddedToShoppingCart(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAddToCart, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void ProductRemovedFromShoppingCart(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventRemoveFromCart, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void ProductAddedToQuotation(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAddToCart, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void ProductRemovedFromQuotation(UI_Model_Product product)
        {
            if (FirebaseReady() && UI.Modules.LogProducts)
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventRemoveFromCart, new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void Login()
        {
            if(FirebaseReady() && UI.Modules.LogLogin)
            {
                FirebaseAnalytics.LogEvent("register");
            }
        }

        public void Register()
        {
            if (FirebaseReady() && UI.Modules.LogLogin)
            {
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
            }
        }
        
        public void GoToPage(PageID page)
        {
            if (FirebaseReady())
            {
                FirebaseAnalytics.LogEvent("navigation","pagename", page.ToString());
            }
        }

        public void ViewProduct(UI_Model_Product product)
        {
            if (FirebaseReady())
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent("product_viewed", new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        public void PlaceProduct(UI_Model_Product product)
        {
            GoToAR();
            if (FirebaseReady())
            {
                if (product == null || product.Name == null)
                    return;
                FirebaseAnalytics.LogEvent("product_placed", new Parameter[]
                {
                    new Parameter(FirebaseAnalytics.ParameterItemId, product.ID),
                    new Parameter(FirebaseAnalytics.ParameterItemName, product.Name),
                    new Parameter(FirebaseAnalytics.ParameterCampaign, product.Promotion.ToString()),
                });
            }
        }

        private float ArOpened = 0;

        public void GoToAR()
        {
            if(ArOpened == 0)
                ArOpened = Time.time;
        }

        public void LeaveAR()
        {
            float artime = Time.time - ArOpened;
            ArOpened = 0;
            if (FirebaseReady())
            {
                FirebaseAnalytics.LogEvent("time_in_ar", "duration", artime);
            }
        }
    }
}