﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    public class UI_Popup_GenericYesNo : UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.GENERIC_YES_NO; } }

        public Text YesText, NoText, Content;
        public Action Yes, No;

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }

        public void Open(Action yesCallback, Action noCallback, string content, string yesText = "Ja", string noText = "Nee")
        {
            Yes = yesCallback;
            No = noCallback;
            Content.text = content;
            YesText.text = yesText;
            NoText.text = noText;
        }

        public void YesClicked()
        {
            if (Yes != null)
                Yes();
            UI.ClosePopup();
        }

        public void NoClicked()
        {
            if (No != null)
                No();
            UI.ClosePopup();
        }
    }
}