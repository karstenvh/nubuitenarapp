﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    public class UI_Popup_ConnectionIssue : UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.CONNECTIONISSUE; } }

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }
    }
}
