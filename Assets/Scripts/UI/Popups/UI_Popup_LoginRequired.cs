﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;

namespace VROwl.UI.Popups
{
    public class UI_Popup_LoginRequired: UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.INLOGREQUIRED; } }

        public Action YesClicked, NoClicked;

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }

        public void Yes()
        {
            if (YesClicked != null)
            {
                YesClicked();
            }
        }

        public void No()
        {
            if (NoClicked != null)
            {
                NoClicked();
            }
        }
    }
}