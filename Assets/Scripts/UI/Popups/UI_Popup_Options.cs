﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI
{
    public class UI_Popup_Options : UI_Popup
    {
        public Text Title;
        public UI_Component_PopupOption OptionPrefab;

        public GameObject OptionContainer;

        private List<Text> texts = new List<Text>();
        private List<UI_Component_PopupOption> optionComponents = new List<UI_Component_PopupOption>();

        public Action OkCallback;
        private UI_Model_ProductOption productOption;

        protected override PopupID Popup { get { return PopupID.OPTIONSELECTION; } }

        private void Start()
        {
            OptionPrefab.gameObject.SetActive(false);
        }

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }

        public void Fill(UI_Model_ProductOption options, Action ok = null)
        {
            productOption = options;
            OkCallback = ok;
            foreach (Text text in texts)
            {
                Destroy(text.gameObject);
            }
            foreach (UI_Component_PopupOption option in optionComponents)
            {
                Destroy(option.gameObject);
            }
            texts.Clear();
            optionComponents.Clear();

            int index = 0;
            foreach (UI_Model_ProductOptionEntry option in options.Options)
            {
                int choice = index;
                UI_Component_PopupOption opt = Instantiate(OptionPrefab);
                opt.Fill(option, (e) => { SelectEntry(choice); }, options);
                optionComponents.Add(opt);
                opt.gameObject.SetActive(true);
                opt.transform.SetParent(OptionContainer.transform);
                opt.transform.localScale = OptionPrefab.transform.localScale;
                index++;
            }
        }

        private void SelectEntry(int index)
        {
            print(index + " clicked");
            productOption.Choice = index;
            Confirm();
        }

        public void Confirm()
        {
            if (OkCallback != null)
                OkCallback();
            UI.ClosePopup();
        }
    }
}
