﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    public class UI_Popup_ColorSelection : UI_Popup
    {
        public Text TitlePrefab;
        public UI_Component_ColorOption OptionPrefab;

        public GameObject OptionContainer;

        private List<Text> texts = new List<Text>();
        private List<UI_Component_ColorOption> optionComponents = new List<UI_Component_ColorOption>();

        public Action OkCallback;

        protected override PopupID Popup { get { return PopupID.COLORSELECTION; } }

        private void Start()
        {
            TitlePrefab.gameObject.SetActive(false);
            OptionPrefab.gameObject.SetActive(false);
        }

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }

        public void Fill(List<UI_ColorOption> options, Action ok = null)
        {
            OkCallback = ok;
            foreach(Text text in texts)
            {
                Destroy(text.gameObject);
            }
            foreach(UI_Component_ColorOption option in optionComponents)
            {
                Destroy(option.gameObject);
            }
            texts.Clear();
            optionComponents.Clear();

            foreach(UI_ColorOption option in options)
            {
                Text text = Instantiate(TitlePrefab);
                text.text = option.Name;
                texts.Add(text);
                text.gameObject.SetActive(true);
                text.transform.SetParent(OptionContainer.transform);
                text.transform.localScale = TitlePrefab.transform.localScale;

                UI_ColorSelection first = null;
                bool foundSelection = false;

                foreach (UI_ColorSelection select in option.Selection)
                {
                    UI_Component_ColorOption opt = Instantiate(OptionPrefab);
                    opt.Fill(select, !option.MultiSelect, option.ToggleSelection);
                    optionComponents.Add(opt);
                    opt.gameObject.SetActive(true);
                    opt.transform.SetParent(OptionContainer.transform);
                    opt.transform.localScale = OptionPrefab.transform.localScale;

                    if (first != null && !option.MultiSelect)
                    {
                        first = select;
                    }

                    if (select.Selected)
                    {
                        foundSelection = true;
                    }
                }

                if (!foundSelection && first != null)
                {
                    first.Selected = true;
                }
            }
        }

        public void Confirm()
        {
            if (OkCallback != null)
                OkCallback();
            UI.ClosePopup();
        }
    }
}
