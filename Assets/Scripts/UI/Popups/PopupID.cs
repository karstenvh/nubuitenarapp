﻿namespace VROwl.UI
{
    /// <summary>
    /// The state identifier for the ui manager.
    /// </summary>
    public enum PopupID
    {
        NONE, ADDEDTOCART, ARWARNING, INLOGREQUIRED, DATAUSAGE, DEVICESUPPORT, COLORSELECTION, OPTIONSELECTION, CONNECTIONISSUE, GENERIC_YES_NO, GENERIC_OK
    }
}