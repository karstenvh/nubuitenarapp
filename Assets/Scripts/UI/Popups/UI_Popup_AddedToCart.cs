﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;

namespace VROwl.UI.Popups
{
    public class UI_Popup_AddedToCart : UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.ADDEDTOCART; } }
        
        public override void InitiateOverlay()
        {
        }

        public void Open(UI_Model_Product product)
        {

        }

        public override void RefreshPopup()
        {
        }
    }
}