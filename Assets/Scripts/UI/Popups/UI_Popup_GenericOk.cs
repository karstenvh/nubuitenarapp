﻿using System;
using UnityEngine.UI;

namespace VROwl.UI
{
    public class UI_Popup_GenericOk : UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.GENERIC_OK; } }

        public Text OkText, Content;
        private Action Ok;

        public override void InitiateOverlay() 
        {
        }

        public override void RefreshPopup()
        {
        }

        public void Open(Action okCallback, string content, string okText = "Ok")
        {
            Ok = okCallback;
            Content.text = content;
            OkText.text = okText;
        }

        public void OkClicked()
        {
            if (Ok != null)
                Ok();
            UI.ClosePopup();
        }
    }
}