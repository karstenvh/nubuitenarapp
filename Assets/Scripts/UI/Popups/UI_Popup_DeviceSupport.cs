﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;
using VROwl.UI.Pages;

namespace VROwl.UI.Popups
{
    public class UI_Popup_DeviceSupport : UI_Popup
    {
        protected override PopupID Popup { get { return PopupID.DEVICESUPPORT; } }

        public override void InitiateOverlay()
        {
        }

        public override void RefreshPopup()
        {
        }

        public void OkClicked()
        {
            UI_Manager.instance.GetPage<UI_Page_Home>().DeviceSupportAccepted();
        }
    }
}