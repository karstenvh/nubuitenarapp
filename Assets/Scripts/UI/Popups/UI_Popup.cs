﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    public abstract class UI_Popup : MonoBehaviour
    {
        // The popup identifier, used to decide when this popup is visible.
        protected abstract PopupID Popup { get; }
        public PopupID ID { get { return Popup; } }

        // If this overlay is active. The only info we need for custom page logic is if this page is shown or not.
        protected bool IsCurrentPopup = false;

        protected UI_Manager UI;

        private bool? prevstate = null;
        private UI_Component[] components;


        /// <summary>
        /// Initiate is called when the UI is loaded.
        /// /// </summary>
        public void Initiate(UI_Manager manager)
        {
            components = GetComponentsInChildren<UI_Component>(true);
            if (components != null && components.Length > 0)
                foreach (UI_Component component in components)
                    component.Initiate();
            UI = manager;
            InitiateOverlay();
        }

        /// <summary>
        /// The abstract method to initiate a page. Allowing for custom initiation implementation for each page.
        /// </summary>
        public abstract void InitiateOverlay();

        /// <summary>
        /// Refresh this popup. 
        /// </summary>
        /// <param name="state">The current state of the UI.</param>
        public void Refresh(PopupID state)
        {
            bool newState = state == Popup;
            //If this page is inactive and it already was inactive the page will not refresh. Unless it's at start up; that's where prevstate comes in.
            if (newState == IsCurrentPopup && !newState && prevstate != null) return;
            prevstate = IsCurrentPopup;
            IsCurrentPopup = newState;
            gameObject.SetActive(IsCurrentPopup);
            if (components != null && components.Length > 0 && IsCurrentPopup)
            {
                foreach (UI_Component component in components)
                {
                    if (component != null)
                    {
                        component.Refresh();
                    }
                }
            }
            RefreshPopup();
        }

        /// <summary>
        /// The abstract method to refresh a popup. Allowing for custom refresh implementation for each popup.
        /// </summary>
        public abstract void RefreshPopup();
    }
}