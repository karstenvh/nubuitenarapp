﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;

namespace VROwl.UI
{
    [CustomEditor(typeof(UI_Stylesheet)), CanEditMultipleObjects]
    public class UI_StylesheetEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if(GUILayout.Button("Apply Stylesheet"))
            {
                UI_Manager ui = FindObjectOfType<UI_Manager>();
                if (ui == null)
                {
                    Debug.LogWarning("No UI manager found!");
                }
                else
                {
                    foreach (UI_Color color in ui.GetComponentsInChildren<UI_Color>(true))
                    {
                        UI_Stylesheet sheet = target as UI_Stylesheet;
                        color.Apply(sheet);
                        EditorUtility.SetDirty(color);
                    }
                    foreach (UI_Font font in ui.GetComponentsInChildren<UI_Font>(true))
                    {
                        UI_Stylesheet sheet = target as UI_Stylesheet;
                        font.Apply(sheet);
                        EditorUtility.SetDirty(font);
                    }
                }
            }
        }
    }
}
#endif