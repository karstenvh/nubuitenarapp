﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    /// <summary>
    /// This abstract class contains the basic setup for each ui component.
    /// </summary>
    public abstract class UI_Component : MonoBehaviour
    {
        public delegate void ComponentLoadedCallback(bool succes);

        /// <summary>
        /// Refresh this page. 
        /// </summary>
        public void Refresh()
        {
            RefreshComponent();
        }
        
        /// <summary>
        /// The abstract method to refresh a page. Allowing for custom refresh implementation for each page. Check the bool Active to see if the page is visible.
        /// </summary>
        public abstract void RefreshComponent();

        /// <summary>
        /// Initiate is called when the UI is loaded. Overrideable.
        /// </summary>
        public virtual void Initiate()
        {
            RefreshComponent();
        }
        
        /// <summary>
        /// The abstract method to load a page.
        /// </summary>
        public virtual void LoadComponent(ComponentLoadedCallback loadCallback)
        {
            loadCallback(true);
        }
    }
}
