﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    public class UI_Component_TutorialAR : UI_Component
    {
        private bool Seen = false;

        private int step = -1;
        public GameObject[] steps;

        public override void RefreshComponent()
        {
            gameObject.SetActive(!Seen);
        }

        private void Start()
        {
            Next();
        }


        public void Next()
        {
            step++;
            for(int i = 0; i < steps.Length; i++)
            {
                steps[i].SetActive(i == step);
            }
            Seen = step >= steps.Length;
            if (Seen)
                gameObject.SetActive(false);
        }
    }
}