﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI
{
    public class UI_Component_PopupOption : UI_Component
    {
        public Text ProductName, ProductPrice;

        private Action<UI_Model_ProductOptionEntry> callback;
        private UI_Model_ProductOptionEntry selection;

        public void Clicked()
        {
            if (callback != null)
                callback(selection);
        }

        private void Update()
        {
        }

        public void Fill(UI_Model_ProductOptionEntry entry, Action<UI_Model_ProductOptionEntry> onClick, UI_Model_ProductOption option)
        {
            ProductPrice.text = "+ € " + entry.Price;
            ProductName.text = entry.Name;
            selection = entry;
            callback = onClick;
        }

        public override void RefreshComponent()
        {
        }        
    }
}