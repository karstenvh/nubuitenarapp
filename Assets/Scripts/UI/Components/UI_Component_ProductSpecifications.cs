﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    [RequireComponent(typeof(RectTransform))]
    public class UI_Component_ProductSpecifications : UI_Component
    {
        public Text HeaderText;
        public GameObject SpecificationsContainer;
        public SpecificationLine SpecificationLinePrefab;
        public Color rowColorGray;

        private bool specificationsVisible;
        private bool updateHeight = false;

        private List<SpecificationLine> specificationTexts = new List<SpecificationLine>();

        private void Start()
        {
            SpecificationLinePrefab.gameObject.SetActive(false);
        }

        public override void RefreshComponent()
        {
        }

        public void Fill(Dictionary<string, string> specifications)
        {
            foreach (SpecificationLine specification in specificationTexts)
            {
                Destroy(specification.gameObject);
            }
            specificationTexts.Clear();
            gameObject.SetActive(specifications != null && specifications.Count > 0);

            short i = 0;
            foreach(KeyValuePair<string, string> specification in specifications)
            {
                if (string.IsNullOrEmpty(specification.Value)) continue;

                SpecificationLine specificationLine = Instantiate(SpecificationLinePrefab);
                specificationLine.Initialize(++i % 2 == 0 ? Color.white : rowColorGray, specification.Key + ": " + specification.Value);
                specificationLine.gameObject.SetActive(true);
                specificationLine.transform.SetParent(SpecificationsContainer.transform);
                specificationLine.transform.localScale = SpecificationLinePrefab.transform.localScale;
                specificationTexts.Add(specificationLine);
            }
            Open();
        }

        public void Clicked()
        {
            specificationsVisible = !specificationsVisible;
            foreach(SpecificationLine specification in specificationTexts)
            {
                specification.gameObject.SetActive(specificationsVisible);
            }
            if (specificationsVisible)
            {
                HeaderText.text = "Algemene specificaties ▼";
            }
            else
            {
                HeaderText.text = "Algemene specificaties ▲";
            }
            updateHeight = true;
        }

        public void Open()
        {
            specificationsVisible = false;
            Clicked();
        }

        public void Close()
        {
            specificationsVisible = true;
            Clicked();
        }
    }
}
