﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_ARShoppingItem : UI_Component
    {
        // The callback delegate; gives modular control over the scroll items without parent references.
        public delegate void OnItemClickEvent(UI_Model_Product product);
        public OnItemClickEvent OnClickEvent;

        public Image Image;
        public Text Title;

        private UI_Model_Product product;

        public override void RefreshComponent()
        {
        }

        public void Fill(UI_Model_Product product)
        {
            this.product = product;
            Title.text = product.Name;
            if (!string.IsNullOrEmpty(product.IconURL)) {
                UI_Helper.Helper.LoadImage(Image, product.IconURL);
            }
        }

        public void Clicked()
        {
            if (product != null && OnClickEvent != null)
                OnClickEvent(product);
        }
    }
}
