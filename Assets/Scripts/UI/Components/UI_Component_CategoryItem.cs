﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    /// <summary>
    /// This component is a single item in the category component. Also used for filters.
    /// </summary>
    public class UI_Component_CategoryItem : UI_Component
    {
        [Header("UI Object references")]
        public Image Image;
        public Text Name;

        [HideInInspector]
        public UI_Model_Category Category;

        // The callback delegate; gives modular control over the scroll items without parent references.
        public delegate void OnItemClickEvent(UI_Component_CategoryItem item);
        public OnItemClickEvent OnClickEvent;

        /// <summary>
        /// Refreshes this component.
        /// </summary>
        public override void RefreshComponent()
        {
        }

        /// <summary>
        /// Initiate should be called when the object gets created.
        /// </summary>
        public override void Initiate()
        {
        }

        /// <summary>
        /// The item click event. Used by UI button event.
        /// </summary>
        public void ItemClicked()
        {
            if (OnClickEvent != null)
                OnClickEvent(this);
        }
        
        /// <summary>
        /// Fill the item with category data.
        /// </summary>
        /// <param name="category"></param>
        public void Fill(UI_Model_Category category)
        {
            Category = category;
            Name.text = category.Name;
            UI_Helper.Helper.LoadImage(Image, category.ImageURL);
        }
    }
}
