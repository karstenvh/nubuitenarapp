﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_SiblingCategories : UI_Component
    {
        public UI_Component_SiblingCategory SiblingCategoryPrefab;
        public GameObject SiblingCategoryContainer;

        private List<UI_Component_SiblingCategory> siblingItems = new List<UI_Component_SiblingCategory>();

        public override void RefreshComponent()
        {
        }

        public void Fill(List<UI_Model_Category> categories)
        {
            foreach(UI_Component_SiblingCategory oldCategory in siblingItems)
            {
                Destroy(oldCategory.gameObject);
            }
            siblingItems = new List<UI_Component_SiblingCategory>();
            foreach (UI_Model_Category category in categories)
            {
                UI_Component_SiblingCategory newCategory = Instantiate(SiblingCategoryPrefab);
                newCategory.Fill(category);     
                newCategory.gameObject.SetActive(true);
                newCategory.transform.SetParent(SiblingCategoryContainer.transform);
                newCategory.transform.localScale = SiblingCategoryPrefab.transform.localScale;
                siblingItems.Add(newCategory);
            }
            SiblingCategoryPrefab.gameObject.SetActive(false);
        }
    }
}
