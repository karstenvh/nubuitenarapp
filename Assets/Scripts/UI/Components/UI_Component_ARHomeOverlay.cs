﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_ARHomeOverlay : UI_Component
    {
        public GameObject QuotationHeader;
        public UI_Component_ARShoppingResults QuotationContainer;

        public GameObject ShoppinCartHeader;
        public UI_Component_ARShoppingResults ShoppingCartContainer;

        public GameObject WishlistHeader;
        public UI_Component_ARShoppingResults WishlistContainer;

        public GameObject CategoryContainer;
        private List<UI_Component_Category> categoryItems;
        
        
        public Action<UI_Model_Category> CategorySelectedCallback;

        public override void RefreshComponent()
        {
            QuotationHeader.SetActive(UI_Manager.instance.IsQuotationVisible() && UI_Manager.instance.GetProductsInQuotation().Count > 0);
            QuotationContainer.gameObject.SetActive(UI_Manager.instance.IsQuotationVisible() && UI_Manager.instance.GetProductsInQuotation().Count > 0);
            ShoppinCartHeader.SetActive(UI_Manager.instance.IsShoppingCartVisible());
            ShoppingCartContainer.gameObject.SetActive(UI_Manager.instance.IsShoppingCartVisible());
            WishlistHeader.SetActive(UI_Manager.instance.IsFavoriteVisible());
            WishlistContainer.gameObject.SetActive(UI_Manager.instance.IsFavoriteVisible());
        }

        private IEnumerator LateEnable()
        {
            //QuotationContainer.gameObject.SetActive(false);
            ShoppingCartContainer.gameObject.SetActive(false);
            WishlistContainer.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            //QuotationContainer.gameObject.SetActive(true);
            ShoppingCartContainer.gameObject.SetActive(true);
            WishlistContainer.gameObject.SetActive(true);
            yield return null;
        }

        public override void LoadComponent(ComponentLoadedCallback loadCallback)
        {
            UI_Manager.instance.LoadCatalogue((catalogue) =>
            {
                if (catalogue != null)
                {
                    List<UI_Model_Category> categories = catalogue.Categories;
                    FillCategories(categories);
                    if (UI_Manager.instance.IsFavoriteVisible())
                    {
                        WishlistContainer.Loading();
                        UI_Manager.instance.LoadFavorites((favorites) => FillFavorite(favorites));
                    }
                    if (UI_Manager.instance.IsQuotationVisible())
                    {
                        QuotationContainer.Loading();
                        UI_Manager.instance.LoadQuotation((quotation) => FillQuotation(quotation));
                    }
                    if (UI_Manager.instance.IsShoppingCartVisible())
                    {
                        ShoppingCartContainer.Loading();
                        UI_Manager.instance.LoadShoppingCart((cart) => FillShoppingCart(cart));
                    }
                    loadCallback(true);
                }
                else
                {
                    loadCallback(false);
                }
            });
        }

        public void FillQuotation(Dictionary<UI_Model_Product, int> quotation)
        {
            List<UI_Model_Product> products = new List<UI_Model_Product>();
            if (quotation != null)
            {
                foreach (KeyValuePair<UI_Model_Product, int> keyvalue in quotation)
                {
                    products.Add(keyvalue.Key);
                }
            }
            QuotationContainer.Fill(products);
            QuotationContainer.DoneLoading();
        }

        public void FillShoppingCart(Dictionary<UI_Model_Product, int> cart)
        {
            List<UI_Model_Product> products = new List<UI_Model_Product>();
            if (cart != null)
            {
                foreach (KeyValuePair<UI_Model_Product, int> keyvalue in cart)
                {
                    products.Add(keyvalue.Key);
                }
            }
            ShoppingCartContainer.Fill(products);
            ShoppingCartContainer.DoneLoading();

            StartCoroutine(LateEnable());
        }

        public void FillFavorite(List<UI_Model_Product> favorites)
        {
            WishlistContainer.Fill(favorites);
            WishlistContainer.DoneLoading();

            StartCoroutine(LateEnable());
        }

        public void FillCategories(List<UI_Model_Category> categories)
        {
            if (categoryItems != null)
            {
                foreach (UI_Component_Category oldCategory in categoryItems)
                    Destroy(oldCategory.gameObject);
            }

            if (categoryItems != null)
            {
                foreach (UI_Component_Category oldCategory in categoryItems)
                    Destroy(oldCategory.gameObject);
            }

            categoryItems = new List<UI_Component_Category>();
            if (CategoryContainer != null)
            {
                for (int i = 0; i < categories.Count; i++)
                {
                    UI_Model_Category category = categories[i];
                    if (category == null || category.SubCategories == null) continue;
                    if (category.SubCategories.Count <= 0) continue;
                    UI_Component_Category categoryComponent = Instantiate(UI_Manager.instance.CategoryPrefab);
                    categoryComponent.OnClickEvent = ItemClicked;
                    categoryComponent.transform.SetParent(CategoryContainer.transform);
                    categoryComponent.name = UI_Constants.PanelPrefix + category.Name;
                    categoryComponent.gameObject.SetActive(true);
                    categoryComponent.transform.localScale = UI_Manager.instance.CategoryPrefab.transform.localScale;
                    categoryComponent.Fill(category);
                    categoryItems.Add(categoryComponent);
                }
            }
            UI_Manager.instance.CategoryPrefab.gameObject.SetActive(false);
        }
        
        public void ItemClicked(UI_Component_CategoryItem item)
        {
            if (CategorySelectedCallback != null)
            {
                CategorySelectedCallback(item.Category);
            }
        }
    }
}
