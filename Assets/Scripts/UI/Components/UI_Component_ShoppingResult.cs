﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;
using System.Linq;

namespace VROwl.UI.Components
{
    public class UI_Component_ShoppingResult : UI_Component
    {
        public int AmountOfTiles = 5;
        public UI_Component_ShoppingItem ItemPrefab;
        public GameObject ResultContainer;
        public UI_Component_PageNumbers PageNumbers;
        public GameObject NumbersContainer;
        public GameObject NoResultsFound;
        public GameObject LoadingContainer;
        public UI_Component_SendShopping SendContainer;
        private List<UI_Component_ShoppingItem> items;
        private Dictionary<UI_Model_Product, int> result;
        private int currentPage;
        private int pages;
		private int amountOfResultsOnPage;
        public ScrollRect ContentScrollView;

        private bool cart;
        private bool loading;
        public UI_Component_ResultItem.OnItemClickEvent OnClickEvent;
        public override void Initiate()
        {
            items = new List<UI_Component_ShoppingItem>();
            if (ItemPrefab != null)
            {
                for (int i = 0; i < AmountOfTiles; i++)
                {
                    UI_Component_ShoppingItem newItem = Instantiate(ItemPrefab);
                    newItem.SetNormal();
                    newItem.gameObject.SetActive(true);
                    newItem.transform.SetParent(ResultContainer.transform);
                    newItem.transform.localScale = ItemPrefab.transform.localScale;
                    newItem.name = UI_Constants.PanelPrefix + "ResultContainer" + (i + 1);
                    newItem.OnClickEvent = ItemClicked;
                    items.Add(newItem);
                }
                ItemPrefab.gameObject.SetActive(false);
            }
        }

        public override void RefreshComponent()
        {
            foreach (UI_Component_ShoppingItem item in items)
            {
                if (item != null)
                {
                    item.Refresh();
                }
            }
        }

        public void Loading()
        {
            loading = true;
            NoResultsFound.SetActive(false);
            ResultContainer.SetActive(false);
            NumbersContainer.SetActive(false);
            LoadingContainer.SetActive(true);
            if (SendContainer != null)
            {
                SendContainer.gameObject.SetActive(false);
            }
            SetHeightToChildren();
        }

        public void DoneLoading()
        {
            loading = false;
            ResultContainer.SetActive(true);
            LoadingContainer.SetActive(false);
            SetHeightToChildren();
        }

        public void Refill()
        {
            int page = currentPage;
            Fill(result, cart);
            if (page > pages - 1)
                page--;
            SetPage(page);
        }

        public void Fill(Dictionary<UI_Model_Product, int> results, bool shoppingCart)
        {
            cart = shoppingCart;
            if(SendContainer != null)
                SendContainer.ShoppingCart = cart;
            result = results;
            int amount = (result.Count - 1);
            if (amount < 0)
                amount = 0;
            pages = amount / AmountOfTiles + 1;
            PageNumbers.SetAmountOfPages(pages);
            NumbersContainer.SetActive(pages > 0);
            NoResultsFound.SetActive(results.Count == 0);
            if(SendContainer != null)
            {
                SendContainer.ShoppingCart = cart;
                SendContainer.gameObject.SetActive(results.Count > 0);
            }

            SetPage(0);
        }

        public void NextPage()
        {
            SetPage(currentPage + 1);
        }

        public void PreviousPage()
        {
            SetPage(currentPage - 1);
        }

        public void SetPage(int page)
        {
            currentPage = Mathf.Clamp(page, 0, pages);
            PageNumbers.SetPage(currentPage);
            amountOfResultsOnPage = result.Count - page * AmountOfTiles;
            amountOfResultsOnPage = Mathf.Clamp(amountOfResultsOnPage, 0, AmountOfTiles);
            if (ResultContainer != null)
            {
                RectTransform resultRect = ResultContainer.GetComponent<RectTransform>();
                VerticalLayoutGroup resultLayout = ResultContainer.GetComponent<VerticalLayoutGroup>();
                if (resultRect != null && resultLayout != null)
                {
                    resultRect.sizeDelta = new Vector2(resultRect.sizeDelta.x, resultLayout.padding.top + resultLayout.padding.bottom + 340 * amountOfResultsOnPage + amountOfResultsOnPage * resultLayout.spacing);
                }
                SetHeightToChildren();
            }
            for (int i = 0; i < AmountOfTiles; i++)
            {
                if (i >= items.Count) break;
                UI_Component_ShoppingItem item = items[i];
                item.gameObject.SetActive(i < amountOfResultsOnPage);
                int itemIndex = i + page * AmountOfTiles;
                List<UI_Model_Product> products = result.Keys.ToList();
                if (itemIndex < result.Count)
                {
                    SetItemToResult(item, products[itemIndex]);
                }
            }
            ContentScrollView.normalizedPosition = new Vector2(ContentScrollView.normalizedPosition.x, 1);
        }

        public void SetHeightToChildren()
        {
            RectTransform rect = GetComponent<RectTransform>();
			if (rect != null)
            {
                float height = 0;
                if (amountOfResultsOnPage == 0) 
				{
                    if (loading)
                    {
                        height = LoadingContainer.GetComponent<RectTransform>().sizeDelta.y + 180;

                    }
                    else
                    {
                        height = NoResultsFound.GetComponent<RectTransform>().sizeDelta.y + 180;
                    }
				} 
				else 
				{
					height = ResultContainer.GetComponent<RectTransform> ().sizeDelta.y + 180;
                    if (SendContainer != null)
                        height += SendContainer.GetComponent<RectTransform>().sizeDelta.y;
                }
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
            }
        }

        public void SetItemToResult(UI_Component_ShoppingItem item, UI_Model_Product result)
        {
            item.Fill(result, Refill, cart);
        }

        private void ItemClicked(UI_Component_ResultItem item)
        {
            if (OnClickEvent != null)
                OnClickEvent(item);
        }
    }
}
