﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_ARResultOverlay : UI_Component
    {
        public Text TitleText;
        public UI_Component_Result ResultsContainer;

        [HideInInspector]
        public Stack<UI_Model_Category> PreviousCategories = new Stack<UI_Model_Category>();
        private UI_Component_Category FilterContainter;

        public Action BackToHome;

        public override void Initiate()
        {
            FilterContainter = Instantiate(UI_Manager.instance.CategoryPrefab);
            FilterContainter.name = UI_Constants.PanelPrefix + "Filters";
            FilterContainter.transform.SetParent(transform);
            FilterContainter.transform.SetSiblingIndex(2);
            FilterContainter.transform.localScale = UI_Manager.instance.CategoryPrefab.transform.localScale;
            FilterContainter.gameObject.SetActive(true);
            FilterContainter.OnClickEvent = AddFilter;
            FilterContainter.RefreshComponent();

            ResultsContainer.OnClickEvent = ItemClicked;
        }

        public override void RefreshComponent()
        {
        }

        public void Fill(UI_Model_Category category)
        {
            PreviousCategories.Push(category);
            TitleText.text = category.Name;
            FilterContainter.gameObject.SetActive(category.SubCategories != null && category.SubCategories.Count > 0);
            FilterContainter.Fill(category);
            ResultsContainer.Loading();
            UI_Manager.instance.LoadCategoryProducts(
                (ps) =>
                {
                    ResultsContainer.Fill(ps);
                    ResultsContainer.DoneLoading();
                }, category.ID);
        }

        public void AddFilter(UI_Component_CategoryItem filter)
        {
            Fill(filter.Category);
        }

        public void GoBack()
        {
            if (PreviousCategories.Count <= 1)
            {
                if (BackToHome != null)
                {
                    BackToHome();
                    PreviousCategories.Clear();
                }
            }
            else
            {
                PreviousCategories.Pop();
                UI_Model_Category previousCategory = PreviousCategories.Pop();
                Fill(previousCategory);
            }
        }

        public void ItemClicked(UI_Component_ResultItem item)
        {
            UI_Manager.instance.OpenProductOverlay(item.Product);
        }
    }
}
