﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_ProductOptions : UI_Component
    {
        public Text OptionText;
        public GridLayoutGroup Grid;
        public UI_Component_ProductOption OptionPrefab;
        private List<UI_Component_ProductOption> options = new List<UI_Component_ProductOption>();
        
        public override void RefreshComponent()
        {
        }

        private void Update()
        {
            
        }

        public void Fill(UI_Model_Product product)
        {
            foreach (UI_Component_ProductOption option in options.ToArray())
            {
                Destroy(option.gameObject);
            }
            options = new List<UI_Component_ProductOption>();
            if (OptionText != null)
            {
                OptionText.gameObject.SetActive(product.Options.Count > 0);
                gameObject.SetActive(product.Options.Count > 0);
            }
            foreach (UI_Model_ProductOption opt in product.Options)
            {
                UI_Component_ProductOption newOption = Instantiate(OptionPrefab);
                newOption.transform.SetParent(Grid.transform);
                newOption.gameObject.SetActive(true);
                newOption.transform.localScale = OptionPrefab.transform.localScale;
                newOption.transform.rotation = OptionPrefab.transform.rotation;
                newOption.Fill(opt);
                options.Add(newOption);
            }
            int rows = ((product.Options.Count + 1) / Grid.constraintCount);
            RectTransform rect = Grid.GetComponent<RectTransform>();
            float height = rows * Grid.cellSize.y + (rows - 1) * Grid.spacing.y + Grid.padding.top + Grid.padding.bottom;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, Mathf.Clamp(height, 0, height));
            OptionPrefab.gameObject.SetActive(false);
        }

        public bool Check()
        {
            bool result = true;
            foreach(UI_Component_ProductOption option in options)
            {
                if(!option.Check())
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
