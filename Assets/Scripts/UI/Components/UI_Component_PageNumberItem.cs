﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    /// <summary>
    /// This component is a single page number button.
    /// </summary>
    public class UI_Component_PageNumberItem : UI_Component
    {
        [Header("UI Object references")]
        public Text NumberText;

        public Image TopBorder;
        public Image BottomBorder;
        public Image LeftBorder;
        public Image RightBorder;

        public UI_Component_Result Results;
        public UI_Component_ARShoppingResults ARShoppingResults;
        public UI_Component_ShoppingResult ShoppingResults;

        [Header("Animation settings")]
        public Color NormalColor;
        public Color SelectedColor;

        private int page;
        private bool dots = false;

        public override void RefreshComponent()
        {
        }
        
        public void SetNumber(int number, bool current)
        {
            page = number - 1;
            NumberText.text = number + "";
            SetBorderColor(current ? SelectedColor : NormalColor);
            dots = false;
        }

        public void SetDots()
        {
            NumberText.text = "...";
            SetBorderColor(NormalColor);
            dots = true;
        }
        
        public void SetBorderColor(Color color)
        {
            TopBorder.color = color;
            BottomBorder.color = color;
            LeftBorder.color = color;
            RightBorder.color = color;
        }

        public void Clicked()
        {
            if (!dots)
            {
                if(Results != null)
                    Results.SetPage(page);
                if (ShoppingResults != null)
                    ShoppingResults.SetPage(page);
                if (ARShoppingResults != null)
                    ShoppingResults.SetPage(page);
            }
        }
    }
}
