﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;
using VROwl.UI.Utils;

namespace VROwl.UI.Components
{
    public class UI_Component_ShoppingItem : UI_Component
    {
        [Header("UI Object references")]
        public Image Image;
        public Text TitleText;
        public Text BodyText;
        public Text ModelNumberText;
        public Text PriceText;
        public GameObject UnfavoriteButton, FavoriteButton, FavoriteSpinner;
        public InputField AmountField;
        public Button MinusButton, PlusButton;
        public GameObject RemoveButton, RemoveSpinner;

        [Header("Overlay Object references")]
        public GameObject OverlayContainer;
        public Text OverlayText;

        [Header("Outline Object references")]
        public Image PanelBorder;

        [Header("Settings")]
        public float NormalThickness, ActionThickness;

        // The callback delegate; gives modular control over the scroll items without parent references.
        public delegate void OnItemClickEvent(UI_Component_ResultItem item);
        public OnItemClickEvent OnClickEvent;

        [HideInInspector]
        public UI_Model_Product Product;

        private bool cart;
        private Action removed;

        public override void RefreshComponent()
        {
            UpdateAmount();

            UnfavoriteButton.SetActive(UI_Manager.instance.IsFavoriteVisible() && !UI_Manager.instance.IsFavorite(Product.ID));
            FavoriteButton.SetActive(UI_Manager.instance.IsFavoriteVisible() && UI_Manager.instance.IsFavorite(Product.ID));
            FavoriteSpinner.SetActive(false);

            RemoveButton.SetActive(true);
            RemoveSpinner.SetActive(false);
        }

        public void OpenOverlay()
        {
            UI_Manager.instance.OpenProductOverlay(Product);
        }

        public void SetActiveAmountInteraction(bool active)
        {
            MinusButton.interactable = active;
            PlusButton.interactable = active;
            AmountField.interactable = active;
        }

        public void SetBorderColor(Color color)
        {
            PanelBorder.color = color;
        }

        public void SetBorderThickness(float thickness)
        {
            //PanelBorder.rectTransform.sizeDelta = new Vector2(PanelBorder.rectTransform.sizeDelta.x, thickness);
        }

        public void SetNormal()
        {
            SetBorderColor(UI_Manager.instance.Stylesheet.ProductNormalColor);
            SetBorderThickness(NormalThickness);
            OverlayContainer.SetActive(false);
        }

        public void SetAction()
        {
            SetBorderColor(UI_Manager.instance.Stylesheet.ProductActionColor);
            SetBorderThickness(ActionThickness);
            OverlayContainer.SetActive(true);
            OverlayText.text = "Actie";
        }

        public void SetRecommended()
        {
            SetBorderColor(UI_Manager.instance.Stylesheet.ProductActionColor);
            SetBorderThickness(ActionThickness);
            OverlayContainer.SetActive(true);
            OverlayText.text = "Aanbevolen";
        }

        public void ToggleFavorite() {
            if (Product != null)
                SetFavorite(!UI_Manager.instance.IsFavorite(Product.ID));
        }

        private void SetFavorite(bool favorite) {
            if (UI_Manager.instance.LoggedIn()) {
                FavoriteSpinner.SetActive(true);
                UnfavoriteButton.SetActive(false);
                FavoriteButton.SetActive(false);

                if (favorite) {
                    UI_Manager.instance.AddToFavorites(Product,
                        added => {
                            FavoriteButton.SetActive(added);
                            Debug.Log("Add to favorites");
                            FavoriteSpinner.SetActive(false);
                        });
                }
                else {
                    UI_Manager.instance.RemoveFromFavorites(Product,
                        removed => {
                            UnfavoriteButton.SetActive(removed);
                            Debug.Log("Removed from favorites");
                            FavoriteSpinner.SetActive(false);
                        });
                }
            }
            else {
                UnfavoriteButton.SetActive(false);
                FavoriteButton.SetActive(false);
            }
        }

        public void SetDownloaded(bool downloaded)
        {
        }
        
        public void Fill(UI_Model_Product product, Action removedCallback, bool shoppingCart = false)
        {
            SetActiveAmountInteraction(true);
            cart = shoppingCart;
            Product = product;
            TitleText.text = product.Name;
            ModelNumberText.text = product.ModelNumber;
            BodyText.text = Utils.UI_Utils.ParseHTML(Product.ShortDescription);
            removed = removedCallback;

            // Check if the product is promoted; Action or Recommended.
            if (product.Promotion == UI_Model_ProductPromotion.Action)
                SetAction();
            else if (product.Promotion == UI_Model_ProductPromotion.Recommended)
                SetRecommended();
            else
                SetNormal();

            Image.sprite = null;
            UI_Helper.Helper.LoadImage(Image, product.IconURL);

            SetDownloaded(UI_Manager.instance.GetDownloadState(product) == UI_Model_ProductDownloadState.DOWNLOADED);

            UnfavoriteButton.SetActive(UI_Manager.instance.IsFavoriteVisible() && !UI_Manager.instance.IsFavorite(Product.ID));
            FavoriteButton.SetActive(UI_Manager.instance.IsFavoriteVisible() && UI_Manager.instance.IsFavorite(Product.ID));

            UpdateAmount();
        }
        
        public void IncreaseAmount()
        {
            if (UI_Manager.instance != null)
            {
                if (cart)
                {
                    SetActiveAmountInteraction(false);
                    UI_Manager.instance.AddToCart(Product, (result) => { SetActiveAmountInteraction(true); RefreshComponent(); });
                }
                else
                {
                    UI_Manager.instance.AddToQuotation(Product);
                }
            }
            UpdateAmount();
        }

        public void DecreaseAmount()
        {
            if (UI_Manager.instance != null)
            {
                if (cart)
                {
                    SetActiveAmountInteraction(false);
                    UI_Manager.instance.RemoveFromCart(Product, (result) => { SetActiveAmountInteraction(true); RefreshComponent(); }, 1);
                }
                else
                {
                    UI_Manager.instance.RemoveFromQuotation(Product, 1);
                }
            }
            UpdateAmount();
        }
        
        public void Remove()
        {
            if (UI_Manager.instance != null)
            {
                if (cart)
                {
                    RemoveButton.SetActive(false);
                    RemoveSpinner.SetActive(true);

                    SetActiveAmountInteraction(false);
                    UI_Manager.instance.RemoveFromCart(Product, (result) => {
                        SetActiveAmountInteraction(true);
                        RefreshComponent();
                    });
                }
                else
                {
                    UI_Manager.instance.RemoveFromQuotation(Product);
                }
            }
            UpdateAmount();
        }

        public void SetAmountToInput()
        {
            int amount = 0;
            if (int.TryParse(AmountField.text, out amount))
            {
                if (UI_Manager.instance != null)
                {
                    if (cart)
                    {
                        SetActiveAmountInteraction(false);
                        UI_Manager.instance.SetAmountInCart(Product, (result) => { SetActiveAmountInteraction(true); RefreshComponent(); }, amount);
                    }
                    else
                    {
                        UI_Manager.instance.SetAmountInQuotation(Product, amount);
                    }
                }
            }
            UpdateAmount();
        }

        public void UpdateAmount()
        {
            if (UI_Manager.instance != null)
            {
                int amount = 0;
                if (cart)
                {
                    if(UI_Manager.instance.IsInCart(Product))
                    {
                        amount = UI_Manager.instance.GetAmountInCart(Product);
                    }
                }
                else
                {
                    if (UI_Manager.instance.IsInQuotation(Product))
                    {
                        amount = UI_Manager.instance.GetAmountInQuotation(Product);
                    }
                }
                PriceText.text = "€" + UI_Utils.FormatPriceFromString(Product.Price) + " incl. btw";
                AmountField.text = amount + "";
                if (amount == 0 && removed != null)
                    removed();
            }
            else
            {
                AmountField.text = "0";
            }
        }
    }
}
