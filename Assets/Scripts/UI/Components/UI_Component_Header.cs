﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    /// <summary>
    /// This is the header component, visible on (almost) all pages.
    /// It is refreshed by the ui manager directly.
    /// </summary>
    public class UI_Component_Header : UI_Component
    {
        public GameObject TabletButtonContainer;
        public GameObject MobileButtonContainer;
        public GameObject FoldoutButton;
        [Header("UI Tablet Object references")]
        public Button LogInButtonTablet;
        public Text LogInTextTablet;
        public Button WishlistButtonTablet;
        public Text WishlistTextTablet;
        public Button CartButtonTablet;
        public Text CartTextTablet;
        public Button QuotationButtonTablet;
        public Text QuotationTextTablet;

        public Button MeasurementButtonTablet;

        [Header("UI Mobile Object references")]
        public Button HomeButtonMobile;
        public Button LogInButtonMobile;
        public Button WishlistButtonMobile;
        public Button CartButtonMobile;
        public Button QuotationButtonMobile;

        public Button MeasurementButtonMobile;

        private UI_Manager UI;
        private bool foldOut = false;

        /// <summary>
        /// Refreshes this component.
        /// </summary>
        public override void RefreshComponent()
        {
            if (UI == null)
                return;

            bool isPhone = UI_Manager.IsPhone();

            TabletButtonContainer.SetActive(!isPhone);
            foldOut = false;
            MobileButtonContainer.SetActive(false);
            FoldoutButton.SetActive(isPhone);

            if (UI.Modules.HasLogin)
            {
                LogInButtonTablet.gameObject.SetActive(!UI.LoggedIn());
                LogInButtonMobile.gameObject.SetActive(!UI.LoggedIn());
            }
            else
            {
                LogInButtonTablet.gameObject.SetActive(false);
                LogInButtonMobile.gameObject.SetActive(false);
            }
            //Tablet
            WishlistButtonTablet.gameObject.SetActive(UI.IsFavoriteVisible());
            CartButtonTablet.gameObject.SetActive(UI.IsShoppingCartVisible());
            QuotationButtonTablet.gameObject.SetActive(UI.IsQuotationVisible());
            MeasurementButtonTablet.gameObject.SetActive(UI.Modules.HasMeasurement);
            //Mobile
            WishlistButtonMobile.gameObject.SetActive(UI.IsFavoriteVisible());
            CartButtonMobile.gameObject.SetActive(UI.IsShoppingCartVisible());
            MeasurementButtonMobile.gameObject.SetActive(UI.Modules.HasMeasurement);
            QuotationButtonMobile.gameObject.SetActive(UI.IsQuotationVisible());
            
            /*
            if (UI.Stylesheet != null)
            {
                //Tablet
                LogInButtonTablet.targetGraphic.color = UI.Page == PageID.LOGINPAGE ? UI.Stylesheet.UserPageColor : UI.Stylesheet.HeaderIconColor;
                LogInTextTablet.color = UI.Page == PageID.LOGINPAGE ? UI.Stylesheet.UserPageColor : UI.Stylesheet.HeaderIconColor;

                WishlistButtonTablet.targetGraphic.color = UI.Page == PageID.WISHLISTPAGE ? UI.Stylesheet.WishlistColor : UI.Stylesheet.HeaderIconColor;
                WishlistTextTablet.color = UI.Page == PageID.WISHLISTPAGE ? UI.Stylesheet.WishlistColor : UI.Stylesheet.HeaderIconColor;

                CartButtonTablet.targetGraphic.color = UI.Page == PageID.SHOPPPINGCARTPAGE ? UI.Stylesheet.ShoppingCartColor : UI.Stylesheet.HeaderIconColor;
                CartTextTablet.color = UI.Page == PageID.SHOPPPINGCARTPAGE ? UI.Stylesheet.ShoppingCartColor : UI.Stylesheet.HeaderIconColor;

                QuotationButtonTablet.targetGraphic.color = UI.Page == PageID.QUOTATIONPAGE ? UI.Stylesheet.QuotationColor : UI.Stylesheet.HeaderIconColor;
                QuotationTextTablet.color = UI.Page == PageID.QUOTATIONPAGE ? UI.Stylesheet.QuotationColor : UI.Stylesheet.HeaderIconColor;

            }
            */
        }

        public void ToggleFoldOut()
        {
            foldOut = !foldOut;
            RefreshFoldOut();
        }

        public void CloseFoldOut()
        {
            foldOut = false;
            RefreshFoldOut();
        }

        public void OpenFoldOut()
        {
            foldOut = true;
            RefreshFoldOut();
        }

        private void RefreshFoldOut()
        {
            MobileButtonContainer.SetActive(UI_Manager.IsPhone() && foldOut);
        }
        
        /// <summary>
        /// Initiate should be called when the object gets created.
        /// </summary>
        public void Initiate(UI_Manager ui)
        {
            foldOut = false;
            UI = ui;
        }
    }
}
