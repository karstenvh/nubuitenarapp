﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_Result : UI_Component
    {
        public int AmountOfTiles = 10;
        public float TileHeight = 405;
        public UI_Component_ResultItem ItemPrefab;
        public GameObject ResultContainer;
        public GameObject LoadingContainer;
        public UI_Component_PageNumbers PageNumbers;
        public GameObject NumbersContainer;
        public GameObject NoResultsFound;
        public RectTransform ParentRect;
        private List<UI_Component_ResultItem> items;
        private List<UI_Model_Product> result;
        private int currentPage;
        private int pages;
		private int amountOfResultsOnPage;

        public UI_Component_ResultItem.OnItemClickEvent OnClickEvent;
        public override void Initiate()
        {
            items = new List<UI_Component_ResultItem>();
            if (ItemPrefab != null)
            {
                for (int i = 0; i < AmountOfTiles; i++)
                {
                    UI_Component_ResultItem newItem = Instantiate(ItemPrefab);
                    newItem.SetNormal();
                    newItem.gameObject.SetActive(true);
                    newItem.transform.SetParent(ResultContainer.transform);
                    newItem.transform.localScale = ItemPrefab.transform.localScale;
                    newItem.name = UI_Constants.PanelPrefix + "ResultContainer" + (i + 1);
                    newItem.OnClickEvent = ItemClicked;
                    items.Add(newItem);
                }
                ItemPrefab.gameObject.SetActive(false);
            }
        }

        public void Loading()
        {
            NoResultsFound.SetActive(false);
            ResultContainer.SetActive(false);
            NumbersContainer.SetActive(false);
            LoadingContainer.SetActive(true);
        }

        public void DoneLoading()
        {
            ResultContainer.SetActive(true);
            LoadingContainer.SetActive(false);
        }

        public override void RefreshComponent()
        {
            foreach(UI_Component_ResultItem item in items)
            {
                if (item != null)
                {
                    item.Refresh();
                }
            }
        }

        public void Fill(List<UI_Model_Product> results)
        {
            if(results == null)
                results = new List<UI_Model_Product>();
            result = results;
            int amount = (result.Count - 1);
            if (amount < 0)
                amount = 0;
            pages = amount / AmountOfTiles + 1;
            if (PageNumbers != null)
                PageNumbers.SetAmountOfPages(pages);
            if(NumbersContainer != null)
                NumbersContainer.SetActive(pages > 0);
            if(NoResultsFound != null)
                NoResultsFound.SetActive(results.Count == 0);
            if(ResultContainer != null)
                ResultContainer.SetActive(results.Count > 0);
            SetPage(0);
            DoneLoading();
        }

        public void NextPage()
        {
            SetPage(currentPage + 1);
        }

        public void PreviousPage()
        {
            SetPage(currentPage - 1);
        }

        public void SetPage(int page)
        {
            currentPage = Mathf.Clamp(page, 0, pages);
            PageNumbers.SetPage(currentPage);
            amountOfResultsOnPage = result.Count - page * AmountOfTiles;
            amountOfResultsOnPage = Mathf.Clamp(amountOfResultsOnPage, 0, AmountOfTiles);
            if (ResultContainer != null)
            {
                RectTransform resultRect = ResultContainer.GetComponent<RectTransform>();
                LayoutGroup resultLayout = ResultContainer.GetComponent<LayoutGroup>();
                if (resultRect != null && resultLayout != null)
                    resultRect.sizeDelta = new Vector2(resultRect.sizeDelta.x, resultLayout.padding.top + resultLayout.padding.bottom + (((amountOfResultsOnPage + 1) / 2) * TileHeight) - 25);
                SetHeightToChildren();
            }
            for (int i = 0; i < AmountOfTiles; i++)
            {
                if (i >= items.Count) break;
                UI_Component_ResultItem item = items[i];
                item.gameObject.SetActive(i < amountOfResultsOnPage);
                int itemIndex = i + page * AmountOfTiles;
                if (itemIndex < result.Count)
                {
                    SetItemToResult(item, result[itemIndex]);
                }
                if (ParentRect != null)
                {
                    ParentRect.localPosition = new Vector2(0, 0);
                }
            }
        }

        public void SetHeightToChildren()
		{ 
			RectTransform rect = GetComponent<RectTransform>();
			if (rect != null) 
			{
				if (amountOfResultsOnPage == 0) 
				{
					rect.sizeDelta = new Vector2 (rect.sizeDelta.x, 400);
				} 
				else 
				{
					rect.sizeDelta = new Vector2 (rect.sizeDelta.x, ResultContainer.GetComponent<RectTransform> ().sizeDelta.y + 180);
				}
            }
            if (ParentRect != null)
            {
                ParentRect.localPosition = new Vector2(0, 0);
            }
        }

        public void SetItemToResult(UI_Component_ResultItem item, UI_Model_Product result)
        {
            item.Fill(result);
        }

        private void ItemClicked(UI_Component_ResultItem item)
        {
            if (OnClickEvent != null)
                OnClickEvent(item);
        }
    }
}
