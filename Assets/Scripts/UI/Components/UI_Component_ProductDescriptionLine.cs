﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    [RequireComponent(typeof(RectTransform))]
    public class UI_Component_ProductDescriptionLine : UI_Component
    {
        public Text HeaderText;
        public Text SubText;

        private bool subtextVisible;
        private bool updateHeight = false;

        public override void RefreshComponent()
        {
        }
        
        public void Clicked()
        {
            subtextVisible = !subtextVisible;
            SubText.gameObject.SetActive(subtextVisible);
            if (subtextVisible)
            {
                HeaderText.text = "Minder info ▼";
            }
            else
            {
                HeaderText.text = "Meer info ▲";
            }
            //updateHeight = true;
        }

        public void Open()
        {
            subtextVisible = false;
            Clicked();
        }

        public void Close()
        {
            subtextVisible = true;
            Clicked();
        }

        private void Update()
        {
            if(updateHeight)
            {
                RefreshHeight();
                updateHeight = false;
            }
        }

        public void RefreshHeight()
        {
            RectTransform rect = GetComponent<RectTransform>();
            if (subtextVisible)
            {
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, HeaderText.rectTransform.sizeDelta.y + SubText.rectTransform.sizeDelta.y + 5);
            }
            else
            {
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, HeaderText.rectTransform.sizeDelta.y + 5);
            }
            transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        }
    }
}
