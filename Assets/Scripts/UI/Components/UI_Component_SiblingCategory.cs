﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_SiblingCategory : UI_Component
    {
        public Text Title;

        public override void RefreshComponent()
        {
        }

        public void Fill(UI_Model_Category category)
        {
            Title.text = category.Name;
        }
    }
}
