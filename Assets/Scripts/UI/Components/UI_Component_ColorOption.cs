﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    public class UI_Component_ColorOption : UI_Component
    {
        public Image Round, Box, Selected, Preview;
        public Text ColorName;

        private Action<UI_ColorSelection> callback;
        private UI_ColorSelection selection;
        private bool exclusive;

        public void Clicked()
        {
            if (selection.Selected && exclusive) return;
            if (callback != null)
                callback(selection);
        }

        private void Update()
        {
            Selected.gameObject.SetActive(selection.Selected);
            Round.color = selection.Selected ? UI_Manager.instance.Stylesheet.MainColor : new Color(1, 1, 1, 1);
            Box.color = selection.Selected ? UI_Manager.instance.Stylesheet.MainColor : new Color(1, 1, 1, 1);
        }

        public void Fill(UI_ColorSelection option, bool exclusive, Action<UI_ColorSelection> onClick)
        {
            this.exclusive = exclusive;
            ColorName.text = option.ColorName;
            selection = option;
            callback = onClick;

            Round.gameObject.SetActive(exclusive);
            Round.GetComponent<Outline>().effectColor = UI_Manager.instance.Stylesheet.MainColor;
            Round.color = new Color(1, 1, 1, 1);

            Box.gameObject.SetActive(!exclusive);
            Box.GetComponent<Outline>().effectColor = UI_Manager.instance.Stylesheet.MainColor;
            Box.color = new Color(1, 1, 1, 1);

            Preview.color = option.Color;

            Selected.gameObject.SetActive(false);
        }
        
        public override void RefreshComponent()
        {
        }
    }
}