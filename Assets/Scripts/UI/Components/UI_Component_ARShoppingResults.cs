﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    public class UI_Component_ARShoppingResults : UI_Component
    {
        public int AmountOfTiles = 6;
        public UI_Component_ARShoppingItem ItemPrefab;
        public GridLayoutGroup ResultContainer;
        public UI_Component_PageNumbers PageNumbers;
        public GameObject NumbersContainer;
        public GameObject NoResultsFound;
        public GameObject LoadingContainer;
        private List<UI_Component_ARShoppingItem> items;
        private List<UI_Model_Product> result;
        private int currentPage;
        private int pages;

        private int visibleRows = 0;

        public override void RefreshComponent()
        {
        }

        public override void Initiate()
        {
            items = new List<UI_Component_ARShoppingItem>();
            if (ItemPrefab != null)
            {
                for (int i = 0; i < AmountOfTiles; i++)
                {
                    UI_Component_ARShoppingItem newItem = Instantiate(ItemPrefab);
                    newItem.gameObject.SetActive(false);
                    newItem.transform.SetParent(ResultContainer.transform);
                    newItem.transform.localScale = ItemPrefab.transform.localScale;
                    newItem.name = UI_Constants.PanelPrefix + "ResultContainer" + (i + 1);
                    newItem.OnClickEvent = UI_Manager.instance.OpenProductOverlay;
                    items.Add(newItem);
                }
                ItemPrefab.gameObject.SetActive(false);
            }
            NoResultsFound.SetActive(true);
        }
        
        public void Loading()
        {
            if(NoResultsFound != null)
                NoResultsFound.SetActive(false);
            if(LoadingContainer != null)
                LoadingContainer.SetActive(true);
            if(NumbersContainer != null)
                NumbersContainer.SetActive(false);
        }

        public void DoneLoading()
        {
            LoadingContainer.SetActive(false);
        }

        public void Fill(List<UI_Model_Product> results)
        {
            result = results;
            int amount = (result.Count - 1);
            if (amount < 0)
                amount = 0;
            pages = (amount + 1) / AmountOfTiles + 1;
            PageNumbers.SetAmountOfPages(pages);
            if (pages <= 0)
            {
                NumbersContainer.SetActive(false);
                visibleRows = 1;
            }
            NoResultsFound.SetActive(results.Count == 0);
            SetPage(0);
            DoneLoading();
        }

        public void NextPage()
        {
            SetPage(currentPage + 1);
        }

        public void PreviousPage()
        {
            SetPage(currentPage - 1);
        }

        public void SetPage(int page)
        {
            currentPage = Mathf.Clamp(page, 0, pages);
            PageNumbers.SetPage(currentPage);
            int amountOfResultsOnPage = result.Count - page * AmountOfTiles;
            amountOfResultsOnPage = Mathf.Clamp(amountOfResultsOnPage, 0, AmountOfTiles);
            visibleRows = amountOfResultsOnPage / 2;
            for (int i = 0; i < AmountOfTiles; i++)
            {
                if (i >= items.Count) break;
                UI_Component_ARShoppingItem item = items[i];
                item.gameObject.SetActive(i < amountOfResultsOnPage);
                int itemIndex = i + page * AmountOfTiles;
                if (itemIndex < result.Count)
                {
                    SetItemToResult(item, result[itemIndex]);
                }
            }
            if (visibleRows == 0)
            {
                visibleRows = 1;
            }
            RefreshHeight();
        }

        public void SetItemToResult(UI_Component_ARShoppingItem item, UI_Model_Product result)
        {
            item.Fill(result);
        }

        private void RefreshHeight()
        {
            float height = ResultContainer.cellSize.y * visibleRows;
            height += ResultContainer.spacing.y * (visibleRows - 1);
            height += ResultContainer.padding.top + ResultContainer.padding.bottom;
            RectTransform numbersRect = NumbersContainer.GetComponent<RectTransform>();
            numbersRect.localPosition = new Vector3(numbersRect.localPosition.x, -height, numbersRect.localPosition.z);
            if (NumbersContainer.activeSelf)
                height += numbersRect.sizeDelta.y;
            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, height);
        }
    }
}
