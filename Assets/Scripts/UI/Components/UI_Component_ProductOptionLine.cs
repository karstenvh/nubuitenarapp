﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    public class UI_Component_ProductOptionLine : UI_Component
    {
        public override void RefreshComponent()
        {
        }

        public Text content;
        private int number;
        private Action<int> callback;


        public void Fill(string text, int id, Action<int> clicked)
        {
            number = id;
            content.text = text;
            callback = clicked;
        }

        public void Clicked()
        {
            if(callback != null)
            {
                callback(number);
            }
        }
    }
}
