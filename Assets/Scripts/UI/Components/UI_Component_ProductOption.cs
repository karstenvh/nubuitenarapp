﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;
using VROwl.UI.Overlays;

namespace VROwl.UI.Components
{
    public class UI_Component_ProductOption : UI_Component
    {
        public Text Title;
        public Text OptionTitle;
        public Image TopBorder;
        public Image BottomBorder;
        public Image LeftBorder;
        public Image RightBorder;

        private Color startColor;
        
        [HideInInspector]
        public UI_Model_ProductOption Option;

        private void Start()
        {
            startColor = TopBorder.color;
        }

        private void Update()
        {
            string text = Option.Name;
            if (Option != null)
            {
                try
                {
                    text = Option.Options[Option.Choice].Name;
                }
                catch
                {
                }
            }
            Title.text = text;
            if (Title.text.EndsWith(":"))
            {
                Title.text = Title.text.Remove(Title.text.Length - 1, 1);
            }
            if (Option.Required)
            {
                Title.text += "*";
            }
        }

        public override void RefreshComponent()
        {
        }

        public void Fill(UI_Model_ProductOption option)
        {
            Option = option;
            Title.text = "Geen";
            OptionTitle.text = option.Name;
            if (Title.text.EndsWith(":"))
            {
                Title.text = Title.text.Remove(Title.text.Length - 1, 1);
            }
            if(option.Required)
            {
                Title.text += "*";
            }
        }
        
        public bool Check()
        {
            if(Option.Required)
            {
                if (Option.Choice == -1 && Option.Options.Count > 0)
                {
                    SetBorderColor(UI_Manager.instance.Stylesheet.WrongInputColor);
                    return false;
                }
            }
            return true;
        }

        public void SetBorderColor(Color color)
        {
            TopBorder.color = color;
            BottomBorder.color = color;
            LeftBorder.color = color;
            RightBorder.color = color;
        }
        
        public void Open()
        {
            UI_Manager.instance.OpenOptionSelectionPopup(Option, () => 
            {
                Fill(Option);
            });
        }        
    }
}