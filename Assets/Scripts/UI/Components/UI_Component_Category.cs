﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Components
{
    /// <summary>
    /// The Category component is a custom made scroll view with buttons in stead of scroll interaction.
    /// This component is also used as filter container because they share the exact same functionality.
    /// </summary>
    public class UI_Component_Category : UI_Component
    {
        [Header("UI Object references")]
        // The header of the category.
        public Text CategoryTitle;
        // The category item prefab.
        public UI_Component_CategoryItem ItemPrefab;
        // The container of the items.
        public GameObject ItemContainer;
        public ScrollRect ScrollView;

        // The click event callback.
        public UI_Component_CategoryItem.OnItemClickEvent OnClickEvent;
        // The list of items, used to clear the content.
        private List<UI_Component_CategoryItem> items;

        /// <summary>
        /// Refreshes this component.
        /// </summary>
        public override void RefreshComponent()
        {
            ScrollView.horizontalNormalizedPosition = 0;
        }

        public void Awake()
        {
        }

        // Fill the category content with products.
        public void Fill(UI_Model_Category category)
        {
            // Get the total amount of subcategories.
            if (category == null) return;
            CategoryTitle.text = category.Name;
            if (category.SubCategories == null) return;
            if(items != null && items.Count > 0)
                foreach (UI_Component_CategoryItem item in items)
                    Destroy(item.gameObject);
            items = new List<UI_Component_CategoryItem>();
            for (int i = 0; i < category.SubCategories.Count; i++)
            {
                // Get the product.
                UI_Model_Category subCategory = category.SubCategories[i];
                // Get the UI item component.
                UI_Component_CategoryItem item = Instantiate(ItemPrefab);
                // Set the attributes of the game component.
                item.transform.SetParent(ItemContainer.transform);
                item.name = UI_Constants.ButtonPrefix + subCategory.Name;
                item.transform.localScale = ItemPrefab.transform.localScale;
                item.gameObject.SetActive(true);
                // Link the callback function.
                item.OnClickEvent = ItemClicked;
                // Fill the item component with the category data.
                item.Fill(subCategory);
                items.Add(item);
            }
            // Hide the prefab. Just for sure.
            ItemPrefab.gameObject.SetActive(false);
            ScrollView.horizontalNormalizedPosition = 0;
        }

        /// <summary>
        /// Item Clicked is a callback for when an item is clicked.
        /// </summary>
        /// <param name="item">Item clicked.</param>
        public void ItemClicked(UI_Component_CategoryItem item)
        {
            if (OnClickEvent != null)
                OnClickEvent(item);
        }
    }
}