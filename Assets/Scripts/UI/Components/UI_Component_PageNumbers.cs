﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Components
{
    public class UI_Component_PageNumbers : UI_Component
    {
        public UI_Component_PageNumberItem PageButton1, PageButton2, PageButton3, PageButton4, PageButton5, PageButton6, PageButton7;
        public Button Next, Previous;

        private int amountOfPages;

        public override void RefreshComponent()
        {
        }

        public void SetAmountOfPages(int amount)
        {
            amountOfPages = amount;
            Next.gameObject.SetActive(amount > 1);
            Previous.gameObject.SetActive(amount > 1);
            PageButton1.gameObject.SetActive(amount > 1);
            PageButton2.gameObject.SetActive(amount > 1);
            PageButton3.gameObject.SetActive(amount > 2);
            PageButton4.gameObject.SetActive(amount > 3);
            PageButton5.gameObject.SetActive(amount > 4);
            PageButton6.gameObject.SetActive(amount > 5);
            PageButton7.gameObject.SetActive(amount > 6);
        }

        public void SetPage(int page)
        {
            page++;
            if (this == null) return;
            Next.gameObject.SetActive(page < amountOfPages);
            Previous.gameObject.SetActive(page > 1);

            PageButton1.SetNumber(1, page == 1);
            PageButton7.SetNumber(amountOfPages, page == amountOfPages);
            if (amountOfPages <= 7)
            {
                PageButton2.SetNumber(2, page == 2);
                PageButton3.SetNumber(3, page == 3);
                PageButton4.SetNumber(4, page == 4);
                PageButton5.SetNumber(5, page == 5);
                PageButton6.SetNumber(6, page == 6);
                PageButton7.SetNumber(7, page == 7);
            }
            else
            {
                if(page == 1 || page == 2 || page == 3 || page == 4)
                {
                    PageButton2.SetNumber(2, page == 2);
                    PageButton3.SetNumber(3, page == 3);
                    PageButton4.SetNumber(4, page == 4);
                    PageButton5.SetNumber(5, false);
                    PageButton6.SetDots();
                }
                else if (page == amountOfPages - 3 || page == amountOfPages - 2 || page == amountOfPages - 1 || page == amountOfPages)
                {
                    PageButton2.SetDots();
                    PageButton3.SetNumber(amountOfPages - 4, false);
                    PageButton4.SetNumber(amountOfPages - 3, page == amountOfPages - 3);
                    PageButton5.SetNumber(amountOfPages - 2, page == amountOfPages - 2);
                    PageButton6.SetNumber(amountOfPages - 1, page == amountOfPages - 1);
                }
                else
                {
                    PageButton2.SetDots();
                    PageButton3.SetNumber(page - 1, false);
                    PageButton4.SetNumber(page, true);
                    PageButton5.SetNumber(page + 1, false);
                    PageButton6.SetDots();
                }
            }
        }
    }
}
