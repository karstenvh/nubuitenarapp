﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI.Pages
{
    public class UI_Page_Loading : UI_Page
    { 
        protected override PageID Page { get { return PageID.LOADINGPAGE; } }
        protected override bool HeaderVisible { get { return false; } }

        public override void InitiatePage()
        {
        }

        public override void RefreshPage()
        {
        }
    }
}
