﻿namespace VROwl.UI
{
    /// <summary>
    /// The state identifier for the ui manager.
    /// </summary>
    public enum PageID
    {
        NONE, LOADINGPAGE, HOMEPAGE, LOGINPAGE, RESULTPAGE, WISHLISTPAGE, SETTINGSPAGE, ARPAGE, SHOPPPINGCARTPAGE, QUOTATIONPAGE
    }
}