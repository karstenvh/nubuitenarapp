﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Pages
{
    public class UI_Page_Settings : UI_Page
    {
        protected override PageID Page { get { return PageID.SETTINGSPAGE; } }
        protected override bool HeaderVisible { get { return true; } }
        
        public GameObject ScrollContent;

        public GameObject LogInContent;

        public override void InitiatePage()
        {
            AddFooter(ScrollContent.transform);
        }

        public override void RefreshPage()
        {
            LogInContent.SetActive(UI.LoggedIn() && UI.Modules.HasLogin);
        }

        public void DeleteAllClicked()
        {
            UI.OpenYesNoPopup(() =>
            {
                UI.ClearCache();
                UI.GoToPage(PageID.HOMEPAGE);
            },
            () => { },
            "Weet u zeker dat u alle gedownloade modellen wilt verwijderen?");
        }

        public void LogOutClicked()
        {
            UI.OpenYesNoPopup(() =>
            {
                UI.LogOut();
                UI.GoToPage(PageID.HOMEPAGE);
            },
            () => { },
            "Weet u zeker dat u wilt uitloggen?");
        }
    }
}
