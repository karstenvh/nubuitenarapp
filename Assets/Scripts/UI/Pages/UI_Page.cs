﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    /// <summary>
    /// This abstract class contains the basic setup for each page.
    /// </summary>
    public abstract class UI_Page : MonoBehaviour
    {
        // The page identifier, used to decide when this page is visible.
        protected abstract PageID Page { get; }
        public PageID ID { get { return Page; } }

        // The header state for this page.
        protected abstract bool HeaderVisible { get; }
        public bool HasHeader { get { return HeaderVisible; } }

        // If this page is active. The only info we need for custom page logic is if this page is shown or not.
        protected bool IsCurrentPage = false;

        protected UI_Manager UI;

        private bool? prevstate = null;
        private UI_Component[] components;
        // State identifiers to identify if the page is opened.
        private PageID currentPage = PageID.NONE;
        private PageID lastPage = PageID.NONE;

        public delegate void PageLoadedCallback(bool succes);

        private GameObject _footer = null;
        protected GameObject Footer { get { return GetFooter(); } set { } }

        /// <summary>
        /// Initiate is called when the UI is loaded.
        /// /// </summary>
        public void Initiate(UI_Manager manager)
        {
            components = GetComponentsInChildren<UI_Component>(true);
            if (components != null && components.Length > 0)
                foreach (UI_Component component in components)
                    component.Initiate();
            UI = manager;
            InitiatePage();
        }

        /// <summary>
        /// The abstract method to initiate a page. Allowing for custom initiation implementation for each page.
        /// </summary>
        public abstract void InitiatePage();

        /// <summary>
        /// Refresh this page. 
        /// </summary>
        /// <param name="state">The current state of the UI.</param>
        public void Refresh(PageID state)
        {
            // Set the page states.
            lastPage = currentPage;
            currentPage = state;
            if (lastPage != currentPage && currentPage == Page)
                PageOpened();
            bool newState = state == Page;
            //If this page is inactive and it already was inactive the page will not refresh. Unless it's at start up; that's where prevstate comes in.
            if (newState == IsCurrentPage && !newState && prevstate != null) return;
            prevstate = IsCurrentPage;
            IsCurrentPage = newState;
            gameObject.SetActive(IsCurrentPage);
            if (components != null && components.Length > 0 && IsCurrentPage)
            {
                foreach (UI_Component component in components)
                {
                    if (component != null)
                    {
                        component.Refresh();
                    }
                }
            }
            RefreshPage();
        }

        /// <summary>
        /// The abstract method to refresh a page. Allowing for custom refresh implementation for each page. Check the bool Active to see if the page is visible.
        /// </summary>
        public abstract void RefreshPage();

        /// <summary>
        /// This is called when the page is opened or reopened.
        /// </summary>
        public virtual void PageOpened()
        {
        }

        /// <summary>
        /// The abstract method to load a page.
        /// </summary>
        public virtual void LoadPage(PageLoadedCallback loadCallback)
        {
            loadCallback(true);
        }
        
        /// <summary>
        /// Make a footer object. Use Footer property to get it.
        /// </summary>
        protected GameObject GetFooter()
        {
            if (_footer == null)
            {
                _footer = Instantiate(UI.FooterPrefab);
                _footer.SetActive(true);
                _footer.transform.localScale = UI.FooterPrefab.transform.localScale;
                UI.FooterPrefab.SetActive(false);
            }
            return _footer;
        }
        
        /// <summary>
        /// Add a footer object to a parent.
        /// </summary>
        protected void AddFooter(Transform parent, bool asLast = true)
        {
            if (UI.FooterPrefab != null && UI.Modules.HasFooter)
            {
                GameObject footer = Footer;
                footer.transform.SetParent(parent);
                footer.transform.localScale = UI.FooterPrefab.transform.localScale;
                if (asLast)
                {
                    footer.transform.SetAsLastSibling();
                }
            }
        }

        protected void SetFooterActive(bool active)
        {
            if (UI.Modules.HasFooter)
            {
                Footer.SetActive(active);
            }
            else
            {
                Footer.SetActive(false);
            }
        }
    }
}