﻿using VROwl.UI.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;
using UnityEngine.UI;

namespace VROwl.UI.Pages
{
    public class UI_Page_Home : UI_Page
    {
        protected override PageID Page { get { return PageID.HOMEPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        public GameObject CategoryContainer, LoadingContainer, EmptySpace;

        private List<UI_Component_Category> categoryItems;
        private bool firstTimeLoading = true;

        public override void InitiatePage()
        {
            firstTimeLoading = true;
        }

        public void Fill(List<UI_Model_Category> categories)
        {
            if(categoryItems != null)
            {
                foreach (UI_Component_Category oldCategory in categoryItems)
                    Destroy(oldCategory.gameObject);
            }
            categoryItems = new List<UI_Component_Category>();
            for (int i = 0; i < categories.Count; i++)
            {
                UI_Model_Category category = categories[i];
                if (category == null || category.SubCategories == null) continue;
                if (category.SubCategories.Count <= 0) continue;
                UI_Component_Category categoryComponent = Instantiate(UI.CategoryPrefab);
                categoryComponent.OnClickEvent = ItemClicked;
                categoryComponent.transform.SetParent(CategoryContainer.transform);
                categoryComponent.name = UI_Constants.PanelPrefix + category.Name;
                categoryComponent.gameObject.SetActive(true);
                categoryComponent.transform.localScale = UI.CategoryPrefab.transform.localScale;
                categoryComponent.Fill(category);
                categoryItems.Add(categoryComponent);
                categoryComponent.RefreshComponent();
            }
            EmptySpace.transform.SetAsLastSibling();
            UI.CategoryPrefab.gameObject.SetActive(false);
            AddFooter(CategoryContainer.transform);
        }

        public override void RefreshPage()
        {
        }
        
        public void Loading()
        {
            LoadingContainer.SetActive(true);
            CategoryContainer.SetActive(false);
        }

        public void DoneLoading()
        {
            LoadingContainer.SetActive(false);
            CategoryContainer.SetActive(true);
        }

        public override void LoadPage(PageLoadedCallback loadCallback)
        {
            if (!firstTimeLoading)
            {
                loadCallback(true);
                Loading();
            }
            UI.LoadCatalogue((catalogue) => 
            {
                if(catalogue != null)
                {
                    List<UI_Model_Category> categories = catalogue.Categories;
                    Fill(categories);
                    DoneLoading();
                    if (firstTimeLoading)
                    {
                        loadCallback(true);
                        firstTimeLoading = false;
                        ShowDataUsagePopupIfNotConfirmed();
                    }
                }
            });
        }

        private void ShowDataUsagePopupIfNotConfirmed()
        {
            int accepted = PlayerPrefs.GetInt("AcceptedDataUsage");
            if (accepted != 1)
            {
                UI.OpenPopup(PopupID.DATAUSAGE);
            }
            else
            {
                ShowDeviceSupportPopupIfNotConfirmed();
            }
        }

        public void DataUsageAccepted()
        {
            ShowDeviceSupportPopupIfNotConfirmed();
            PlayerPrefs.SetInt("AcceptedDataUsage", 1);
            PlayerPrefs.Save();
        }

        private void ShowDeviceSupportPopupIfNotConfirmed()
        {
            int accepted = PlayerPrefs.GetInt("AcceptedDeviceSupport");
            if (accepted != 1)
            {
                UI.OpenPopup(PopupID.DEVICESUPPORT);
            }
            else
            {
                UI_Manager.instance.ClosePopup();
            }
        }

        public void DeviceSupportAccepted()
        {
            PlayerPrefs.SetInt("AcceptedDeviceSupport", 1);
            PlayerPrefs.Save();
            UI_Manager.instance.ClosePopup();
        }

        public void ItemClicked(UI_Component_CategoryItem item)
        {
            UI.GoToResultPage(item.Category);
        }
    }
}
