﻿using VROwl.UI.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Models;

namespace VROwl.UI.Pages
{
    public class UI_Page_Result : UI_Page
    {
        protected override PageID Page { get { return PageID.RESULTPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        public Text TitleTextMobile;
        public Text TitleTextTablet;
        public GameObject PageContainer;
        public UI_Component_Result ResultsContainerMobile;
        public UI_Component_Result ResultsContainerTablet;
        public ScrollRect ContentScrollView;
        public GameObject ScrollContent;
        public GameObject ResultHeader;

        [HideInInspector]
        public Stack<UI_Model_Category> PreviousCategories;
        private UI_Component_Category FilterContainter;

        public override void InitiatePage()
        {
            FilterContainter = Instantiate(UI.CategoryPrefab);
            FilterContainter.transform.localScale = UI.CategoryPrefab.transform.localScale;
            FilterContainter.name = UI_Constants.PanelPrefix + "Filters";
            FilterContainter.transform.SetParent(PageContainer.transform);
            FilterContainter.transform.SetSiblingIndex(0);
            FilterContainter.transform.localScale = UI.CategoryPrefab.transform.localScale;
            FilterContainter.gameObject.SetActive(true);
            FilterContainter.OnClickEvent = AddFilter;
            FilterContainter.RefreshComponent();

            ResultHeader.SetActive(UI.Stylesheet.ShowResultHeader);
            if (ResultsContainerMobile != null)
            {
                ResultsContainerMobile.OnClickEvent = ItemClicked;
            }
            if(ResultsContainerTablet != null)
            {
                ResultsContainerTablet.OnClickEvent = ItemClicked;
            }
            AddFooter(ScrollContent.transform);
        }

        public override void RefreshPage()
        {
        }

        public void Fill(UI_Model_Category category)
        {
            ContentScrollView.normalizedPosition = new Vector2(ContentScrollView.normalizedPosition.x, 1);

            if (ResultsContainerMobile != null)
            {
                ResultsContainerMobile.Loading();
            }
            if (ResultsContainerTablet != null)
            {
                ResultsContainerTablet.Loading();
            }

            PreviousCategories.Push(category);

            if (TitleTextTablet != null)
            {
                TitleTextTablet.text = category.Name;
            }
            if (TitleTextMobile != null)
            {
                TitleTextMobile.text = category.Name;
            }

            FilterContainter.gameObject.SetActive(category.SubCategories != null && category.SubCategories.Count > 0);
            FilterContainter.Fill(category);
            SetFooterActive(false);

            if (UI.Modules.UseNuBuitenAppFlow)
            {
                FilterContainter.gameObject.SetActive(false);

                UI.LoadCategoryFilters(
                    filters =>
                    {
                        FilterContainter.gameObject.SetActive(filters != null && filters.SubCategories.Count > 0);
                        FilterContainter.Fill(filters);
                        if (TitleTextTablet != null)
                        {
                            TitleTextTablet.text = filters.Name;
                        }
                        if (TitleTextMobile != null)
                        {
                            TitleTextMobile.text = filters.Name;
                        }

                    }, category.ID);
            }

            UI.LoadCategoryProducts(
                (ps) =>
                {
                    SetFooterActive(true);
                    if (ResultsContainerMobile != null)
                    {
                        ResultsContainerMobile.Fill(ps);
                        ResultsContainerMobile.DoneLoading();
                    }
                    if (ResultsContainerTablet != null)
                    {
                        ResultsContainerTablet.Fill(ps);
                        ResultsContainerTablet.DoneLoading();
                    }
                }, category.ID);
        }

        public void AddFilter(UI_Component_CategoryItem filter)
        {
            Fill(filter.Category);
        }

        public void GoBack()
        {
            if (PreviousCategories == null || PreviousCategories.Count <= 1)
                UI.GoToPage(PageID.HOMEPAGE);
            else
            {
                PreviousCategories.Pop();
                UI_Model_Category previousCategory = PreviousCategories.Pop();
                Fill(previousCategory);
            }
        }

        public void ItemClicked(UI_Component_ResultItem item)
        {
            UI.OpenProductOverlay(item.Product);
        }

        public override void PageOpened()
        {
            PreviousCategories = new Stack<UI_Model_Category>();
        }

        public void SiblingCategoryClicked()
        {

        }
    }
}
