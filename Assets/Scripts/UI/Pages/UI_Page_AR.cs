﻿using UnityEngine;
using UnityEngine.UI;
using VROwl.AR.Placement;
using VROwl.UI.Components;
using VROwl.UI.Models;

namespace VROwl.UI.Pages
{
    public class UI_Page_AR : UI_Page
    {
        protected override PageID Page { get { return PageID.ARPAGE; } }
        protected override bool HeaderVisible { get { return false; } }

        [Header("UI Object references")]
        public Button DeleteButton;
        public Button AddButton;
        public Button PlaceButton;
        public Button HomeButton;
        public Button ScreenshotButton;
        public Button MeasureButton;
        public Button MeasureDeleteButton;
        public Button MeasureStartButton;
        public Button MeasureEndButton;
        public Button MeasureCancelButton;
        public Button QuotationButton;
        public Button InfoButton;
        public Button ColorButton;
        public GameObject OverlayBackground;
        public GameObject Overlay;
        public GameObject ARInstructions;
        public GameObject ComplexityContainer;
        public Image ComplexityProgress;
        public Text ComplexityTooMany;

        // The callback delegate for button events.
        public delegate void ButtonClicked();
        public ButtonClicked OnDeleteClicked;
        public ButtonClicked OnAddClicked;
        public ButtonClicked OnPlaceClicked;
        public ButtonClicked OnHomeClicked;
        public ButtonClicked OnScreenshotClicked;
        public ButtonClicked OnMeasureClicked;
        public ButtonClicked OnMeasureDeleteClicked;
        public ButtonClicked OnMeasureStartClicked;
        public ButtonClicked OnMeasureEndClicked;
        public ButtonClicked OnMeasureCancelClicked;
        public ButtonClicked OnQuotationClicked;
        public ButtonClicked OnInfoClicked;
        public ButtonClicked OnColorClicked;

        public bool ShowInstructionOnPageLoad = true;

        public ScrollRect ScrollView;
        public Text LoadingText;
        public Text ScanSuccessText;

        public UI_Component_ARHomeOverlay HomeOverlayTablet;
        public UI_Component_ARHomeOverlay HomeOverlayPhone;
        public UI_Component_ARResultOverlay ResultOverlayTablet;
        public UI_Component_ARResultOverlay ResultOverlayPhone;
        public UI_Component_TutorialAR TutorialOverlay;

        public ARPlacementInteraction AR;

        public float SuccesfulMessageDuration = 5;
        public float SuccesfulMessageFadeTime = 1;
        
        private bool objectSelected = false;
        private bool measurementSelected = false;
        private bool showingInstructions = false;
        private bool inMeasureMode = false;
        private bool startedMeasuring = false;

        private float fadeDelay;
        private float fadeDuration;

        public override void InitiatePage()
        {
        }

        public override void RefreshPage()
        {
            QuotationButton.gameObject.SetActive(UI.IsQuotationVisible());
            RefreshButtons();
        }

        public override void PageOpened()
        {
            TutorialOverlay.RefreshComponent();
            ScanSuccessText.gameObject.SetActive(false);
            SetComplexityProgress(0);
            objectSelected = false;
            if (ShowInstructionOnPageLoad)
            {
                ShowARInstructions();
            }
            else
            {
                HideARInstructions();
            }
            CloseOverlay();
        }

        public void ShowARInstructions()
        {
            showingInstructions = true;
            RefreshARInstructions();
            RefreshButtons();
        }

        public void HideARInstructions()
        {
            showingInstructions = false;
            RefreshARInstructions();
            RefreshButtons();
        }

        private void RefreshARInstructions()
        {
            ARInstructions.SetActive(showingInstructions);

            AddButton.interactable = !showingInstructions;
            DeleteButton.interactable = !showingInstructions;
            ScreenshotButton.interactable = !showingInstructions;
            MeasureButton.interactable = !showingInstructions;
            QuotationButton.interactable = !showingInstructions;
        }

        public void DeleteClicked()
        {
            if (OnDeleteClicked != null)
                OnDeleteClicked();
            RefreshButtons();
        }

        public void AddClicked()
        {
            OpenOverlay();
            if (OnAddClicked != null)
                OnAddClicked();
            RefreshButtons();
        }

        public void PlaceClicked()
        {
            if(OnPlaceClicked != null)
                OnPlaceClicked();
            RefreshButtons();
        }

        public void HomeClicked()
        {
            if (inMeasureMode)
            {
                StopMeasuring();
            }

            if (OnHomeClicked != null)
                OnHomeClicked();
            else
                UI.GoToPage(PageID.HOMEPAGE);
        }

        public void ScreenshotClicked()
        {
            if (OnScreenshotClicked != null)
                OnScreenshotClicked();
        }

        public void MeasureClicked()
        {
            inMeasureMode = true;
            startedMeasuring = false;

            if (OnMeasureClicked != null)
                OnMeasureClicked();
            RefreshButtons();
        }

        public void MeasureStartClicked()
        {
            if (OnMeasureStartClicked != null)
                OnMeasureStartClicked();
            StartMeasuring();
        }

        public void StartMeasuring()
        {
            inMeasureMode = true;
            startedMeasuring = true;
            objectSelected = false;
            RefreshButtons();
        }

        public void MeasureEndClicked()
        {
            if (OnMeasureEndClicked != null)
                OnMeasureEndClicked();
            StopMeasuring();
        }

        public void StopMeasuring()
        {
            inMeasureMode = false;
            startedMeasuring = false;
            objectSelected = false;
            RefreshButtons();
        }

        public void MeasureCancelClicked()
        {
            if (OnMeasureCancelClicked != null)
                OnMeasureCancelClicked();
            StopMeasuring();
        }

        public void MeasureDeleteClicked()
        {
            if (OnMeasureDeleteClicked != null)
                OnMeasureDeleteClicked();

            DeselectMeasurement();
            StopMeasuring();
        }

        public void QuotationClicked()
        {
            if (OnQuotationClicked != null)
                OnQuotationClicked();
            RefreshButtons();
        }

        public void InfoClicked()
        {
            if (OnInfoClicked != null)
                OnInfoClicked();
            RefreshButtons();
            UI.OpenProductOverlayOfSelectedObject();
        }

        public void ColorClicked()
        {
            if (OnColorClicked != null)
                OnColorClicked();
        }

        public void SelectObject()
        {
            objectSelected = true;
            RefreshButtons();
        }

        public void DeselectObject()
        {
            objectSelected = false;
            RefreshButtons();
        }

        public void SelectMeasurement()
        {
            measurementSelected = true;
            RefreshButtons();
        }

        public void DeselectMeasurement()
        {
            measurementSelected = false;
            RefreshButtons();
        }

        public void RefreshButtons()
        {
            if (showingInstructions)
            {
                InfoButton.gameObject.SetActive(false);
                ColorButton.gameObject.SetActive(false);
                DeleteButton.gameObject.SetActive(false);
                AddButton.gameObject.SetActive(false);
                PlaceButton.gameObject.SetActive(false);
                HomeButton.gameObject.SetActive(true);
                ScreenshotButton.gameObject.SetActive(false);

                MeasureButton.gameObject.SetActive(false);
                MeasureDeleteButton.gameObject.SetActive(false);
                MeasureStartButton.gameObject.SetActive(false);
                MeasureEndButton.gameObject.SetActive(false);
                MeasureCancelButton.gameObject.SetActive(false);
                QuotationButton.gameObject.SetActive(false);
            }
            else
            {
                ColorButton.gameObject.SetActive(objectSelected 
                    && !inMeasureMode && UI.Modules.HasColorCustomisation
                    && AR != null && AR.HasSelection && AR.Selection.productInfo != null 
                    && AR.Selection.productInfo.HasColorOption());
                InfoButton.gameObject.SetActive(objectSelected && !inMeasureMode);
                DeleteButton.gameObject.SetActive(objectSelected && !inMeasureMode);
                PlaceButton.gameObject.SetActive(objectSelected && !inMeasureMode);
                AddButton.gameObject.SetActive(!objectSelected && !inMeasureMode);
                HomeButton.gameObject.SetActive(!inMeasureMode);
                ScreenshotButton.gameObject.SetActive(UI.Modules.HasScreenshot && !inMeasureMode);

                MeasureButton.gameObject.SetActive(UI.Modules.HasMeasurement && !measurementSelected && !inMeasureMode);
                MeasureDeleteButton.gameObject.SetActive(UI.Modules.HasMeasurement && measurementSelected);
                MeasureStartButton.gameObject.SetActive(!startedMeasuring && inMeasureMode);
                MeasureEndButton.gameObject.SetActive(startedMeasuring && inMeasureMode);
                MeasureCancelButton.gameObject.SetActive(inMeasureMode);
                QuotationButton.gameObject.SetActive(UI.IsQuotationVisible() && !inMeasureMode && !measurementSelected);
            }
        }

        public void OpenOverlay()
        {
            Overlay.gameObject.SetActive(true);
            if (HomeOverlayTablet != null && !UI_Manager.IsPhone())
            {
                HomeOverlayTablet.gameObject.SetActive(true);
            }
            if (HomeOverlayPhone != null && UI_Manager.IsPhone())
            {
                HomeOverlayPhone.gameObject.SetActive(true);
            }
            OverlayBackground.SetActive(true);
            LoadingText.gameObject.SetActive(true);
            LoadingText.text = "Laden...";
            ScrollView.gameObject.SetActive(false);

            if (HomeOverlayTablet != null && !UI_Manager.IsPhone())
            {
                HomeOverlayTablet.LoadComponent((succes) =>
                {
                    if (succes)
                    {
                        LoadingText.gameObject.SetActive(false);
                        ScrollView.gameObject.SetActive(true);
                        ScrollView.content = HomeOverlayTablet.GetComponent<RectTransform>();
                        HomeOverlayTablet.gameObject.SetActive(true);
                        if (ResultOverlayTablet != null)
                        {
                            ResultOverlayTablet.gameObject.SetActive(false);
                            ResultOverlayTablet.BackToHome = OpenOverlay;
                        }
                        HomeOverlayTablet.CategorySelectedCallback = GoToResult;
                    }
                    else
                    {
                        LoadingText.text = "Laden mislukt...\n Probeer het later nog eens.";
                    }
                });
            }
            if(HomeOverlayPhone != null && UI_Manager.IsPhone())
            {
                HomeOverlayPhone.LoadComponent((succes) =>
                {
                    if (succes)
                    {
                        LoadingText.gameObject.SetActive(false);
                        ScrollView.gameObject.SetActive(true);
                        ScrollView.content = HomeOverlayPhone.GetComponent<RectTransform>();
                        HomeOverlayPhone.gameObject.SetActive(true);
                        if (ResultOverlayPhone != null)
                        {
                            ResultOverlayPhone.gameObject.SetActive(false);
                            ResultOverlayPhone.BackToHome = OpenOverlay;
                        }
                        HomeOverlayPhone.CategorySelectedCallback = GoToResult;
                    }
                    else
                    {
                        LoadingText.text = "Laden mislukt...\n Probeer het later nog eens.";
                    }
                });
            }
        }

        public void CloseOverlay()
        {
            Overlay.gameObject.SetActive(false);
            if (HomeOverlayTablet != null && !UI_Manager.IsPhone())
            {
                HomeOverlayTablet.gameObject.SetActive(false);
            }
            if (HomeOverlayPhone != null && UI_Manager.IsPhone())
            {
                HomeOverlayPhone.gameObject.SetActive(false);
            }
            OverlayBackground.SetActive(false);
            RefreshButtons();
        }

        public void GoToResult(UI_Model_Category category)
        {
            if (ResultOverlayTablet != null && !UI_Manager.IsPhone())
            {
                ScrollView.content = ResultOverlayTablet.GetComponent<RectTransform>();
                ResultOverlayTablet.gameObject.SetActive(true);
                ResultOverlayTablet.Fill(category);
            }
            if(ResultOverlayPhone != null & UI_Manager.IsPhone())
            {
                ScrollView.content = ResultOverlayPhone.GetComponent<RectTransform>();
                ResultOverlayPhone.gameObject.SetActive(true);
                ResultOverlayPhone.Fill(category);
            }
            if (HomeOverlayTablet != null && !UI_Manager.IsPhone())
            {
                HomeOverlayTablet.gameObject.SetActive(false);
            }
            if (HomeOverlayPhone != null && UI_Manager.IsPhone())
            {
                HomeOverlayPhone.gameObject.SetActive(false);
            }
        }

        public void OnPlaneFound()
        {
            ScanSuccessText.gameObject.SetActive(true);
            ScanSuccessText.color = new Color(ScanSuccessText.color.r, ScanSuccessText.color.g, ScanSuccessText.color.b, 1);
            fadeDelay = Time.time + SuccesfulMessageDuration;
        }

        /// <summary>
        /// Set the percentage of the complexity of the scene. (0 - 1)
        /// </summary>
        /// <param name="percentage"></param>
        public void SetComplexityProgress(float percentage)
        {
            percentage = Mathf.Clamp01(percentage);
            ComplexityProgress.fillAmount = percentage;
            ComplexityContainer.SetActive(percentage > 0);
            ComplexityTooMany.gameObject.SetActive(percentage >= 1);
        }

        private void Update()
        {
            if(fadeDelay != 0 && Time.time > fadeDelay)
            {
                fadeDelay = 0;
                fadeDuration = SuccesfulMessageFadeTime;
            }
            if (fadeDuration != 0)
            {
                fadeDuration -= Time.deltaTime;
                if (fadeDuration < 0)
                {
                    fadeDuration = 0;
                    ScanSuccessText.gameObject.SetActive(false);
                }
                float percentage = fadeDuration / SuccesfulMessageFadeTime;
                ScanSuccessText.color = new Color(ScanSuccessText.color.r, ScanSuccessText.color.g, ScanSuccessText.color.b, percentage);
            }
        }        
    }
}
