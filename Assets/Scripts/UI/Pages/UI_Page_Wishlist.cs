﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Components;
using VROwl.UI.Models;

namespace VROwl.UI.Pages {
    public class UI_Page_Wishlist : UI_Page {
        protected override PageID Page { get { return PageID.WISHLISTPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        public UI_Component_Result ResultContainerTablet;
        public UI_Component_Result ResultContainerPhone;
        public GameObject ScrollContent;

        public override void InitiatePage() {
            AddFooter(ScrollContent.transform);
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.OnClickEvent = ItemClicked;
            }
            else {
                ResultContainerPhone.OnClickEvent = ItemClicked;
            }
        }

        public override void RefreshPage() {
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.RefreshComponent();
            }
            else {
                ResultContainerPhone.RefreshComponent();
            }
        }

        public override void LoadPage(PageLoadedCallback loadCallback) {
            loadCallback(true);
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.Loading();
            }
            else {
                ResultContainerPhone.Loading();
            }

            if (UI_Manager.instance.wishlistId == -1) {
                StartCoroutine(WaitForWishlist());
            }
            else {
                UI_Manager.instance.LoadFavorites((favorites) => Fill(favorites));
            }
        }

        public void Fill(List<UI_Model_Product> products) {
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.Fill(products);
                ResultContainerTablet.DoneLoading();
            }
            else {
                ResultContainerPhone.Fill(products);
                ResultContainerPhone.DoneLoading();
            }
        }

        public void ItemClicked(UI_Component_ResultItem item) {
            UI.OpenProductOverlay(item.Product);
        }

        private IEnumerator WaitForWishlist() {
            float timeout = 5;
            while (UI_Manager.instance.wishlistId == -1 && timeout > 0) {
                timeout -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            UI_Manager.instance.LoadFavorites((favorites) => Fill(favorites));
        }
    }
}
