﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Components;
using VROwl.UI.Models;

namespace VROwl.UI.Pages {
    public class UI_Page_ShoppingCart : UI_Page {
        protected override PageID Page { get { return PageID.SHOPPPINGCARTPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        public UI_Component_ShoppingResult ResultContainerTablet;
        public UI_Component_ShoppingResult ResultContainerPhone;
        public GameObject ScrollContent;

        public ShoppingCartTotalPrice CartPriceTablet;
        public ShoppingCartTotalPrice CartPricePhone;

        public override void InitiatePage() {
            AddFooter(ScrollContent.transform);
        }

        public override void RefreshPage() {
            Debug.Log("refreshing the shopping cart page!");
            UI_Manager.instance.LoadShoppingCart((cart) => Fill(cart));
        }

        public void Fill(Dictionary<UI_Model_Product, int> cart) {
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.Fill(cart, true);
                ResultContainerTablet.DoneLoading();
                CartPriceTablet.Init();
            }
            else {
                ResultContainerPhone.Fill(cart, true);
                ResultContainerPhone.DoneLoading();
                CartPricePhone.Init();
            }
        }

        public override void LoadPage(PageLoadedCallback loadCallback) {
            loadCallback(true);
            if (!UI_Manager.IsPhone()) {
                ResultContainerTablet.Loading();
            }
            else {
                ResultContainerPhone.Loading();
            }
            UI_Manager.instance.LoadShoppingCart((cart) => Fill(cart));
        }
    }
}
