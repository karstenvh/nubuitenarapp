﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Components;
using VROwl.UI.Models;

namespace VROwl.UI.Pages
{
    public class UI_Page_QuotationPage : UI_Page
    {
        protected override PageID Page { get { return PageID.QUOTATIONPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        public UI_Component_ShoppingResult ResultContainerTablet, ResultContainerPhone;
        public GameObject ScrollContent;
        public InputField NameField, MailField, CommentsField;

        public override void InitiatePage()
        {
            AddFooter(ScrollContent.transform);
        }

        public override void RefreshPage()
        {
            if (ResultContainerTablet != null)
            {
                ResultContainerTablet.Refresh();
            }
            if (ResultContainerPhone != null)
            {
                ResultContainerPhone.Refresh();
            }
        }

        public void Fill(Dictionary<UI_Model_Product, int> quotation)
        {
            if (ResultContainerTablet != null)
            {
                ResultContainerTablet.Fill(quotation, false);
                ResultContainerTablet.DoneLoading();
            }
            if (ResultContainerPhone != null)
            {
                ResultContainerPhone.Fill(quotation, false);
                ResultContainerPhone.DoneLoading();
            }
        }

        public override void LoadPage(PageLoadedCallback loadCallback)
        {
            loadCallback(true);
            if (ResultContainerTablet != null)
            {
                ResultContainerTablet.Loading();
            }
            if (ResultContainerPhone != null)
            {
                ResultContainerPhone.Loading();
            }
            UI_Manager.instance.LoadQuotation((quotation) => Fill(quotation));
        }

        public void SendQuotation()
        {
            UI.SendQuotation(NameField.text, MailField.text, CommentsField.text, (result) => 
            {
                if (result)
                {
                    UI.OpenOkPopup(() => { UI.GoToPage(PageID.HOMEPAGE); }, "Uw offerte is verzonden!");
                }
                else
                {
                    UI.OpenOkPopup(null, "Verzenden mislukt. Probeer het later nog eens...");
                }
            });
        }
    }
}
