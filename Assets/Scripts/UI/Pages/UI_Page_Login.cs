﻿// copyright Uil VR Solutions B.V.

using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI.Pages
{
    public class UI_Page_Login : UI_Page
    {
        protected override PageID Page { get { return PageID.LOGINPAGE; } }
        protected override bool HeaderVisible { get { return true; } }

        [Header("Tablet References")]
        public InputField UsernameInputTablet;
        public InputField PasswordInputTablet;
        public GameObject UsernameTextTablet, PasswordTextTablet, LoginInTextTablet, ScrollContentTablet;
        public Outline UsernameOutlineTablet, PasswordOutlineTablet;

        [Header("Phone References")]
        public InputField UsernameInputMobile;
        public InputField PasswordInputMobile;
        public GameObject UsernameTextMobile, PasswordTextMobile, LoginInTextMobile, ScrollContentMobile;
        public Outline UsernameOutlineMobile, PasswordOutlineMobile;

        private Color normalOutlineColor;

        public override void InitiatePage()
        {
            if (!UI_Manager.IsPhone())
            {
                AddFooter(ScrollContentTablet.transform);
            }
            else
            {
                AddFooter(ScrollContentMobile.transform);
            }
        }

        public override void RefreshPage()
        {
            if (IsCurrentPage)
            {
                if (!UI_Manager.IsPhone())
                {
                    UsernameInputTablet.text = "";
                    PasswordInputTablet.text = "";
                    UsernameInputTablet.gameObject.SetActive(true);
                    UsernameTextTablet.SetActive(true);
                    PasswordInputTablet.gameObject.SetActive(true);
                    PasswordTextTablet.SetActive(true);
                    LoginInTextTablet.SetActive(false);
                }
                else
                {
                    UsernameInputMobile.text = "";
                    PasswordInputMobile.text = "";
                    UsernameInputMobile.gameObject.SetActive(true);
                    UsernameTextMobile.SetActive(true);
                    PasswordInputMobile.gameObject.SetActive(true);
                    PasswordTextMobile.SetActive(true);
                    LoginInTextMobile.SetActive(false);
                }
            }
        }

        public void LoginClicked()
        {
            if (!UI_Manager.IsPhone())
            {
                UsernameInputTablet.gameObject.SetActive(false);
                UsernameTextTablet.SetActive(false);
                PasswordInputTablet.gameObject.SetActive(false);
                PasswordTextTablet.SetActive(false);
                LoginInTextTablet.SetActive(true);
                UI.LogIn(UsernameInputTablet.text, PasswordInputTablet.text,
                    () => UI.GoToPage(PageID.HOMEPAGE),
                    () =>
                    {
                        UsernameInputTablet.gameObject.SetActive(true);
                        UsernameTextTablet.SetActive(true);
                        PasswordInputTablet.gameObject.SetActive(true);
                        PasswordTextTablet.SetActive(true);
                        LoginInTextTablet.SetActive(false);
                        UsernameOutlineTablet.effectColor = UI.Stylesheet.WrongInputColor;
                        PasswordOutlineTablet.effectColor = UI.Stylesheet.WrongInputColor;
                    });
            }
            else
            {
                UsernameInputMobile.gameObject.SetActive(false);
                UsernameTextMobile.SetActive(false);
                PasswordInputMobile.gameObject.SetActive(false);
                PasswordTextMobile.SetActive(false);
                LoginInTextMobile.SetActive(true);
                UI.LogIn(UsernameInputMobile.text, PasswordInputMobile.text,
                    () => UI.GoToPage(PageID.HOMEPAGE),
                    () =>
                    {
                        UsernameInputMobile.gameObject.SetActive(true);
                        UsernameTextMobile.SetActive(true);
                        PasswordInputMobile.gameObject.SetActive(true);
                        PasswordTextMobile.SetActive(true);
                        LoginInTextMobile.SetActive(false);
                        UsernameOutlineMobile.effectColor = UI.Stylesheet.WrongInputColor;
                        PasswordOutlineMobile.effectColor = UI.Stylesheet.WrongInputColor;
                    });
            }
        }

        private void Start()
        {
            if (!UI_Manager.IsPhone())
            {
                normalOutlineColor = UsernameOutlineTablet.effectColor;
            }
            else
            {
                normalOutlineColor = UsernameOutlineMobile.effectColor;
            }
        }

        public void InputChanged()
        {

            if (!UI_Manager.IsPhone())
            {
                UsernameOutlineTablet.effectColor = normalOutlineColor;
                PasswordOutlineTablet.effectColor = normalOutlineColor;
            }
            else
            {
                UsernameOutlineMobile.effectColor = normalOutlineColor;
                PasswordOutlineMobile.effectColor = normalOutlineColor;
            }
        }

        public void Register()
        {
            if (UI.Analytics != null)
                UI.Analytics.Register();
            UI.GoToURL("account/register");
        }
    }
}