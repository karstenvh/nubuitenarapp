﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;

namespace VROwl.UI.Test
{
    public static class UI_Test_FakeData
    {
        public static UI_Model_Catalogue GetNuBuitenCatalogue()
        {
            return new UI_Model_Catalogue()
            {
                Name = "NuBuiten",
                HeaderIcon = "",
                Categories = new List<UI_Model_Category>()
                {
                    new UI_Model_Category()
                    {
                        Name = "Blokhut",
                        SubCategories = new List<UI_Model_Category>()
                        {
                            new UI_Model_Category()
                            {
                                Name = "Blokhut op dakvorm",
                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/1028/210/157790.jpg",
                                SubCategories = new List<UI_Model_Category>()
                                {
                                    new UI_Model_Category()
                                    {
                                        Name = "Platdak",
                                        ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/19910/210/161140.jpg",
                                        SubCategories = new List<UI_Model_Category>()
                                        {
                                            new UI_Model_Category()
                                            {
                                                Name = "Douglas",
                                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/34006/265378_preview.jpg",
                                                Products = new List<UI_Model_Product>()
                                                {
                                                    new UI_Model_Product()
                                                    {
                                                        ID = 1,
                                                        Name = "Buitenverblijf Comfort | 300cm | C5Z",
                                                        Brand = "Westwood",
                                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/69/400566_thumbnail.png",
                                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/44779/438901_preview.jpeg",
                                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/44779/438901_default.jpeg" },
                                                        SiteURL = "https://nubuiten.nl/westwood-buitenverblijf-comfort-300cm-c5z",
                                                        Information = "4 types beschikbaar",
                                                        Price = "1999.00",
                                                        PriceExclusive = "1759.00",
                                                        Promotion = UI_Model_ProductPromotion.Recommended,
                                                        Options = new List<UI_Model_ProductOption>()
                                                        {
                                                            new UI_Model_ProductOption()
                                                            {
                                                                Name = "Vloer",
                                                                Required =  true,
                                                                Options = new List<UI_Model_ProductOptionEntry>
                                                                {
                                                                    new UI_Model_ProductOptionEntry
                                                                    {
                                                                        Name = "Blank",
                                                                        Price = "135.00"
                                                                    },
                                                                    new UI_Model_ProductOptionEntry
                                                                    {
                                                                        Name = "Groen geïmpregneerd",
                                                                        Price = "179.00"
                                                                    },
                                                                    new UI_Model_ProductOptionEntry
                                                                    {
                                                                        Name = "Bruin geïmpregneerd",
                                                                        Price = "179.00"
                                                                    }
                                                                }
                                                            },

                                                            new UI_Model_ProductOption()
                                                            {
                                                                Name = "Bewerkingstechniek",
                                                                Required =  false,
                                                                Options = new List<UI_Model_ProductOptionEntry>
                                                                {
                                                                    new UI_Model_ProductOptionEntry
                                                                    {
                                                                        Name = "Blank",
                                                                        Price = "0.00"
                                                                    }
                                                                }
                                                            },

                                                        }
                                                    },
                                                    new UI_Model_Product()
                                                    {
                                                        ID = 2,
                                                        Name = "Blokhut Melbourne | Excellent",
                                                        Brand = "Hillhout",
                                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/69/400566_thumbnail.png",
                                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/35389/282071_preview.jpg",
                                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/35389/282071_default.jpg" },
                                                        SiteURL = "https://nubuiten.nl/hillhout-blokhut-melbourne-excellent",
                                                        Information = "4 types beschikbaar",
                                                        Price = "2569.00",
                                                    },
                                                }
                                            },
                                            new UI_Model_Category()
                                            {
                                                Name = "Vuren",
                                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/1039/210/3212.jpg",
                                                Products = new List<UI_Model_Product>()
                                                {
                                                    new UI_Model_Product()
                                                    {
                                                        ID = 3,
                                                        Name = "Blokhut Aragon",
                                                        SiteURL = "https://nubuiten.nl/woodvision-hoekberging-aragon",
                                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/4697/210/157811.jpg",
                                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/4697/616/157811.jpg" },
                                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/orginal/dZagQYmBVzBau7JgSfNTkhTP1iDSu7nH.png",
                                                        Price = "929.16",
                                                        Information = "1 kleur beschikbaar",
                                                        Brand = "Woodvision",
                                                    },
                                                    new UI_Model_Product()
                                                    {
                                                        ID = 4,
                                                        Name = "Blokhut Lena | 635 x 340 cm",
                                                        SiteURL = "https://nubuiten.nl/woodvision-blokhut-lena",
                                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/a476a6d42e36a701b9089925640ab739/210-210_blokhut%20lena%201.jpg",
                                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/a476a6d42e36a701b9089925640ab739/616-616_blokhut%20lena%201.jpg" },
                                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/orginal/dZagQYmBVzBau7JgSfNTkhTP1iDSu7nH.png",
                                                        Price = "3011.65",
                                                        Information = "1 kleur beschikbaar",
                                                        Brand = "Woodvision",
                                                        Promotion = UI_Model_ProductPromotion.Action,
                                                    },
                                                }
                                            }
                                        },
                                        Products =  new List<UI_Model_Product>()
                                        {
                                            new UI_Model_Product()
                                            {
                                                ID = 5,
                                                Name = "Premium blokhut Ligne | 250 x 250 cm",
                                                Information = "12 afmetingen beschikbaar",
                                                IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/44763/408271_preview.jpeg",
                                                ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/44819/409240_default.jpeg" },
                                                Brand = "Smartshed",
                                                BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/f044c3e73acfcdc7e7038d0ff57a7766/80-80_smartshed-01.png",
                                                SiteURL = "https://nubuiten.nl/smartshed-premium-blokhut-ligne-250x250-cm",
                                            }
                                        }
                                    }
                                }
                            },
                            new UI_Model_Category()
                            {
                                Name = "Geïmpregneerde blokhutten",
                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/4672/210/156878.jpg",
                                Products = new List<UI_Model_Product>()
                                {
                                    new UI_Model_Product()
                                    {
                                        ID = 6,
                                        Name = "Blokhut Donau 280",
                                        Brand = "Woodvision",
                                        Information = "1 kleur beschikbaar",
                                        Price = "1107.91",
                                        Downloaded = false,
                                        Options = new List<UI_Model_ProductOption>()
                                        {
                                            new UI_Model_ProductOption()
                                            {
                                                Name = "Afwerking hout",
                                                Options = new List<UI_Model_ProductOptionEntry>
                                                {
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Blank",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Geïmpregneerd-bruin",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "geïmpregneerd-groen",
                                                        Price = "0.00"
                                                    }
                                                },
                                                Required = false,
                                            },
                                            new UI_Model_ProductOption()
                                            {
                                                Name = "Maatvoering",
                                                Options = new List<UI_Model_ProductOptionEntry>
                                                {
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Donau 172",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Donau 230",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Donau 280",
                                                        Price = "0.00"
                                                    }
                                                },
                                                Required = false,
                                            },
                                            new UI_Model_ProductOption()
                                            {
                                                Name = "Shingles",
                                                Options = new List<UI_Model_ProductOptionEntry>
                                                {
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Bruin",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Groen",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Rood",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Zwart",
                                                        Price = "0.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Grijs",
                                                        Price = "0.00"
                                                    }
                                                },
                                                Required = true,
                                            },
                                            new UI_Model_ProductOption()
                                            {
                                                Name = "Vloer",
                                                Options = new List<UI_Model_ProductOptionEntry>
                                                {
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Blank",
                                                        Price = "92.50"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "geïmpregneerd bruin",
                                                        Price = "125.00"
                                                    },
                                                    new UI_Model_ProductOptionEntry
                                                    {
                                                        Name = "Geïmpregneerd groen",
                                                        Price = "125.00"
                                                    }
                                                },
                                                Required = true,
                                            }
                                        },
                                        SiteURL = "https://nubuiten.nl/woodvision-blokhut-donau-280",
                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/orginal/dZagQYmBVzBau7JgSfNTkhTP1iDSu7nH.png",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/4672/210/156878.jpg",
                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/4672/616/156878.jpg" },
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 7,
                                        Name = "Blokhut Box geimpregneerd | 240x240 cm",
                                        Brand = "Gardenas",
                                        Price = "828.00",
                                        Downloaded = false,
                                        SiteURL = "https://nubuiten.nl/gardenas-tuinkast-calais-gd2002-gd9001-gd2020imp",
                                        ImageURLs = new List<string>(){"https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/46028/421788_original.png" },
                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/brand/299/430340_thumbnail.jpeg",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/46028/421788_preview.png",
                                    }
                                }
                            },
                            new UI_Model_Category()
                            {
                                Name = "Geverfde blokhutten",
                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/37052/310314_preview.jpg",
                                Products = new List<UI_Model_Product>()
                                {
                                    new UI_Model_Product()
                                    {
                                        ID = 8,
                                        Name = "Blokhut Usedom 245x245 | Terragrijs/Wit",
                                        Brand = "Karibu",
                                        Price = "1383.00",
                                        Downloaded = false,
                                        SiteURL = "https://nubuiten.nl/karibu-blokhut-usedom-245x245-terragrijswit",
                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/37/80/177066.png",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/37052/310314_default.jpg",
                                    }
                                }
                            },
                            new UI_Model_Category()
                            {
                                Name = "Geverfde blokhutten 2",
                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/37052/310314_preview.jpg",
                                Products = new List<UI_Model_Product>()
                                {
                                    new UI_Model_Product()
                                    {
                                        ID = 9,
                                        Name = "Blokhut Usedom 245x245 | Terragrijs/Wit",
                                        Brand = "Karibu",
                                        Price = "1383.00",
                                        Downloaded = false,
                                        SiteURL = "https://nubuiten.nl/karibu-blokhut-usedom-245x245-terragrijswit",
                                        BrandImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/37/80/177066.png",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/product/37052/310314_default.jpg",
                                    }
                                }
                            },
                        }
                    },
                    new UI_Model_Category()
                    {
                        Name = "Schutting",
                        SubCategories = new List<UI_Model_Category>()
                        {
                            new UI_Model_Category()
                            {
                                Name = "Grenen",
                                ImageURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/210-210_plank%2015.png",
                                Products = new List<UI_Model_Product>()
                                {
                                    new UI_Model_Product()
                                    {
                                        ID = 10,
                                        Name = "Grenen plankenschermen, type 1",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Action,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 11,
                                        Name = "Grenen plankenschermen, type 2",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Action,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 12,
                                        Name = "Grenen plankenschermen, type 3",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 13,
                                        Name = "Grenen plankenschermen, type 4",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Recommended,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 14,
                                        Name = "Grenen plankenschermen, type 5",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Recommended,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 15,
                                        Name = "Grenen plankenschermen, type 6",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 16,
                                        Name = "Grenen plankenschermen, type 7",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                   {
                                        ID = 17,
                                        Name = "Grenen plankenschermen, type 8",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Action,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                   },
                                    new UI_Model_Product()
                                   {
                                        ID = 18,
                                        Name = "Grenen plankenschermen, type 9",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                   },
                                    new UI_Model_Product()
                                    {
                                        ID = 19,
                                        Name = "Grenen plankenschermen, type 10",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 20,
                                        Name = "Grenen plankenschermen, type 11",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Recommended,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 21,
                                        Name = "Grenen plankenschermen, type 12",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 22,
                                        Name = "Grenen plankenschermen, type 13",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 23,
                                        Name = "Grenen plankenschermen, type 14",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.None,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                    new UI_Model_Product()
                                    {
                                        ID = 24,
                                        Name = "Grenen plankenschermen, type 15",
                                        Brand = "Woodvision",
                                        IconURL = "https://s3-eu-west-1.amazonaws.com/nubuiten-images/6038b6f9c20b9fab024bab681423eaa9/616-616_plank%2015.png",
                                        Price = "37,75",
                                        BrandImageURL = "https://nubuiten.nl/ontdek-nubuiten/merken/woodvision",
                                        Downloaded = true,
                                        Promotion = UI_Model_ProductPromotion.Action,
                                        SiteURL = "https://nubuiten.nl/plankenscherm-grenen-13mm",
                                    },
                                }
                            }
                        }
                    }
                }
            };
        }
    }
}
