﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecificationLine : MonoBehaviour {
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text text;

    public void Initialize(Color color, string str) {
        image.color = color;
        this.text.text = str;
    }
}