﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Modules : MonoBehaviour
{
    [Header("Wishlist / Favorite Module")]
    public bool HasWishlist;
    public bool WishlistRequiresLogin;

    [Header("Shopping Cart Module")]
    public bool HasShoppingCart;
    public bool ShoppingCartRequiresLogin;

    [Header("Quotation Module")]
    public bool HasQuotation;
    public bool QuotationRequiresLogin;
    public string QuotationAddress;

    [Header("Login Module")]
    public bool HasLogin;

    [Header("Measurement Module")]
    public bool HasMeasurement;

    [Header("Screenshot Module")]
    public bool HasScreenshot;
    
    [Header("Firebase Module")]
    public bool HasFirebase;
    public bool LogProducts;
    public bool LogLogin;
    public bool LogUsageDuration;

    [Header("Color Customisation Module")]
    public bool HasColorCustomisation;

    [Header("Extra Settings")]
    public bool HasFooter;
    public bool UseNuBuitenAppFlow;
}
