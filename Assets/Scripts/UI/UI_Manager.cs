﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VROwl.AR.Placement;
using VROwl.AR.Measurements;
using VROwl.Miscellaneous;
using VROwl.Network.BackofficeApi;
using VROwl.Network.ContentDeliveryApi;
using VROwl.UI.Components;
using VROwl.UI.Models;
using VROwl.UI.Overlays;
using VROwl.UI.Pages;
using VROwl.UI.Popups;
using Assets.Scripts.Miscellaneous;
using VROwl.Network;
using VROwl.AR.Models;
using System.IO;
using VROwl.UI.Analytics;
using VROwl.Network.BackofficeApi.Models;

namespace VROwl.UI
{

    /// <summary>
    /// UI Manager takes care of the UI and navigation. It uses a state to decide which page and
    /// which components are shown.
    /// </summary>
    public class UI_Manager : AsyncRunner
    {

        public static bool EditorIsPhone = true;

        [HideInInspector]
        public static UI_Manager instance;

        [Header("UI States")]
        // The pagestate describes the current page.
        public PageID Page = PageID.HOMEPAGE;

        // The overlaystate describes the current overlay, and if there is one active.
        public OverlayID Overlay = OverlayID.NONE;

        // The overlaystate describes the current overlay, and if there is one active.
        public PopupID Popup = PopupID.NONE;
        
        [Header("Test Settings")]
        public bool Test = false;
        public float FakeDelay = 1;
        public bool IgnoreMissingModelInfo = false;
        private UI_Model_Catalogue testCatalogue;
        [Space(10)]
        public bool testEditorIsPhone = false;
        
        [Header("UI Object References")]
        public UI_Stylesheet Stylesheet;
        public UI_Analytics Analytics;
        public Modules Modules;
        public UI_Component_Header Header;
        public UI_Overlay_Product ProductOverlayTablet;
        public UI_Overlay_Product ProductOverlayPhone;
        public GameObject OverlayObject;
        public GameObject PopupObject;
        public GameObject UICamera;

        [Header("API References")]
        [SerializeField]
        private ContentDeliveryApiClient contentDeliveryApiClient;
        [SerializeField]
        private BackofficeApiClient backofficeApiClient;
        [SerializeField]
        private string webshopUrl;

        [Header("UI Prefab References")]

        [HideInInspector]
        public UI_Model_Catalogue Catalogue;

        [HideInInspector]
        public GameObject FooterPrefab
        {
            get
            {
                if (IsPhone())
                {
                    return PhoneFooterPrefab;
                }
                else
                {
                    return TabletFooterPrefab;
                }
            }
        }
        [HideInInspector]
        public UI_Component_Category CategoryPrefab
        {
            get
            {
                if (IsPhone())
                {
                    return PhoneCategoryPrefab;
                }
                else
                {
                    return TabletCategoryPrefab;
                }
            }
        }

        [Header("UI Component Prefab References")]
        public GameObject TabletFooterPrefab;
        public GameObject PhoneFooterPrefab;
        public UI_Component_Category PhoneCategoryPrefab;
        public UI_Component_Category TabletCategoryPrefab;

        [SerializeField]
        private string arSceneName;

        [SerializeField]
        private string uiSceneName;
        
        // All the pages;
        private UI_Page[] pages;

        // All the popups;
        private UI_Popup[] popups;

        // Shopping cart list: key is product id, value is entry description.
        public UI_ShoppingCart ShoppingCart;
        private Coroutine basketUpdateCoroutine;

        // Quotation list: key is product, value is amount in quotation.
        private Dictionary<UI_Model_Product, int> Quotation = new Dictionary<UI_Model_Product, int>();

        // Favorite list.
        public int wishlistId = -1;
        // Wishlist dictionary: key is product id, value is entry id.
        private Dictionary<int, int> Favorites = new Dictionary<int, int>();
        private HashSet<int> pendingFavoriteDeletion = new HashSet<int>();
        private HashSet<int> pendingFavoriteAddition = new HashSet<int>();
        private Coroutine wishlistUpdateCoroutine;

        private Action ARSceneCallback;

        private HashSet<int> activeDownloads = new HashSet<int>();
        private DownloadService downloadService;
        private ModelCache modelCache;

        // References to AR scene objects to unsubscribe from events.
        ARPlacementInteraction arInteraction;
        MeasuringTapeInteraction measuringInteraction;
        BasePlaneManager basePlaneManager;

        public Action OnCartUpdated;

        private void Awake()
        {
            //Check if instance already exists
            if (instance == null)
                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);
            DontDestroyOnLoad(this);
        }

        /// <summary>
        /// Start is called once when the object is loaded.
        /// </summary>
        private void Start()
        {

            downloadService = new DownloadService(new DownloadService.Settings(), this);
            modelCache = new ModelCache();

            ShoppingCart = new UI_ShoppingCart();

            var defaultModelLoader = GetComponent<DefaultModelLoader>();
            if (defaultModelLoader)
            {
                defaultModelLoader.UpdateDefaultModels(modelCache);
            }

            Initiate();

            // Show loading page while going to the homepage.
            GoToPage(PageID.LOADINGPAGE);

            //
            if (PlayerPrefs.HasKey("User")) {
                UserModelPrefs ump = JsonUtility.FromJson<UserModelPrefs>(PlayerPrefs.GetString("User"));
                if (ump.token == null) {
                    PlayerPrefs.DeleteKey("User");
                }
                else {
                    contentDeliveryApiClient.GetUserFromToken(ump.id, user => {
                        backofficeApiClient.User = new UserModel() {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            Id = user.Id,
                            Token = ump.token
                        };

                        FindWishlistId();
                        wishlistUpdateCoroutine = StartCoroutine(RefreshWishlist());
                        basketUpdateCoroutine = StartCoroutine(RefreshBasket());

                        if (Analytics != null)
                            Analytics.Login();

                        RefreshUI();
                        GoToPage(PageID.HOMEPAGE);
                    },
                    (errorcode, message) => {
                        Debug.LogWarningFormat("Could not log in: {0}: {1}", errorcode, message);
                    });
                }
            }
            //
            else {
                if (Application.internetReachability == NetworkReachability.NotReachable) {
                    OpenPopup(PopupID.CONNECTIONISSUE);
                }
                else {
                    RefreshUI();
                    GoToPage(PageID.HOMEPAGE);
                }
            }
        }

        private int pic = 0;
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F11))
            {
                ScreenCapture.CaptureScreenshot(pic + ".png");
                pic++;
            }
        }

        public static UI_Device GetDeviceType()
        {
            if(IsPhone())
            {
                return UI_Device.PHONE;
            }
            else
            {
                return UI_Device.TABLET;
            }
        }

        /// <summary>
        /// Check if the device is a phone. Based on screen height.
        /// </summary>
        public static bool IsPhone()
        {
#if IOS
            if(UnityEngine.iOS.Device.generation.ToString().ToLower().Contains("ipad"))
            {
                return false;
            }
            else
            {
                return true;
            }
#endif
#if UNITY_EDITOR
            EditorIsPhone = UI_Manager.instance.testEditorIsPhone;
            return UI_Manager.instance.testEditorIsPhone;
#endif
            float diagonal = DeviceDiagonalSizeInInches();
            return diagonal <= 6.5f;
        }

        /// <summary>
        /// Get the diagonal size of the device.
        /// </summary>
        public static float DeviceDiagonalSizeInInches()
        {
            float screenWidth = Screen.width / Screen.dpi;
            float screenHeight = Screen.height / Screen.dpi;

#if UNITY_EDITOR
            try
            {
                string[] res = UnityEditor.UnityStats.screenRes.Split('x');
                screenHeight = float.Parse(res[0]) / Screen.dpi;
                screenWidth = float.Parse(res[1]) / Screen.dpi;
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
#endif

            float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

            return diagonalInches;
        }

        /// <summary>
        /// Set the state of the UI to a page. This refreshes the UI to show the new page.
        /// </summary>
        /// <param name="id"></param>
        public void GoToPage(PageID id)
        {
            if (id == Page)
                return;
            PageID previous = Page;
            Page = id;
            if (Modules.HasFirebase && Analytics != null)
                Analytics.GoToPage(id);
            Overlay = OverlayID.NONE;
            UI_Page currentPage = GetCurrentPage();
            currentPage.LoadPage(succes =>
            {
                if (!succes)
                {
                    Page = previous;
                }
                RefreshUI();
            });
        }

        /// <summary>
        /// Set the state of the UI to a page. This refreshes the UI to show the new page.
        /// </summary>
        /// <param name="page"></param>
        public void GoToPage(UI_Page page)
        {
            if (page != null)
                GoToPage(page.ID);
        }

        /// <summary>
        /// Set the state of the UI to the result page and fill it with the category data.
        /// </summary>
        /// <param name="category"></param>
        public void GoToResultPage(UI_Model_Category category)
        {
            GoToPage(PageID.RESULTPAGE);
            UI_Page_Result resultPage = GetPage<UI_Page_Result>();
            if (resultPage != null)
                resultPage.Fill(category);
        }

        /// <summary>
        /// Initiate the UI and pages.
        /// </summary>
        private void Initiate()
        {
            // Find all pages and initiate them.
            pages = GetComponentsInChildren<UI_Page>(true);
            if (pages == null || pages.Length <= 0)
                return;
            foreach (UI_Page page in pages)
                page.Initiate(this);

            // Find all overlays and initiate them.
            if (ProductOverlayTablet != null)
            {
                ProductOverlayTablet.Initiate(this);
            }
            if (ProductOverlayPhone != null)
            {
                ProductOverlayPhone.Initiate(this);
            }

            // Find all popups and initiate them.
            popups = GetComponentsInChildren<UI_Popup>(true);
            if (popups == null || popups.Length <= 0)
                return;
            foreach (UI_Popup popup in popups)
                popup.Initiate(this);

            // Initiate the header.
            Header.Initiate(this);
            if(Analytics != null)
                Analytics.Initiate(this);
        }

        /// <summary>
        /// Refresh the UI of most UI components and pages.
        /// </summary>
        private void RefreshUI()
        {
            UI_Page currentPage = GetCurrentPage();
            Header.gameObject.SetActive(currentPage != null && currentPage.HasHeader);
            UICamera.SetActive(Page != PageID.ARPAGE);
            OverlayObject.SetActive(Overlay != OverlayID.NONE);
            PopupObject.SetActive(Popup != PopupID.NONE);
            foreach (UI_Page page in pages)
                page.Refresh(Page);            
            foreach (UI_Popup popup in popups)
                popup.Refresh(Popup);

            if (ProductOverlayTablet != null)
                ProductOverlayTablet.Refresh(Overlay);
            if (ProductOverlayPhone != null)
                ProductOverlayPhone.Refresh(Overlay);

            Header.Refresh();
        }

        /// <summary>
        /// Get the page of type T.
        /// </summary>
        /// <typeparam name="T">The page type.</typeparam>
        /// <returns>The page, if there is one.</returns>
        public T GetPage<T>()
        {
            foreach (UI_Page page in pages)
            {
                if (page.GetType() == typeof(T))
                    return (T)System.Convert.ChangeType(page, typeof(T));
            }
            return default(T);
        }

        /// <summary>
        /// Get the current page.
        /// </summary>
        /// <returns></returns>
        public UI_Page GetCurrentPage()
        {
            return GetPage(Page);
        }

        /// <summary>
        /// Get the page of type state.
        /// </summary>
        /// <param name="id">The page identifier.</param>
        /// <returns>The page if there is one.</returns>
        public UI_Page GetPage(PageID id)
        {
            return (from p in pages where p.ID == id select p).FirstOrDefault();
        }
        
        /// <summary>
        /// Close the overlay.
        /// </summary>
        public void CloseOverlay()
        {
            Overlay = OverlayID.NONE;
            RefreshUI();
        }

        /// <summary>
        /// Set the state of the UI to an overlay. This refreshes the UI to show the new overlay.
        /// </summary>
        /// <param name="id"></param>
        public void OpenOverlay(OverlayID id)
        {
            if (id == Overlay)
                return;
            Overlay = id;
            RefreshUI();
        }

        /// <summary>
        /// Set the state of the UI to an overlay. This refreshes the UI to show the new overlay.
        /// </summary>
        /// <param name="overlay"></param>
        public void OpenOverlay(UI_Overlay overlay)
        {
            if (overlay != null)
                OpenOverlay(overlay.ID);
        }

        /// <summary>
        /// Opens the overlay with the given product.
        /// </summary>
        /// <param name="product"></param>
        public void OpenProductOverlay(UI_Model_Product product)
        {
            if (Analytics != null)
                Analytics.ViewProduct(product);
            OpenOverlay(OverlayID.PRODUCTOVERLAY);
            UI_Overlay_Product productOverlay = IsPhone() ? ProductOverlayPhone : ProductOverlayTablet;
            if (productOverlay != null && product != null)
            {
                productOverlay.Loading();
                LoadProduct(p =>
                {
                    productOverlay.Fill(p);
                    productOverlay.DoneLoading();
                }, product.ID);
            }
        }

        /// <summary>
        /// Open the popup for color selections
        /// </summary>
        /// <param name="options"></param>
        public void OpenColorSelectionPopup(List<UI_ColorOption> options, Action callback = null)
        {
            OpenPopup(PopupID.COLORSELECTION);
            UI_Popup_ColorSelection colorPopup = GetPopup<UI_Popup_ColorSelection>();
            if (colorPopup != null)
            {
                colorPopup.Fill(options, callback);
            }
        }
        
        /// <summary>
        /// Open the popup for color selections
        /// </summary>
        /// <param name="options"></param>
        public void OpenOptionSelectionPopup(UI_Model_ProductOption options, Action callback = null)
        {
            OpenPopup(PopupID.OPTIONSELECTION);
            UI_Popup_Options optionsPopup = GetPopup<UI_Popup_Options>();
            if (optionsPopup != null)
            {
                optionsPopup.Fill(options, callback);
            }
        }

        /// <summary>
        /// Open a popup that asks a yes or no question.
        /// </summary>
        public void OpenYesNoPopup(Action yesCallback, Action noCallback, string content, string yesText = "Ja", string noText = "Nee")
        {
            OpenPopup(PopupID.GENERIC_YES_NO);
            UI_Popup_GenericYesNo popup = GetPopup<UI_Popup_GenericYesNo>();
            if (popup != null)
            {
                popup.Open(yesCallback, noCallback, content, yesText, noText);
            }
        }

        /// <summary>
        /// Open a popup that show a ok statement.
        /// </summary>
        public void OpenOkPopup(Action okCallback, string content, string okText = "Ok")
        {
            OpenPopup(PopupID.GENERIC_OK);
            UI_Popup_GenericOk popup = GetPopup<UI_Popup_GenericOk>();
            if (popup != null)
            {
                popup.Open(okCallback, content, okText);
            }
        }


        /// <summary>
        /// Get the popup of type T.
        /// </summary>
        /// <typeparam name="T">The popup type.</typeparam>
        /// <returns>The overlay, if there is one.</returns>
        public T GetPopup<T>()
        {
            foreach (UI_Popup popup in popups)
            {
                if (popup.GetType() == typeof(T))
                    return (T)System.Convert.ChangeType(popup, typeof(T));
            }
            return default(T);
        }

        /// <summary>
        /// Get the popup of type state.
        /// </summary>
        /// <param name="id">The popup identifier.</param>
        /// <returns>The popup if there is one.</returns>
        public UI_Popup GetPopup(PopupID id)
        {
            return (from p in popups where p.ID == id select p).FirstOrDefault();
        }

        /// <summary>
        /// Close the popup.
        /// </summary>
        public void ClosePopup()
        {
            Popup = PopupID.NONE;
            RefreshUI();
        }

        /// <summary>
        /// Set the state of the UI to an popup. This refreshes the UI to show the new popup.
        /// </summary>
        public void OpenPopup(PopupID id)
        {
            if (id == Popup)
                return;
            Popup = id;
            RefreshUI();
        }

        /// <summary>
        /// Set the state of the UI to an popup. This refreshes the UI to show the new popup.
        /// </summary>
        public void OpenPopup(UI_Popup popup)
        {
            if (popup != null)
                OpenPopup(popup.ID);
        }

        /// <summary>
        /// Open a website with an external app. This is used by UI events.
        /// </summary>
        /// <param name="url">The url to go to.</param>
        public void GoToURL(string url)
        {
            if(Page == PageID.ARPAGE)
            {
                UI_Popup_LeavingAR ARPopup = GetPopup<UI_Popup_LeavingAR>();
                if (ARPopup != null)
                {
                    ARPopup.YesClicked = () =>
                    {
                        ForceGoToURL(url);
                    };
                    ARPopup.NoClicked = ClosePopup;
                    OpenPopup(ARPopup);
                }
                else
                {
                    ForceGoToURL(url);
                }
            }
            else
            {
                ForceGoToURL(url);
            }
        }

        private void ForceGoToURL(string url)
        {
            if (!string.IsNullOrEmpty(webshopUrl))
            {
                var builder = new UriBuilder(webshopUrl);
                builder.Path = url;

                url = builder.ToString();
            }

            if (LoggedIn())
            {
                backofficeApiClient.GetSSOToken(token =>
                {
                    var builder = new UriBuilder(url);
                    builder.Query = "single-sign-in-token=" + token;

                    Application.OpenURL(builder.ToString());
                },
                (code, msg) => Application.OpenURL(url));
            }
            else
            {
                Application.OpenURL(url);
            }
        }

        /// <summary>
        /// Checks whether the user is logged in and is authenticated.
        /// </summary>
        /// <returns>Wether the user is logged in</returns>
        public bool LoggedIn()
        {
            return backofficeApiClient.LoggedIn;
        }

        /// <summary>
        /// Try to log in. 
        /// </summary>
        public void LogIn(string username, string password, Action onSucces, Action onFailure)
        {
            backofficeApiClient.LogIn(username, password,
                user =>
                {
                    FindWishlistId();
                    wishlistUpdateCoroutine = StartCoroutine(RefreshWishlist());
                    basketUpdateCoroutine = StartCoroutine(RefreshBasket());

                    if (onSucces != null)
                        onSucces();
                    if (Analytics != null)
                        Analytics.Login();

                    UserModelPrefs ump = new UserModelPrefs() {
                        firstName = user.FirstName,
                        lastName = user.LastName,
                        email = user.Email,
                        id = user.Id,
                        token = user.Token
                    };

                    string dataAsJson = JsonUtility.ToJson(ump);
                    PlayerPrefs.SetString("User", dataAsJson);
                },
                (code, message) =>
                {
                    Debug.LogWarningFormat("Could not log in: {0}: {1}", code, message);

                    if (onFailure != null) 
                        onFailure();
                });
        }

        /// <summary>
        /// Try to log out.
        /// </summary>
        public void LogOut()
        {
            StopCoroutine(basketUpdateCoroutine);
            StopCoroutine(wishlistUpdateCoroutine);
            wishlistId = -1;

            if (PlayerPrefs.HasKey("User")) {
                PlayerPrefs.DeleteKey("User");
            }

            backofficeApiClient.LogOut();
        }

        /// <summary>
        /// Open the AR scene and UI.
        /// </summary>
		public void GoToAR()
        {
            if (Analytics != null)
                Analytics.GoToAR();
            PlaceProduct(new UI_Model_Product() { Name = "AR" });
        }

        /// <summary>
        /// Place a product in AR.
        /// </summary>
        /// <param name="product">The product.</param>
        public void PlaceProduct(UI_Model_Product product)
        {
            if (Analytics != null)
                Analytics.PlaceProduct(product);
            if (Page == PageID.ARPAGE)
            {
                PlaceProductInAR(product);
            }
            else
            {
                if (!string.IsNullOrEmpty(arSceneName))
                {
                    GoToPage(PageID.ARPAGE);
                    UnityEngine.SceneManagement.SceneManager.sceneLoaded += ARSceneLoaded;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(arSceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
                    ARSceneCallback = () => 
                    {
                        PlaceProductInAR(product);
                    };
                }
                else
                {
                    Debug.LogError("arSceneName is empty");
                }
            }
        }

        /// <summary>
        /// Start measuring the space in AR.
        /// </summary>
        public void StartMeasuring()
        {
            if (Page == PageID.ARPAGE)
            {
                StartMeasuringInAR();
            }
            else
            {
                if (!string.IsNullOrEmpty(arSceneName))
                {
                    GoToPage(PageID.ARPAGE);
                    UnityEngine.SceneManagement.SceneManager.sceneLoaded += ARSceneLoaded;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(arSceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
                    ARSceneCallback = () =>
                    {
                        StartMeasuringInAR();
                    };
                }
                else
                {
                    Debug.LogError("arSceneName is empty");
                }
            }
        }

        /// <summary>
        /// Leave the AR modus and go to the target page. This warns the user first.
        /// </summary>
        /// <param name="target"></param>
        public void LeaveAR(PageID target)
        {
            if (Analytics != null)
                Analytics.LeaveAR();
            UI_Popup_LeavingAR ARPopup = GetPopup<UI_Popup_LeavingAR>();
            if (ARPopup != null)
            {
                ARPopup.YesClicked = () =>
                {
                    UnsubscribeAREvents();
                    UnityEngine.SceneManagement.SceneManager.LoadScene(uiSceneName);
                    GoToPage(target);
                    ClosePopup();
                };
                ARPopup.NoClicked = ClosePopup;
                OpenPopup(ARPopup);
            }
            else
            {
                UnsubscribeAREvents();

                UnityEngine.SceneManagement.SceneManager.LoadScene(uiSceneName);
                GoToPage(target);
            }
        }

        private void UnsubscribeAREvents()
        {
            UI_Page_AR arPage = GetPage<UI_Page_AR>();

            if (arPage != null)
            {
                // Unsubscribe from events
                arPage.OnDeleteClicked -= arInteraction.DeleteSelection;
                arPage.OnPlaceClicked -= arInteraction.Deselect;
                arPage.OnHomeClicked -= GoHomeFromAR;
                arPage.OnQuotationClicked -= GoToQuotation;

                arPage.OnMeasureClicked -= measuringInteraction.EnableMeasuring;
                arPage.OnMeasureStartClicked -= measuringInteraction.StartMeasuring;
                arPage.OnMeasureEndClicked -= measuringInteraction.StopMeasuring;
                arPage.OnMeasureCancelClicked -= measuringInteraction.CancelMeasuring;
                arPage.OnMeasureDeleteClicked -= measuringInteraction.DeleteSelection;

                arPage.OnMeasureClicked -= arInteraction.Deselect;
                arPage.OnScreenshotClicked -= arInteraction.Deselect;
                arPage.OnAddClicked -= arInteraction.Deselect;

                // Event from AR to UI
                arInteraction.OnSelected -= arPage.SelectObject;
                arInteraction.OnDeselected -= arPage.DeselectObject;

                measuringInteraction.OnSelected -= arPage.SelectMeasurement;
                measuringInteraction.OnDeselected -= arPage.DeselectMeasurement;
                measuringInteraction.OnEdit -= arPage.StartMeasuring;

                basePlaneManager.OnFirstPlaneFound -= arPage.HideARInstructions;
                basePlaneManager.OnFirstPlaneFound -= arPage.OnPlaneFound;
            }
        }

        /// <summary>
        /// The Scene loaded callback.
        /// </summary>
        private void ARSceneLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode mode)
        {
            UnityEngine.SceneManagement.SceneManager.sceneLoaded -= ARSceneLoaded;

            if (scene.name == arSceneName)
            {
                arInteraction = FindObjectOfType<ARPlacementInteraction>();
                measuringInteraction = FindObjectOfType<MeasuringTapeInteraction>();
                basePlaneManager = FindObjectOfType<BasePlaneManager>();

                UI_Page_AR arPage = GetPage<UI_Page_AR>();

#if !UNITY_EDITOR
                StaticARKitBridge.ResetScene();
#endif

                arPage.AR = arInteraction;
                
                // Events from UI to AR
                arPage.OnDeleteClicked += arInteraction.DeleteSelection;
                arPage.OnPlaceClicked += arInteraction.Deselect;
                arPage.OnHomeClicked += GoHomeFromAR;
                arPage.OnQuotationClicked += GoToQuotation;

                arPage.OnMeasureClicked += measuringInteraction.EnableMeasuring;
                arPage.OnMeasureStartClicked += measuringInteraction.StartMeasuring;
                arPage.OnMeasureEndClicked += measuringInteraction.StopMeasuring;
                arPage.OnMeasureCancelClicked += measuringInteraction.CancelMeasuring;
                arPage.OnMeasureDeleteClicked += measuringInteraction.DeleteSelection;

                arPage.OnMeasureClicked += arInteraction.Deselect;
                arPage.OnScreenshotClicked += arInteraction.Deselect;
                arPage.OnAddClicked += arInteraction.Deselect;
                arPage.OnColorClicked += arInteraction.OpenColorPopup;

                // Event from AR to UI
                arInteraction.OnSelected += arPage.SelectObject;
                arInteraction.OnDeselected += arPage.DeselectObject;
                arInteraction.OnUpdateComplexity += arPage.SetComplexityProgress;
                arInteraction.uiManager = this;

                measuringInteraction.OnSelected += arPage.SelectMeasurement;
                measuringInteraction.OnDeselected += arPage.DeselectMeasurement;
                measuringInteraction.OnEdit += arPage.StartMeasuring;

                basePlaneManager.OnFirstPlaneFound += arPage.HideARInstructions;
                basePlaneManager.OnFirstPlaneFound += arPage.OnPlaneFound;

                if (ARSceneCallback != null)
                {
                    ARSceneCallback();
                }
            }
        }

        /// <summary>
        /// Start placing a product in AR. This is the callback when the AR scene is loaded and should not be called directly.
        /// </summary>
        private void PlaceProductInAR(UI_Model_Product product)
        {
            ARPlacementInteraction arInteraction = FindObjectOfType<ARPlacementInteraction>();

            if (arInteraction == null)
            {
                Debug.LogWarning("Send UI_Model_Product to AR scene!");
            }
            else
            {
                arInteraction.StartPlacement(product);
            }
            CloseOverlay();
            ClosePopup();
            GetPage<UI_Page_AR>().CloseOverlay();
        }

        public void OpenProductOverlayOfSelectedObject()
        {
            ARPlacementInteraction arInteraction = FindObjectOfType<ARPlacementInteraction>();
            if (arInteraction != null && arInteraction.Selection != null && arInteraction.Selection.productInfo != null)
            {
                OpenProductOverlay(arInteraction.Selection.productInfo);
            }
        }

        /// <summary>
        /// Start measuring in AR. This is the callback when the AR scene is loaded and should not be called directly.
        /// </summary>
        private void StartMeasuringInAR()
        {
            MeasuringTapeInteraction arInteraction = FindObjectOfType<MeasuringTapeInteraction>();

            if (arInteraction != null)
            { 
                arInteraction.EnableMeasuring();
            }
            CloseOverlay();
            ClosePopup();
            GetPage<UI_Page_AR>().CloseOverlay();
            GetPage<UI_Page_AR>().MeasureClicked();
        }

        /// <summary>
        /// Leave AR and go to the HomePage
        /// </summary>
        private void GoHomeFromAR()
        {
            LeaveAR(PageID.HOMEPAGE);
        }

        /// <summary>
        /// Leave AR and go to the Quotation Page and add the currently placed objects to the quotation.
        /// </summary>
        private void GoToQuotation()
        {
            foreach (ARPlaceable placeable in FindObjectsOfType<ARPlaceable>())
            {
                AddToQuotation(placeable.productInfo, placeable.NrOfProducts);
            }
            LeaveAR(PageID.QUOTATIONPAGE);
        }

        /// <summary>
        /// Check if buttons, icons and pages featuring favorite (or wishlist) are visible.
        /// </summary>
        public bool IsFavoriteVisible()
        {
            if (Modules.WishlistRequiresLogin)
            {
                return LoggedIn() && Modules.HasWishlist;
            }
            else
            {
                return Modules.HasWishlist;
            }
        }

        /// <summary>
        /// Check if buttons, icons and pages featuring the shopping cart are visible.
        /// </summary>
        public bool IsShoppingCartVisible()
        {
            if (Modules.ShoppingCartRequiresLogin)
            {
                return LoggedIn() && Modules.HasShoppingCart;
            }
            else
            {
                return Modules.HasShoppingCart;
            }
        }

        /// <summary>
        /// Check if buttons, icons and pages featuring the quotation are visible.
        /// </summary>
        public bool IsQuotationVisible()
        {
            if (Modules.QuotationRequiresLogin)
            {
                return LoggedIn() && Modules.HasQuotation;
            }
            else
            {
                return Modules.HasQuotation;
            }
        }

        public bool IsModelInCache(UI_Model_Product product)
        {
            return modelCache.IsModelInCache(product.ID);
        }

        /// <summary>
        /// Get the download state of a product.
        /// </summary>
        public UI_Model_ProductDownloadState GetDownloadState(UI_Model_Product product)
        {
            if(Test)
            {
                return UI_Model_ProductDownloadState.NONE;
            }
            else
            {
                if (activeDownloads.Contains(product.ID))
                {
                    return UI_Model_ProductDownloadState.DOWNLOADING;
                }
                else if (modelCache.HasModel(product.ID))
                {
                    if(product.ARModelInfo != null)
                    {
                        return modelCache.IsModelUpToDate(product.ID, product.ARModelInfo.Hash) ? UI_Model_ProductDownloadState.DOWNLOADED : UI_Model_ProductDownloadState.OUTDATED;
                    }
                }

                return UI_Model_ProductDownloadState.NONE;
            }
        }

        /// <summary>
        /// Download the model of a product.
        /// </summary>
        public void DownloadProductModel(UI_Model_Product product, Action<bool> callback)
        {
            if (Test)
            {
                PlanAction(() =>
                {
                    callback(true);
                }, FakeDelay);
            }
            else
            {
                if (product.ARModelInfo == null)
                {
                    callback(false);
                }
                else if (!activeDownloads.Contains(product.ID))
                {
                    activeDownloads.Add(product.ID);

                    downloadService.Download(product.ARModelInfo.Url, Path.Combine(ModelCache.CacheFolder, product.ARModelInfo.FileName), results =>
                    {
                        activeDownloads.Remove(product.ID);

                        var result = results.FirstOrDefault();
                        if(result == null || result.HasError)
                        {
                            Debug.LogWarningFormat("Could not download model from url [{0}] Error: {1}", result.OriginalRequest.DownloadUrl, result.ErrorMessage);
                            callback(false);
                        }
                        else
                        {
                            modelCache.SaveDownloadedModel(product.ID, result, product.ARModelInfo.Hash);
                            callback(true);
                        }
                    });
                }
            }
        }

        public void ClearCache()
        {
            modelCache.ClearCache();
        }

        public void DeleteAllModels(Action<bool> callback)
        {
            List<UI_Model_Product> products = new List<UI_Model_Product>();
            if(products != null && products.Count > 0)
            {
                DeleteProductModel(products, 0, callback);
            }
            else
            {
                callback(true);
            }
        }


        private void DeleteProductModel(List<UI_Model_Product> products, int current, Action<bool> callback)
        {
            if (current >= products.Count)
            {
                callback(true);
                return;
            }
            else
            {
                DeleteProductModel(products[current], (result) =>
                {
                    if (result)
                    {
                        DeleteProductModel(products, current + 1, callback);
                        return;
                    }
                    else
                    {
                        callback(false);
                        return;
                    }
                });
            }
        }

        /// <summary>
        /// Delete the model of a product.
        /// </summary>
        public void DeleteProductModel(UI_Model_Product product, Action<bool> callback)
        {
            if(Test)
            {
                PlanAction(() => {
                    callback(true);
                }, FakeDelay);
            }
            else
            {
                modelCache.DeleteModel(product.ID);
                callback(modelCache.HasModel(product.ID));
            }
        }

		public string GetModelPath(UI_Model_Product product)
		{
			return modelCache.GetModelPath(product.ID);
		}

        /// <summary>
        /// Mark a product as favorite or add it to the wishlist (same thing).
        /// </summary>
        public void AddToFavorites(UI_Model_Product product, Action<bool> callback)
        {
            if (!Favorites.ContainsKey(product.ID) && !pendingFavoriteAddition.Contains(product.ID))
            {
                pendingFavoriteAddition.Add(product.ID);
                backofficeApiClient.AddWishlistEntry(wishlistId, product.ID, 1,
                    wishlist =>
                    {
                        Favorites = wishlist.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId, p => p.Id);
                        pendingFavoriteAddition.Remove(product.ID);
                        callback(true);
                        Analytics.ProductAddedToWishlist(product);
                    },
                    (code, msg) =>
                    {
                        LoadFavorites(favorites =>
                        {
                            pendingFavoriteAddition.Remove(product.ID);
                            callback(false);
                        });
                        Debug.LogWarningFormat("Could not add to wishlist: {0} {1}", code, msg);
                    });
            }
        }

        /// <summary>
        /// Remove a product from favorites or remove it from the wishlist (same thing).
        /// </summary>
        public void RemoveFromFavorites(UI_Model_Product product, Action<bool> callback)
        {
            if(Favorites.ContainsKey(product.ID) && !pendingFavoriteDeletion.Contains(product.ID))
            {
                pendingFavoriteDeletion.Add(product.ID);
                backofficeApiClient.DeleteWishListEntry(wishlistId, Favorites[product.ID],
                () =>
                {
                    LoadFavorites(favorites => {
                        pendingFavoriteDeletion.Remove(product.ID);
                        callback(true);
                        Analytics.ProductRemovedFromWishlist(product);
                    });
                },
                (code, msg) =>
                {
                    LoadFavorites(favorites =>
                    {
                        pendingFavoriteDeletion.Remove(product.ID);
                        callback(false);
                    });
                    Debug.LogWarningFormat("Could not remove from wishlist: {0} {1}", code, msg);
                });
            }
        }

        /// <summary>
        /// Check if the product is a favorite or in the wishlist (same thing).
        /// </summary>
        public bool IsFavorite(int productId)
        {
            return pendingFavoriteAddition.Contains(productId) || (Favorites.ContainsKey(productId) && !pendingFavoriteDeletion.Contains(productId));
        }

        /// <summary>
        /// Check if the product is in the shopping cart.
        /// </summary>
        public bool IsInCart(UI_Model_Product product)
        {
            return ShoppingCart.Cart.ContainsKey(product.ID);
        }

        /// <summary>
        /// Check if the product is in the quotation.
        /// </summary>
        public bool IsInQuotation(UI_Model_Product product)
        {
            return Quotation.ContainsKey(product);
        }

        /// <summary>
        /// Get the amount of a product in the shopping cart.
        /// </summary>
        public int GetAmountInCart(UI_Model_Product product)
        {
            if (IsInCart(product))
            {
                return ShoppingCart.Cart[product.ID].Amount;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get the amount of a product in the quotation.
        /// </summary>
        public int GetAmountInQuotation(UI_Model_Product product)
        {
            if (IsInQuotation(product))
            {
                return Quotation[product];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get all products in the quotation.
        /// </summary>
        /// <returns></returns>
        public List<UI_Model_Product> GetProductsInQuotation()
        {
            List<UI_Model_Product> products = new List<UI_Model_Product>();
            foreach (KeyValuePair<UI_Model_Product, int> pair in Quotation)
            {
                products.Add(pair.Key);
            }
            return products;
        }

        /// <summary>
        /// Add an object to the shopping cart. Optional: the amount of the product.
        /// </summary>
        public void AddToCart(UI_Model_Product product, Action<bool> callback, int amount = 1)
        {
            if (amount <= 0)
            {
                return;
            }

            if (ShoppingCart.Cart.ContainsKey(product.ID))
            {
                var entry = ShoppingCart.Cart[product.ID];
                var newAmount = entry.Amount + amount;

                SetAmountInCart(product, callback, newAmount);
            }
            else
            {
                if(Analytics != null)
                {
                    Analytics.ProductAddedToShoppingCart(product);
                }

                backofficeApiClient.AddBasketEntry(
                    new ProductEntryPostObject
                    {
                        ProductId = product.ID,
                        Amount = amount,
                        Options = product.Options.Where(o => o.Choice >= 0 && o.Choice < o.Options.Count).Select(
                            o => new ProductEntryPostObject.ProductEntryOptions
                            {
                                OptionId = o.ID,
                                OptionValue = o.Options[o.Choice].ID
                            }).ToList()
                    },
                basket =>
                {
                    ShoppingCart.Cart = basket.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId);
                    callback(true);
                    RefreshUI();
                },
                (code, msg) =>
                {
                    Debug.LogWarningFormat("Could not add to cart: {0} {1}", code, msg);
                    callback(false);
                });
            }
        }

        /// <summary>
        /// Add an object to the quotation. Optional: the amount of the product.
        /// </summary>
        public void AddToQuotation(UI_Model_Product product, int amount = 1)
        {
            //Set the choice to 0 in case the product gets added without selection.
            foreach(UI_Model_ProductOption option in product.Options)
            {
                if (option.Choice == -1 && option.Required)
                    option.Choice = 0;
            }
            if (amount <= 0)
            {
                amount = 1;
            }
            if (Quotation.ContainsKey(product))
            {
                Quotation[product] += amount;
            }
            else
            {
                Quotation.Add(product, amount);
                Analytics.ProductAddedToQuotation(product);
            }
        }

        /// <summary>
        /// Remove a product from the shopping cart. Optional: the amount of the product.
        /// </summary>
        public void RemoveFromCart(UI_Model_Product product, Action<bool> callback, int amount = 0)
        {
            if(ShoppingCart.Cart.ContainsKey(product.ID))
            {
                if (Analytics != null)
                {
                    Analytics.ProductRemovedFromShoppingCart(product);
                }
                
                int entryId = ShoppingCart.Cart[product.ID].Id;
                int currentAmount = ShoppingCart.Cart[product.ID].Amount;

                if (amount <= 0 || amount >= currentAmount)
                {
                    backofficeApiClient.DeleteBasketEntry(entryId, () => {
                        Debug.Log("Entry deleted");
                        RefreshUI();
                        //backofficeApiClient.GetBasket(basket => {
                        //    ShoppingCart = basket.Entries.ToDictionary(e => e.ProductId);
                        //    RefreshUI();
                        //    Debug.Log("Shopping cart updated");
                        //},
                        //(code, msg) => {
                        //    Debug.LogWarningFormat("Could not remove from cart: {0} {1}", code, msg);
                        //    callback(false);
                        //});

                        callback(true);
                    }, (code, msg) => {
                        Debug.LogWarningFormat("Could not remove from cart: {0} {1}", code, msg);
                        callback(false);
                    });
                }
                else
                {
                    var remaining = currentAmount - amount;
                    SetAmountInCart(product, callback, remaining);                    
                }
            }
        }

        /// <summary>
        /// Remove a product from the quotation. Optional: the amount of the product.
        /// </summary>
        public void RemoveFromQuotation(UI_Model_Product product, int amount = 0)
        {
            if (amount <= 0)
            {
                Quotation.Remove(product);
                Analytics.ProductRemovedFromQuotation(product);
            }
            else if (Quotation.ContainsKey(product))
            {
                if (amount >= Quotation[product])
                {
                    Quotation.Remove(product);
                    Analytics.ProductRemovedFromQuotation(product);
                }
                else
                {
                    Quotation[product] -= amount;
                }
            }
        }

        /// <summary>
        /// Change the amount of a product in the shopping cart. Set to 0 to remove from the shopping cart.
        /// </summary>
        public void SetAmountInCart(UI_Model_Product product, Action<bool> callback, int amount)
        {
            if (amount <= 0)
            {
                if(Analytics != null)
                {
                    Analytics.ProductRemovedFromShoppingCart(product);
                }
                
                RemoveFromCart(product, callback);
            }
            else
            {        
                if (ShoppingCart.Cart.ContainsKey(product.ID))
                {
                    int entryId = ShoppingCart.Cart[product.ID].Id;

                    backofficeApiClient.UpdateBasketEntry(entryId, 
                        new ProductEntryPostObject
                        {
                            ProductId = product.ID,
                            Amount = amount,
                            Options = product.Options.Where(o => o.Choice >= 0 && o.Choice < o.Options.Count).Select(
                                o => new Network.BackofficeApi.Models.ProductEntryPostObject.ProductEntryOptions
                                {
                                    OptionId = o.ID,
                                    OptionValue = o.Options[o.Choice].ID
                                }).ToList()
                        },
                        basket =>
                        {
                            ShoppingCart.Cart = basket.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId);
                            callback(true);
                            RefreshUI();
                        },
                        (code, msg) =>
                        {
                            Debug.LogWarningFormat("Could not add to cart: {0} {1}", code, msg);
                            callback(false);
                        });
                }
                else
                {
                    AddToCart(product, callback, amount);
                }
            }

            if (OnCartUpdated != null) {
                OnCartUpdated.Invoke();
            }

        }

        /// <summary>
        /// Change the amount of a product in the shopping cart. Set to 0 to remove from the shopping cart.
        /// </summary>
        public void SetAmountInQuotation(UI_Model_Product product, int amount)
        {
            if (amount <= 0)
            {
                if (Quotation.ContainsKey(product))
                {
                    Quotation.Remove(product);
                    Analytics.ProductRemovedFromQuotation(product);
                }
            }
            else
            {
                if (Quotation.ContainsKey(product))
                {
                    Quotation[product] = amount;
                }
                else
                {
                    Quotation.Add(product, amount);
                }
            }
        }

        /// <summary>
        /// Load the shopping cart using the back office API.
        /// </summary>
        public void LoadShoppingCart(Action<Dictionary<UI_Model_Product, int>> callback)
        {
            if (Test)
            {
                PlanAction(() => {
                    callback(Catalogue.Categories.SelectMany(c => c.GetAllProducts(true).Where(p => ShoppingCart.Cart.ContainsKey(p.ID))).ToDictionary(p => p, p => ShoppingCart.Cart[p.ID].Amount));
                }, FakeDelay);
            }
            else
            {
                backofficeApiClient.GetBasket((basket) =>
                {
                    ShoppingCart.Cart = basket.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId);
                    ShoppingCart.TotalPrice = basket.Total;
                    ShoppingCart.HasAdditionalCosts = basket.Entries.Exists(p => p.Options.Count > 0);
                    callback(ShoppingCart.Cart.ToDictionary(p => p.Value.ToUIModel(), p => p.Value.Amount));
                    //RefreshUI();
                }, (code, message) => Debug.LogWarningFormat("Could not load Shopping Cart: {0} {1}", code, message));
            }
        }

        /// <summary>
        /// Load the quotation using the back office API.
        /// </summary>
        public void LoadQuotation(Action<Dictionary<UI_Model_Product, int>> callback)
        {
            if (Test)
            {
                PlanAction(() => {
                    callback(Quotation);
                }, FakeDelay);
            }
            else
            {
                callback(Quotation);
            }
        }

        /// <summary>
        /// Send the quotation.
        /// </summary>
        public void SendQuotation(string senderName, string senderEmail, string comments, Action<bool> callback)
        {
        }
        
        /// <summary>
        /// Go to the shopping cart page on the site.
        /// </summary>
        public void SendShoppingCart()
        {
            GoToURL("account/basket");

        }

        /// <summary>
        /// Load the favorites (or wishlist) using the back office API.
        /// </summary>
        public void LoadFavorites(Action<List<UI_Model_Product>> callback)
        {
            if (Test)
            {
                PlanAction(() => {
                    callback(Catalogue.Categories.SelectMany(c => c.GetAllProducts(true).Where(p => Favorites.ContainsKey(p.ID))).ToList());
                }, FakeDelay);
            }
            else
            {
                backofficeApiClient.GetWishlist(wishlistId,
                    wishlist => {
                        Favorites = wishlist.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId, p => p.Id);
                        callback(wishlist.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).Select(e => e.ToUIModel()).ToList());
                    },
                    (code, message) => callback(new List<UI_Model_Product>()));
            }
        }

        /// <summary>
        /// Load the catalogue.
        /// </summary>
        public void LoadCatalogue(Action<UI_Model_Catalogue> callback)
        {
            if (Test)
            {
                testCatalogue = UI.Test.UI_Test_FakeData.GetNuBuitenCatalogue();
                PlanAction(() => {
                    callback(testCatalogue);
                }, FakeDelay);
            }
            else
            {
                contentDeliveryApiClient.GetComponentByCode(6, "mainmenu", component =>
                {
                    var catalogue = component.ToCatalogue();

                    if (Modules.UseNuBuitenAppFlow)
                    {
                        int categoryCount = catalogue.Categories.Count;

                        var categoryList = new UI_Model_Category[catalogue.Categories.Count];

                        for (int i = 0; i < catalogue.Categories.Count; i++)
                        {
                            int index = i;
                            LoadCategoryFilters(filters =>
                            {
                                --categoryCount;

                                categoryList[index] = filters;

                                if (categoryCount <= 0)
                                {
                                    var result = new UI_Model_Catalogue
                                    {
                                        ID = catalogue.ID,
                                        HeaderIcon = catalogue.HeaderIcon,
                                        Name = catalogue.Name,
                                        Categories = categoryList.Where(c => c != null && c.SubCategories.Any()).ToList()
                                    };


                                    callback(result);
                                }
                            }, catalogue.Categories[i].ID);
                        }
                    }
                    else
                    {
                        callback(catalogue);
                    }
                }, (code, message) =>
                {
                    Debug.LogWarningFormat("Could not load Catalogue: {0} {1}", code, message);
                    callback(null);
                });
            }
        }

        /// <summary>
        /// Load a category by id.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="categoryId"></param>
        public void LoadCategoryProducts(Action<List<UI_Model_Product>> callback, int categoryId)
        {
            if (Test)
            {
                if (testCatalogue == null)
                {
                    testCatalogue = UI.Test.UI_Test_FakeData.GetNuBuitenCatalogue();
                }
                foreach (UI_Model_Category category in testCatalogue.Categories)
                {
                    if (category.ID == categoryId)
                    {
                        PlanAction(() => {
                            callback(category.GetAllProducts(true));
                        }, FakeDelay);
                        return;
                    }
                }
                callback(null);
            }
            else
            {
                contentDeliveryApiClient.GetProductsByCategory(categoryId,
                    products => callback(products.Select(p => p.ToUIModel()).ToList()),
                    (code, message) => 
                    {
                        Debug.LogWarningFormat("Could not get products for categoryId {0}: {1} {2}", categoryId, code, message);
                        callback(null);
                    });
            }
        }

        /// <summary>
        /// Load the subcategories of category.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="categoryId"></param>
        public void LoadCategoryFilters(Action<UI_Model_Category> callback, int categoryId)
        {
            if (Test)
            {

                if (testCatalogue == null)
                {
                    testCatalogue = UI.Test.UI_Test_FakeData.GetNuBuitenCatalogue();
                }

                PlanAction(() => callback(testCatalogue.Categories.SingleOrDefault(c => c.ID == categoryId)), FakeDelay);
            }
            else
            {
                contentDeliveryApiClient.GetPage(categoryId,
                    page => callback(page.ToUIModel()),
                    (code, message) =>
                    {
                        Debug.LogWarningFormat("Could not get page for categoryId {0}: {1} {2}", categoryId, code, message);
                        callback(null);
                    });
            }
        }

        /// <summary>
        /// Load all products.
        /// </summary>
        /// <param name="callback"></param>
        public void LoadAllProducts(Action<List<UI_Model_Product>> callback)
        {
            if (Catalogue == null)
            {
                LoadCatalogue((catalogue) =>
                {
                    if (catalogue != null)
                    {
                        Catalogue = catalogue;
                        LoadAllProducts(Catalogue, callback);
                    }
                    else
                    {
                        callback(new List<UI_Model_Product>());
                    }
                });
            }
            else
            {
                LoadAllProducts(Catalogue, callback);
            }
        }

        /// <summary>
        /// Load all products in catalogue.
        /// </summary>
        /// <param name="catalogue"></param>
        /// <param name="callback"></param>
        public void LoadAllProducts(UI_Model_Catalogue catalogue, Action<List<UI_Model_Product>> callback)
        {
            List<UI_Model_Product> result = new List<UI_Model_Product>();
            foreach(UI_Model_Category category in catalogue.Categories)
            {
                List<UI_Model_Product> products = category.GetAllProducts(true);
                result.AddRange(products);
            }
            callback(result);
        }

        /// <summary>
        /// Load a product by id.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="productId"></param>
        public void LoadProduct(Action<UI_Model_Product> callback, int productId)
        {
            if (Test)
            {
                if (testCatalogue == null)
                {
                    testCatalogue = UI.Test.UI_Test_FakeData.GetNuBuitenCatalogue();
                }
                foreach (UI_Model_Category category in testCatalogue.Categories)
                {
                    foreach (UI_Model_Product product in category.GetAllProducts(true))
                    {
                        if (product.ID == productId)
                        {
                            PlanAction(() => {
                                callback(product);
                            }, FakeDelay);
                            return;
                        }
                    }
                }
                callback(null);
            }
            else
            {
                contentDeliveryApiClient.GetProductById(productId,
                    product => callback(product.ToUIModel(ShoppingCart.Cart.ContainsKey(product.Id) ? ShoppingCart.Cart[product.Id] : null)),
                    (code, message) =>
                    {
                        Debug.LogWarningFormat("Could not get product id {0}: {1} {2}", productId, code, message);
                        callback(null);
                    });
            }
        }

        public void PlanAction(Action action, float delay)
        {
            StartCoroutine(DelayAction(action, Time.time + delay));
        }

        private IEnumerator DelayAction(Action action, float time)
        {
            while(Time.time < time)
            {
                yield return null;
            }
            action();
        }

        private void FindWishlistId()
        {
            backofficeApiClient.GetWishlists(
                wishlists =>
                {
                    var wishlist = wishlists.Data.SingleOrDefault(w => w.Name == "AR Wishlist");
                    if (wishlist != null)
                    {
                        wishlistId = wishlist.Id;
                        Favorites = wishlist.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId, p => p.Id);
                    }
                    else
                    {
                        backofficeApiClient.CreateWishlist("AR Wishlist",
                            w =>
                            {
                                wishlistId = w.Id;
                                Favorites = w.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId, p => p.Id);
                            },
                            (code, msg) => Debug.LogWarningFormat("Could not find AR Wishlist: {0} {1}", code, msg));
                    }
                },
                (code, msg) => Debug.LogWarningFormat("Could not find AR Wishlist: {0} {1}", code, msg));
        }

        private IEnumerator RefreshWishlist()
        {
            while (true)
            {
                if (LoggedIn() && wishlistId != -1)
                {
                    backofficeApiClient.GetWishlist(wishlistId,
                        wishlist => Favorites = wishlist.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId, p => p.Id),
                        (code, message) => Debug.LogWarningFormat("Could not refresh wishlist: {0} {1}", code, message));
                }

                yield return new WaitForSeconds(60);
            }
        }

        private IEnumerator RefreshBasket()
        {
            while (true)
            {
                if (LoggedIn())
                {
                    backofficeApiClient.GetBasket(
                        basket => {
                            ShoppingCart.Cart = basket.Entries.GroupBy(p => p.ProductId).Select(g => g.First()).ToDictionary(p => p.ProductId);
                            ShoppingCart.HasAdditionalCosts = basket.Entries.Exists(p => p.Options.Count > 0);
                            ShoppingCart.TotalPrice = basket.Total;
                        },
                        (code, message) => Debug.LogWarningFormat("Could not refresh wishlist: {0} {1}", code, message));
                }

                yield return new WaitForSeconds(60);
            }
        }
    }
}
