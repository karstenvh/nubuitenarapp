﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.UI.Models;

namespace VROwl.UI
{
    public class UI_DeviceUnique : MonoBehaviour
    {
        public UI_Device Device;

        private void Awake()
        {
            if(UI_Manager.GetDeviceType() != Device)
            {
                Destroy(gameObject);
            }
        }
    }
}
