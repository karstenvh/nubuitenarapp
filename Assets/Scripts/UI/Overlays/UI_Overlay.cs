﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    /// <summary>
    /// This abstract class contains the basic setup for each overlay.
    /// </summary>
    public abstract class UI_Overlay : MonoBehaviour
    {
        // The overlay identifier, used to decide when this overlay is visible.
        protected abstract OverlayID Overlay { get; }
        public OverlayID ID { get { return Overlay; } }

        // If this overlay is active. The only info we need for custom page logic is if this page is shown or not.
        protected bool IsCurrentOverlay = false;

        protected UI_Manager UI;

        private bool? prevstate = null;
        private UI_Component[] components;


        /// <summary>
        /// Initiate is called when the UI is loaded.
        /// /// </summary>
        public void Initiate(UI_Manager manager)
        {
            components = GetComponentsInChildren<UI_Component>(true);
            if (components != null && components.Length > 0)
                foreach (UI_Component component in components)
                    component.Initiate();
            UI = manager;
            InitiateOverlay();
        }

        /// <summary>
        /// The abstract method to initiate a page. Allowing for custom initiation implementation for each page.
        /// </summary>
        public abstract void InitiateOverlay();

        /// <summary>
        /// Refresh this overlay. 
        /// </summary>
        /// <param name="state">The current state of the UI.</param>
        public void Refresh(OverlayID state)
        {
            bool newState = state == Overlay;
            //If this page is inactive and it already was inactive the page will not refresh. Unless it's at start up; that's where prevstate comes in.
            if (newState == IsCurrentOverlay && !newState && prevstate != null) return;
            prevstate = IsCurrentOverlay;
            IsCurrentOverlay = newState;
            gameObject.SetActive(IsCurrentOverlay);
            if (components != null && components.Length > 0 && IsCurrentOverlay)
            {
                foreach (UI_Component component in components)
                {
                    if (component != null)
                    {
                        component.Refresh();
                    }
                }
            }
            RefreshOverlay();
        }

        /// <summary>
        /// The abstract method to refresh a page. Allowing for custom refresh implementation for each page. Check the bool Active to see if the page is visible.
        /// </summary>
        public abstract void RefreshOverlay();
    }
}