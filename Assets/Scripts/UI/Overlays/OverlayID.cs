﻿namespace VROwl.UI
{
    /// <summary>
    /// The state identifier for the ui manager.
    /// </summary>
    public enum OverlayID
    {
        NONE, PRODUCTOVERLAY, LOGINOVERLAY
    }
}