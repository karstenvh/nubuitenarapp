﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI.Components;
using VROwl.UI.Models;
using VROwl.UI.Utils;

namespace VROwl.UI.Overlays
{
    public class UI_Overlay_Product : UI_Overlay
    {
        protected override OverlayID Overlay { get { return OverlayID.PRODUCTOVERLAY; } }
        [Header("UI Object references")]
        public Image ProductImage;
        public Image BrandImage;

        public Text TitleText;
        public Text DescriptionText;
        public Text DeliveryText;
        public Text PriceText;
        public Text PriceExclusiveText;
        public Text AdditionalCostsText;
        public Text OldPriceText;
        public Text DownloadText;

        public Button GoToSiteButton;
        public Button PlaceButton, DownloadButton, DeleteButton, AddToCartButton, AddToQuotationButton;
        public Button IncreaseCartButton, DecreaseCartButton, IncreaseQuotationButton, DecreaseQuotationButton;
        public GameObject FavoriteButton, UnfavoriteButton, FavoriteSpinner;
        public GameObject DownloadingContainer, ColorSelectionContainer;
        public GameObject NextImageButton, PreviousImageButton;
        public Button CloseOverlayButton;

        public UI_Component_ProductOptions Options;
        public UI_Component_ProductDescriptionLine DescriptionLine;
        public UI_Component_ProductSpecifications Specifications;

        [HideInInspector]
        public UI_Model_Product Product;

        public GameObject AddToQuotationPanel, AddToCartPanel, AmountQuotationPanel, AmountCartPanel;
        public InputField QuotationAmountInput, CartAmountInput;

        public GameObject Content;
        public GameObject LoadingContent;
        public GameObject LoadingFailedContent;

        public VerticalLayoutGroup layoutGroup;

        private int currentImage = 0;

        public override void InitiateOverlay()
        {
        }

        public override void RefreshOverlay()
        {
            UpdateCart();
            UpdateQuotation();
        }

        int checkInterval = 10;
        private void Update()
        {
            checkInterval--;
            if(checkInterval <= 0)
            {
                checkInterval = 10;
                RefreshAdditionalCost();
                RefreshOverlay();
            }
        }

        private IEnumerator ToggleLayoutGroup() {
            layoutGroup.enabled = false;
            yield return new WaitForEndOfFrame();
            layoutGroup.enabled = true;
            yield return null;
        }

        public void Fill(UI_Model_Product product)
        {
            if (product != null)
            {
                SetInteraction(true);
                Product = product;
                TitleText.text = Product.Name;
                DescriptionText.text = UI.Modules.UseNuBuitenAppFlow ? Utils.UI_Utils.ParseHTML(Product.Information) : Product.Information;
                if (string.IsNullOrEmpty(product.Information))
                {
                    DescriptionLine.gameObject.SetActive(false);
                }
                else
                {
                    DescriptionLine.gameObject.SetActive(true);
                    DescriptionLine.Open();
                }

                Specifications.Fill(product.Specifications);
                AddToQuotationPanel.SetActive(UI.IsQuotationVisible());
                AddToCartPanel.SetActive(UI.IsShoppingCartVisible());
                PriceText.text = "€ " + UI_Utils.FormatPriceFromString(Product.Price) + " incl. btw";
                if (string.IsNullOrEmpty(Product.PriceExclusive))
                {
                    PriceExclusiveText.gameObject.SetActive(false);
                    PriceExclusiveText.text = "€ " + 0 + " excl. btw";
                }
                else
                {
                    PriceExclusiveText.gameObject.SetActive(true);
                    PriceExclusiveText.text = "€ " + Product.PriceExclusive + " excl. btw";
                }
                OldPriceText.text = "€ " + UI_Utils.FormatPriceFromString(Product.OldPrice);
                OldPriceText.gameObject.SetActive(!string.IsNullOrEmpty(Product.OldPrice));
                DeliveryText.gameObject.SetActive(!string.IsNullOrEmpty(Product.DeliveryTime));
                DeliveryText.text = "Levertijd: " + Product.DeliveryTime;
                Options.Fill(Product);

                GoToSiteButton.gameObject.SetActive(!string.IsNullOrEmpty(Product.SiteURL));

                FavoriteButton.SetActive(UI.IsFavoriteVisible() && UI.IsFavorite(Product.ID));
                UnfavoriteButton.SetActive(UI.IsFavoriteVisible() && !UI.IsFavorite(Product.ID));
                FavoriteSpinner.SetActive(false);

                UI_Helper.Helper.LoadImage(ProductImage, Product.IconURL);
                UI_Helper.Helper.LoadImage(BrandImage, Product.BrandImageURL);

                currentImage = 0;
                NextImageButton.SetActive(product.ImageURLs != null && product.ImageURLs.Count > 1);
                PreviousImageButton.SetActive(product.ImageURLs != null && product.ImageURLs.Count > 1);
                RefreshImage();

                UpdateCart();
                UpdateQuotation();
                if (product.ARModelInfo != null || UI.IgnoreMissingModelInfo)
                {
                    DownloadText.text = "Downloaden";
                    SetDownloadState(UI_Manager.instance.GetDownloadState(product));
                }
                else
                {
                    DownloadingContainer.SetActive(false);
                    DownloadButton.gameObject.SetActive(false);
                    PlaceButton.gameObject.SetActive(false);
                    DeleteButton.gameObject.SetActive(false);
                }
                ColorSelectionContainer.SetActive(UI.Modules.HasColorCustomisation && product.Colors != null && product.Colors.Count > 0);
                RefreshAdditionalCost();

                StartCoroutine(ToggleLayoutGroup());
            }
        }

        public void ToggleFavorite() {
            if (Product != null) {
                SetFavorite(!UI_Manager.instance.IsFavorite(Product.ID));
            }
        }

        private void SetFavorite(bool favorite) {
            if (UI_Manager.instance.LoggedIn()) {
                FavoriteSpinner.SetActive(true);
                UnfavoriteButton.SetActive(false);
                FavoriteButton.SetActive(false);

                if (favorite) {
                    UI_Manager.instance.AddToFavorites(Product,
                        added => {
                            FavoriteButton.SetActive(added);
                            Debug.Log("Add to favorites");
                            FavoriteSpinner.SetActive(false);
                        });
                }
                else {
                    UI_Manager.instance.RemoveFromFavorites(Product,
                        removed => {
                            UnfavoriteButton.SetActive(removed);
                            Debug.Log("Removed from favorites");
                            FavoriteSpinner.SetActive(false);
                        });
                }
            }
            else {
                UnfavoriteButton.SetActive(false);
                FavoriteButton.SetActive(false);
            }
        }

        public void Loading()
        {
            Content.SetActive(false);
            LoadingContent.SetActive(true);
            LoadingFailedContent.SetActive(false);
        }

        public void DoneLoading()
        {
            Content.SetActive(true);
            LoadingContent.SetActive(false);
            LoadingFailedContent.SetActive(false);
        }

        public void FailedLoading()
        {
            Content.SetActive(false);
            LoadingContent.SetActive(false);
            LoadingFailedContent.SetActive(true);

        }

        public void SetInteraction(bool active)
        {
            QuotationAmountInput.interactable = active;
            CartAmountInput.interactable = active;
            IncreaseCartButton.interactable = active;
            DecreaseCartButton.interactable = active;
            IncreaseQuotationButton.interactable = active;
            DecreaseQuotationButton.interactable = active;
            AddToCartButton.interactable = active;
            AddToQuotationButton.interactable = active;
        }

        public void GoToSite()
        {
            if (Product != null && !string.IsNullOrEmpty(Product.SiteURL))
                UI.GoToURL(Product.SiteURL);
        }

        public void Place()
        {
            UI.PlaceProduct(Product);
        }

        public void Delete()
        {
            UI.OpenYesNoPopup(() => {
                UI.DeleteProductModel(Product, (result) =>
                {
                    Fill(Product);
                });
            },
            () => { },
            "Weet u zeker dat u dit model wilt verwijderen?"
            );            
        }

        public void Download()
        {
            SetDownloadState(UI_Model_ProductDownloadState.DOWNLOADING);
            UI.DownloadProductModel(Product, (result) => { SetDownloadState(result ? UI_Model_ProductDownloadState.DOWNLOADED : UI_Model_ProductDownloadState.FAILED); });
        }

        public void AddToCart()
        {
            if (Options.Check())
            {
                SetInteraction(false);
                UI.AddToCart(Product, (result) => { SetInteraction(true); RefreshOverlay(); });
                UpdateCart();
            }
        }

        public void AddToQuotation()
        {
            if (Options.Check())
            {
                UI.AddToQuotation(Product);
                UpdateQuotation();
            }
        }

        public void AmountQuotationChanged()
        {
            int amount = 0;
            if (int.TryParse(QuotationAmountInput.text, out amount))
            {
                UI.SetAmountInQuotation(Product, amount);
            }
            UpdateQuotation();
        }

        public void AmountCartChanged()
        {
            int amount = 0;
            if (int.TryParse(CartAmountInput.text, out amount))
            {
                SetInteraction(false);
                UI.SetAmountInCart(Product, (result) => { SetInteraction(true); RefreshOverlay(); }, amount);
            }
            UpdateCart();
        }

        public void IncreaseQuotation()
        {
            UI.AddToQuotation(Product);
            UpdateQuotation();
        }

        public void DecreaseQuotation()
        {
            UI.RemoveFromQuotation(Product, 1);
            UpdateQuotation();
        }

        public void IncreaseCart()
        {
            SetInteraction(false);
            UI.AddToCart(Product, (result) => { SetInteraction(true); RefreshOverlay(); });
            UpdateCart();
        }

        public void DecreaseCart()
        {
            SetInteraction(false);
            UI.RemoveFromCart(Product, (result) => { SetInteraction(true); RefreshOverlay(); }, 1);
            UpdateCart();
        }

        public void UpdateQuotation()
        {
            if (UI.IsInQuotation(Product))
            {
                QuotationAmountInput.text = UI.GetAmountInQuotation(Product) + "";
            }
            else
            {
                QuotationAmountInput.text = "0";
            }
            AddToQuotationPanel.SetActive(UI.IsQuotationVisible());
            AddToQuotationButton.gameObject.SetActive(!UI.IsInQuotation(Product));
            AmountQuotationPanel.SetActive(UI.IsInQuotation(Product));
        }

        public void UpdateCart()
        {
            if (UI.IsInCart(Product))
            {
                CartAmountInput.text = UI.GetAmountInCart(Product) + "";
            }
            else
            {
                CartAmountInput.text = "0";
            }
            AddToCartPanel.SetActive(UI.IsShoppingCartVisible());
            AddToCartButton.gameObject.SetActive(!UI.IsInCart(Product));
            AmountCartPanel.SetActive(UI.IsInCart(Product));
        }

        public void SetDownloadState(UI_Model_ProductDownloadState state)
        {
            if(state == UI_Model_ProductDownloadState.FAILED)
            {
                DownloadText.text = "Downloaden mislukt...";
            }
            else
            {
                DownloadText.text = "Downloaden";
            }
            DownloadingContainer.SetActive(state == UI_Model_ProductDownloadState.DOWNLOADING);
            DownloadButton.gameObject.SetActive(state != UI_Model_ProductDownloadState.DOWNLOADED && state != UI_Model_ProductDownloadState.DOWNLOADING);
            PlaceButton.gameObject.SetActive(state == UI_Model_ProductDownloadState.DOWNLOADED);
            DeleteButton.gameObject.SetActive(state == UI_Model_ProductDownloadState.DOWNLOADED && UI.IsModelInCache(Product));
        }

        public void NextImage()
        {
            currentImage++;
            if (currentImage >= Product.ImageURLs.Count)
                currentImage = 0;
            RefreshImage();
        }

        public void PreviousImage()
        {
            currentImage--;
            if (currentImage < 0)
                currentImage = Product.ImageURLs.Count - 1;
            RefreshImage();
        }

        private void RefreshImage()
        {
            UI_Helper.Helper.LoadImage(ProductImage, Product.ImageURLs[currentImage]);
        }

        public void OpenColorSelection()
        {
            UI.OpenColorSelectionPopup(Product.Colors);
        }

        private void RefreshAdditionalCost()
        {
            AdditionalCostsText.gameObject.SetActive(Product != null && Product.HasAdditionalPrice());
        }
    }
}