﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    [RequireComponent(typeof(CanvasScaler))]
    public class UI_Orientation : MonoBehaviour
    {
        private DeviceOrientation currentOrientation = DeviceOrientation.Unknown;

        void Update()
        {
            if (currentOrientation != Input.deviceOrientation)
            {
                if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                {
                    ChangeToLandscape();
                }
                else if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
                {
                    ChangeToPortrait();
                }
                currentOrientation = Input.deviceOrientation;
            }
        }

        private void ChangeToLandscape()
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
        }

        private void ChangeToPortrait()
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
        }
    }
}
