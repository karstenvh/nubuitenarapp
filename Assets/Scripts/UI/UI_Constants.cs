﻿namespace VROwl.UI
{
    public static class UI_Constants
    {
        public static string PanelPrefix = "pnl_";
        public static string ImagePrefix = "img_";
        public static string ButtonPrefix = "btn_";
        public static string TextPrefix = "txt_";
        public static string InputFieldPrefix = "inpt_";
        public static string DropdownPrefix = "drp_";
    }
}