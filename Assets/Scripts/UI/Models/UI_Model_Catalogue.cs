﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI.Models
{
    public class UI_Model_Catalogue : UI_Model
    {
        public string Name;
        public string HeaderIcon;

        public List<UI_Model_Category> Categories = new List<UI_Model_Category>();
    }
}
