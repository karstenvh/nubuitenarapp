﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UI_ColorOption
{
    public string Name;
    public List<string> MaterialNames = new List<string>();
    public bool MultiSelect;
    public List<UI_ColorSelection> Selection = new List<UI_ColorSelection>();

    public void ToggleSelection(UI_ColorSelection selection)
    {
        if (selection.Selected)
            Deselect(selection);
        else
            Select(selection);
    }

    public void Select(UI_ColorSelection selection)
    {
        if (MultiSelect)
        {
            selection.Selected = true;
        }
        else
        {
            foreach(UI_ColorSelection s in Selection)
            {
                s.Selected = false;
            }
            selection.Selected = true;
        }
    }

    public void Deselect(UI_ColorSelection selection)
    {
        selection.Selected = false;
    }

    public List<UI_ColorSelection> GetSelectedColors()
    {
        return (from s in Selection where s.Selected select s).ToList();
    }
}

public class UI_ColorSelection
{
    public string ColorName;
    public Color Color;
    public bool Selected;
}