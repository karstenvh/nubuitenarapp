﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VROwl.Network.BackofficeApi.Models;
using VROwl.UI.Utils;

public class UI_ShoppingCart {
    public string Price { get; private set; }
    public double TotalPrice
    {
        set
        {
            Price = UI_Utils.FormatPrice(value);
        }
    }

    public bool HasAdditionalCosts { get; set; }

    public bool IsEmpty
    {
        get
        {
            return !(Cart.Count > 0);
        }
    }

    public Dictionary<int, ProductEntryModel> Cart = new Dictionary<int, ProductEntryModel>();
}
