﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI.Models
{
    public class UI_Model_ProductOption : UI_Model
    {
        public string Name;
        public bool Required;
        public List<UI_Model_ProductOptionEntry> Options;
        public int Choice = -1;

        public bool HasAdditionalCost()
        {
            if(Options != null && Options.Count > 0)
            {
                if(Choice >= 0 && Choice < Options.Count)
                {
                    if(!string.IsNullOrEmpty(Options[Choice].Price))
                    {
                        string numbers = System.Text.RegularExpressions.Regex.Match(Options[Choice].Price, @"\d+").Value;
                        int price = 0;
                        if (Int32.TryParse(numbers, out price))
                            return price != 0;
                        else
                            return true;
                    }
                }
            }
            return false;
        }
    }

    public class UI_Model_ProductOptionEntry : UI_Model
    {
        public string Name;
        public string Price;
    }
}
