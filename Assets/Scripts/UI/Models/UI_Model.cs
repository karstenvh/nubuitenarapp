﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VROwl.UI
{
    public class UI_Model
    {
        public int ID;

        public override bool Equals(object obj)
        {
            var item = obj as UI_Model;

            if (item == null)
            {
                return false;
            }

            return ID.Equals(item.ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}
