﻿using System.Collections.Generic;

namespace VROwl.UI.Models
{
    [System.Serializable]
    public class UI_Model_Product : UI_Model
    {
        public class AR_Model_Info
        {
            public string FileName;
            public string Url;
            public string Hash;
        }

        public string Name;
        public string ModelNumber;
        public string Price;
        public string PriceExclusive = null;
        public string OldPrice;
        public string Brand;
        public string DeliveryTime;
        public string Information;
        private string _shortDescription;
        public string ShortDescription {
            get
            {
                if (string.IsNullOrEmpty(_shortDescription))
                    return Information;
                else
                    return _shortDescription;
            }
            set
            {
                _shortDescription = value;
            }
        }
        public string IconURL;
        public string ImageURL {
            get
            {
                if (ImageURLs != null && ImageURLs.Count > 0)
                {
                    string url = ImageURLs[0];
                    if (string.IsNullOrEmpty(url))
                        return IconURL;
                    else
                        return url;
                }
                else
                    return "";
            }
        }
        public List<string> ImageURLs = new List<string>();
        public string BrandImageURL;
        public string SiteURL;
        public UI_Model_ProductPromotion Promotion = UI_Model_ProductPromotion.None;
        public bool Downloaded;
        public List<UI_Model_ProductOption> Options = new List<UI_Model_ProductOption>();
        public Dictionary<string, string> Specifications = new Dictionary<string, string>();
        public AR_Model_Info ARModelInfo;
        public List<UI_ColorOption> Colors = new List<UI_ColorOption>();

        public bool HasColorOption()
        {
            return Colors != null && Colors.Count > 0;
        }

        public bool HasAdditionalPrice()
        {
            foreach(UI_Model_ProductOption option in Options)
            {
                if (option.HasAdditionalCost())
                    return true;
            }
            return false;
        }
    }

    public enum UI_Model_ProductPromotion
    {
        None, Action, Recommended,
    }
}
