﻿using System.Collections.Generic;

namespace VROwl.UI.Models
{
    public class UI_Model_Category : UI_Model
    {
        public string Name;
        public string ImageURL;
        public List<UI_Model_Category> SubCategories = new List<UI_Model_Category>();
        public List<UI_Model_Product> Products = new List<UI_Model_Product>();

        public List<UI_Model_Product> GetAllProducts(bool includeSubCategories)
        {
            if (!includeSubCategories)
                return Products;
            else
            {
                List<UI_Model_Product> products = new List<UI_Model_Product>();
                if(Products != null)
                    products.AddRange(Products);
                if(SubCategories != null)
                    foreach (UI_Model_Category subcategory in SubCategories)
                        products.AddRange(subcategory.GetAllProducts(true));
                return products;
            }
        }
    }
}
