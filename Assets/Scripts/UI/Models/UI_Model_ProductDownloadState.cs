﻿namespace VROwl.UI.Models
{
    public enum UI_Model_ProductDownloadState
    {
        NONE, DOWNLOADING, DOWNLOADED, OUTDATED, FAILED
    }
}