﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    [RequireComponent(typeof(Text))]
    public class UI_Font : MonoBehaviour
    {
        [HideInInspector]
        public string font;

        public void Apply(UI_Stylesheet stylesheet)
        {
            Text text = GetComponent<Text>();
            if (text != null && stylesheet != null && !string.IsNullOrEmpty(font))
            {
                text.font = stylesheet.GetFont(font);
            }
        }
    }
}
