﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;

namespace VROwl.UI
{
    [CustomEditor(typeof(UI_Color)), CanEditMultipleObjects]
    public class UI_ColorEditor : Editor
    {
        public string[] options = UI_Stylesheet.GetColors().ToArray();
        public int index = 0;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (options == null || options.Length <= 0)
            {
                GUILayout.Label("Stylesheet has no available options!");
            }
            else
            {
                UI_Color Color = target as UI_Color;
                if(Color == null)
                {
                    return;
                }
                if (string.IsNullOrEmpty(Color.color))
                {
                    index = 0;
                    Color.color = "";
                }
                else
                {
                    for (int opt = 0; opt < options.Length; opt++)
                    {
                        string option = options[opt];
                        if (option.Equals(Color.color))
                        {
                            index = opt;
                            break;
                        }
                    }
                }
                UI_Stylesheet stylesheet = FindObjectOfType<UI_Stylesheet>();
                if (stylesheet != null)
                {
                    Color color = stylesheet.GetColor(Color.color);
                    GUI.backgroundColor = color;
                    //GUI.contentColor = color;
                }
                index = EditorGUILayout.Popup(index, options);
                Color.color = options[index];
                foreach (Object obj in targets)
                {
                    if (obj.GetType() == typeof(UI_Color))
                    {
                        UI_Color color = obj as UI_Color;
                        color.color = options[index];
                        EditorUtility.SetDirty(obj);
                    }
                }
                if(GUILayout.Button("Apply"))
                {
                    Color.Apply(stylesheet);
                    foreach (Object obj in targets)
                    {
                        if (obj.GetType() == typeof(UI_Color))
                        {
                            UI_Color color = obj as UI_Color;
                            color.Apply(stylesheet);
                            EditorUtility.SetDirty(obj);
                        }
                    }
                }
                EditorUtility.SetDirty(target);
            }
        }
    }
}
#endif