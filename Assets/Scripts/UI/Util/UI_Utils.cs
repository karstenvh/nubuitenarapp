﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections;
using System.Text.RegularExpressions;
using Assets.Scripts.Miscellaneous;
using UnityEngine;
using UnityEngine.Networking;

namespace VROwl.UI.Utils
{
    public static class UI_Utils
    {
        public static void DestoryChildrenOfParent(GameObject parent)
        {
            foreach (Transform child in parent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        private static LRUCache<string, Sprite> imageCache = new LRUCache<string, Sprite>(200);

        public static IEnumerator LoadImage(UnityEngine.UI.Image image, string url)
        {
            image.sprite = null;
            if (string.IsNullOrEmpty(url))
            {
                image.gameObject.SetActive(false);
                yield break;
            }

            Sprite sprite;
            if(imageCache.TryGet(url, out sprite))
            {
                image.sprite = sprite;
                image.gameObject.SetActive(true);
                yield break;
            }

            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url, true);
            yield return request.SendWebRequest();

            if (image == null || image.gameObject == null)
                yield break;

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogWarningFormat("An error occurred while requesting image at [{0}]\n\tError {1}: {2}", url, request.responseCode, request.error);
                image.gameObject.SetActive(false);
                yield break;
            }

            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            if (texture != null)
            {
                sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                image.gameObject.SetActive(true);
                imageCache.Add(url, sprite);
                image.sprite = sprite;
            }
            else
            {
                image.gameObject.SetActive(false);
            }
        }

        public static string FormatPrice(double price) {
            string formattedPrice = price.ToString("#,##0.00");
            formattedPrice = formattedPrice.Replace('.', '@');
            formattedPrice = formattedPrice.Replace(',', '.');
            formattedPrice = formattedPrice.Replace('@', ',');

            return formattedPrice;
        }

        public static string FormatPriceFromString(string price) {
            double nPrice;
            double.TryParse(price, out nPrice);
            if (!double.IsNaN(nPrice)) {
                return FormatPrice(nPrice);
            }
            return price;
        }

        public static string ParseHTML(string html)
        {
            if (string.IsNullOrEmpty(html))
                return "";
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = html.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\n",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\n• ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\n\n",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\n\n",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\n",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = Regex.Replace(result, @"\\n", string.Empty, RegexOptions.IgnoreCase);

                // Replace the strong tags with temporary bold tags
                result = Regex.Replace(result, @"<( )*strong([^>])*>", "}b{", RegexOptions.IgnoreCase);
                result = Regex.Replace(result, @"<( )*(/)( )*strong([^>])*>", "}/b{", RegexOptions.IgnoreCase);

                // Replace the em tags with temporary italic tags
                result = Regex.Replace(result, @"<( )*em([^>])*>", "}i{", RegexOptions.IgnoreCase);
                result = Regex.Replace(result, @"<( )*(/)( )*em([^>])*>", "}/i{", RegexOptions.IgnoreCase);

                result = Regex.Replace(result, @"<( )*(/)( )*li([^>])*>", string.Empty, RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Change the temporary tags with proper tags formatting }b{ to <b> etc
                result = Regex.Replace(result, @"}([^{]*){", "<$1>", RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                return result;
            }
            catch
            {
                return html;
            }
        }
    }
}