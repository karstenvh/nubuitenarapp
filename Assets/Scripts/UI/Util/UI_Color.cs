﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    [RequireComponent(typeof(Graphic))]
    public class UI_Color : MonoBehaviour
    {
        [HideInInspector]
        public string color;
        
        public void Apply(UI_Stylesheet stylesheet)
        {
            Graphic graphic = GetComponent<Graphic>();
            if (graphic != null && stylesheet != null && !string.IsNullOrEmpty(color))
            {
                graphic.color = stylesheet.GetColor(color);
            }
        }
    }
}
