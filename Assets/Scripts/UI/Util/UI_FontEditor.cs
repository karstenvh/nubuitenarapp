﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;

namespace VROwl.UI
{
    [CustomEditor(typeof(UI_Font)), CanEditMultipleObjects]
    public class UI_FontEditor : Editor
    {
        public string[] options = UI_Stylesheet.GetFonts().ToArray();
        public int index = 0;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (options == null || options.Length <= 0)
            {
                GUILayout.Label("Stylesheet has no available options!");
            }
            else
            {
                UI_Font Font = target as UI_Font;
                if (Font == null)
                {
                    return;
                }
                if (string.IsNullOrEmpty(Font.font))
                {
                    index = 0;
                    Font.font = "";
                }
                else
                {
                    for (int opt = 0; opt < options.Length; opt++)
                    {
                        string option = options[opt];
                        if (option.Equals(Font.font))
                        {
                            index = opt;
                            break;
                        }
                    }
                }
                UI_Stylesheet stylesheet = FindObjectOfType<UI_Stylesheet>();
                if (stylesheet != null)
                {
                    Font font = stylesheet.GetFont(Font.font);
                    EditorStyles.popup.font = font;
                }
                index = EditorGUILayout.Popup(index, options);
                Font.font = options[index];
                foreach (Object obj in targets)
                {
                    if (obj.GetType() == typeof(UI_Font))
                    {
                        UI_Font font = obj as UI_Font;
                        font.font= options[index];
                        EditorUtility.SetDirty(obj);
                    }
                }
                if (GUILayout.Button("Apply"))
                {
                    Font.Apply(stylesheet);
                    foreach (Object obj in targets)
                    {
                        if (obj.GetType() == typeof(UI_Font))
                        {
                            UI_Font font = obj as UI_Font;
                            font.Apply(stylesheet);
                            EditorUtility.SetDirty(obj);
                        }
                    }
                }
                EditorStyles.popup.font = null;
                EditorUtility.SetDirty(target);
            }
        }
    }
}
#endif