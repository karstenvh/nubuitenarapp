﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using VROwl.UI;
using VROwl.UI.Models;

public class ShoppingCartTotalPrice : MonoBehaviour {
    [SerializeField]
    private Text textPrice;
    [SerializeField]
    private GameObject textAdditionalPrice;
    [SerializeField]
    private Button btnCheckout;

    private string pricePreText;

    public void Init() {
        if (string.IsNullOrEmpty(pricePreText)) pricePreText = textPrice.text;

        bool hasAdditionalCosts = UI_Manager.instance.ShoppingCart.HasAdditionalCosts;
        textAdditionalPrice.SetActive(hasAdditionalCosts);

        textPrice.text = string.Format("{0}{1}{2}", pricePreText, UI_Manager.instance.ShoppingCart.Price, hasAdditionalCosts ? "*" : "");

        btnCheckout.interactable = !UI_Manager.instance.ShoppingCart.IsEmpty;
    }
}