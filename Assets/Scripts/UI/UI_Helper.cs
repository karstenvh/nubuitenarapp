﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VROwl.UI
{
    public class UI_Helper : MonoBehaviour
    {
        public static UI_Helper Helper;
        
        void Awake()
        {
            if (Helper == null)
                Helper = this;
            else if (Helper != this)
                Destroy(gameObject);

            //DontDestroyOnLoad(gameObject);
        }

        public void LoadImage(Image image, string url)
        {
            StartCoroutine(Utils.UI_Utils.LoadImage(image, url));
        }
    }
}