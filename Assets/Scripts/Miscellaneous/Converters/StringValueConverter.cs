﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FullSerializer;

namespace VROwl.Miscellaneous.Converters
{
    public class StringValueConverter : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return type == typeof(string);
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            if (data.IsString)
            {
                instance = data.AsString;
                return fsResult.Success;
            }
            else if (data.IsInt64)
            {
                instance = data.AsInt64.ToString();
                return fsResult.Success;
            }
            else if (data.IsDouble)
            {
                instance = data.AsDouble.ToString();
                return fsResult.Success;
            }
            else if (data.IsList)
            {
                instance = string.Join(" ", data.AsList.Select(d =>
                {
                    object output = null;
                    TryDeserialize(d, ref output, storageType);
                    return output;
                }).Cast<string>().Where(s => !string.IsNullOrEmpty(s)).ToArray());
                return fsResult.Success;
            }
            else
            {
                instance = data.ToString();
                return fsResult.Success;
            }
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            if (instance is string)
            {
                serialized = new fsData((string)instance);
                return fsResult.Success;
            }

            serialized = null;
            return fsResult.Fail("Object is not a string");
        }
    }
}
