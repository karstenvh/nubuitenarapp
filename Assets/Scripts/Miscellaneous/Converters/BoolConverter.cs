﻿using System;
using FullSerializer;

namespace VROwl.Miscellaneous.Converters
{
    public class BoolConverter : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return type == typeof(bool);
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            if (data.IsBool)
            {
                instance = data.AsBool;
                return fsResult.Success;
            }
            else if (data.IsInt64)
            {
                instance = data.AsInt64 != 0;
                return fsResult.Success;
            }

            return fsResult.Fail("Could not convert data to bool: " + data);
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            if (instance is bool)
            {
                serialized = new fsData((bool)instance);
                return fsResult.Success;
            }

            serialized = null;
            return fsResult.Fail("Object is not a bool");
        }
    }
}