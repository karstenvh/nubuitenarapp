﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FullSerializer;

namespace VROwl.Miscellaneous.Converters
{
    public class ProductVariationConverter : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return type == typeof(List<object>);
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            var result = fsResult.Success;

            if (data.IsList)
            {
                instance = data.AsList.Select(d => new object()).ToList();
            }
            else if (data.IsDictionary)
            {
                instance = data.AsDictionary.Values.Select(v => new object()).ToList();
            }
            else
            {
                result = fsResult.Fail("Could not deserialize " + data);
            }

            return result;
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            var list = (List<object>)instance;
            var result = fsResult.Success;           

            serialized = fsData.CreateList(list.Count);
            var serializedList = serialized.AsList;

            foreach (object item in list)
            {
                fsData itemData;

                // note: We don't fail the entire deserialization even if the
                //       item failed
                var itemResult = Serializer.TrySerialize(item, out itemData);
                result.AddMessages(itemResult);
                if (itemResult.Failed)
                    continue;

                serializedList.Add(itemData);
            }

            return result;
        }
    }
}
