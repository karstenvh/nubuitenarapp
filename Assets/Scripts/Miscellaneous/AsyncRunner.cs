﻿// copyright Uil VR Solutions B.V.

using UnityEngine;

namespace Assets.Scripts.Miscellaneous
{
    /// <summary>
    /// An empty <see cref="MonoBehaviour"/> used to run operations asynchronously.
    /// </summary>
    /// <seealso cref="UnityEngine.MonoBehaviour"/>
    public class AsyncRunner : MonoBehaviour
    {
        // Purposely left empty
    }
}