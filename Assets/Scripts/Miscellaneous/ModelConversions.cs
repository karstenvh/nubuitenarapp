﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using VROwl.Network.BackofficeApi.Models;
using VROwl.Network.ContentDeliveryApi.Models;
using VROwl.UI.Models;

namespace VROwl.Miscellaneous
{
    public static class ModelConversions
    {
        public static UI_Model_Catalogue ToCatalogue(this ComponentModel component)
        {
            return new UI_Model_Catalogue
            {
                Name = component.Name,
                Categories = component.MenuItems.OrderBy(m => m.SortOrder).Select(m => m.ToCatagory()).ToList()
            };
        }

        public static UI_Model_Category ToCatagory(this ComponentMenuItemModel menuItem)
        {
            return new UI_Model_Category
            {
                ID = menuItem.PageId,
                Name = menuItem.Name,
                ImageURL = menuItem.ImageUrl,
                SubCategories = menuItem.SubMenus.OrderBy(sub => sub.SortOrder).SelectMany(sub => sub.MenuItems).Select(m => m.ToCatagory()).ToList()
            };
        }

        public static UI_Model_Product ToUIModel(this ProductModel product, ProductEntryModel cartEntry)
        {
            var result = new UI_Model_Product
            {
                ID = product.Id,
                Name = product.Name,
                Information = product.Description,
                Price = product.Prices.Count() > 0 ? product.Prices[0] : null,
                PriceExclusive = product.PriceExclusive,
                OldPrice = product.Prices.Count() > 1 ? product.Prices[1] : null,
                Colors = product.ColorableMaterials != null ? GetColorOptions(product.ColorableMaterials) : new List<UI_ColorOption>(),
                Promotion = product.IsRecommended ? UI_Model_ProductPromotion.Recommended : product.SpecialPrice != null ? UI_Model_ProductPromotion.Action : UI_Model_ProductPromotion.None,
                IconURL = product.Media.Any() ? product.Media.First().GetPreviewUrl() : null,
                ImageURLs = product.Media.Any() ? ImageVersionModelsToURLS(product.Media) : null,
                Brand = product.Brand != null ? product.Brand.Name : null,
                BrandImageURL = product.Brand != null && product.Brand.Media.Any() ? product.Brand.Media.First().Source : null,
                SiteURL = product.Url,
                DeliveryTime = product.ShippingTime != null ? product.ShippingTime.Name : null,
                Options = product.Variables.Select(
                    v => new UI_Model_ProductOption
                    {
                        ID = v.Id,
                        Name = v.Name,
                        Required = v.Required,
                        Options = v.Options.Select(
                            o => new UI_Model_ProductOptionEntry
                            {
                                ID = o.Id,
                                Name = o.Name,
                                Price = o.Price
                            }).ToList()
                    }).ToList(),
                Specifications = product.Properties.Where(p => p.ShowInSpecifications).ToDictionary(p => p.Name, p => p.Value),
                ARModelInfo = GetARModelInfo(product.Files)
            };

            if(cartEntry != null)
            {
                foreach(var option in result.Options)
                {
                    var entryOption = cartEntry.Options.FirstOrDefault(o => o.OptionId == option.ID);
                    if(entryOption != null)
                    {
                        option.Choice = option.Options.FindIndex(o => o.ID == entryOption.OptionValue);
                    }
                }
            }

            return result;
        }

        public static UI_Model_Product.AR_Model_Info GetARModelInfo(System.Collections.Generic.List<MediaModel> files)
        {
            var model = files.FirstOrDefault(f => f.Metadata.FileName.EndsWith("gltf", System.StringComparison.InvariantCultureIgnoreCase));

            return model != null ? new UI_Model_Product.AR_Model_Info
            {
                FileName = model.Metadata.FileName,
                Url = model.Source,
                Hash = model.Hash
            } : null;
        }

        public static System.Collections.Generic.List<string> ImageVersionModelsToURLS(System.Collections.Generic.List<VersionedMediaModel<ImageVersionsModel>> images)
        {
            System.Collections.Generic.List<string> urls = new System.Collections.Generic.List<string>();
            foreach(VersionedMediaModel<ImageVersionsModel> image in images)
            {
                if (image.TypeId == 1)
                {
                    string url = image.GetImageUrl();
                    urls.Add(url);
                }
            }
            return urls;
        }

        public static UI_Model_Product ToUIModel(this ProductEntryModel product)
        {
            return new UI_Model_Product {
                ID = product.ProductId,
                Name = product.Product.Name,
                Price = string.IsNullOrEmpty(product.Product.SpecialPrice) ? product.Product.Price : product.Product.SpecialPrice,
                OldPrice = !string.IsNullOrEmpty(product.Product.SpecialPrice) ? product.Product.Price : null,
                Promotion = product.Product.IsRecommended ? UI_Model_ProductPromotion.Recommended : !string.IsNullOrEmpty(product.Product.SpecialPrice) ? UI_Model_ProductPromotion.Action : UI_Model_ProductPromotion.None,
                ShortDescription = product.Product.Description,
                IconURL = product.Product.Image == null ? product.Product.ImageUrl : product.Product.Image.src,
                SiteURL = product.Product.Url,
                Options = product.Options != null ? product.Options.Select(o => new UI_Model_ProductOption
                {
                    ID = o.OptionId,
                    Choice = 0,
                    Options = new System.Collections.Generic.List<UI_Model_ProductOptionEntry>
                    {
                        new UI_Model_ProductOptionEntry
                        {
                            ID = o.OptionValue
                        }
                    }
                }).ToList() : new System.Collections.Generic.List<UI_Model_ProductOption>()
            };
        }

        public static UI_Model_Category ToUIModel(this PageModel page)
        {
            return new UI_Model_Category
            {
                ID = page.Id,
                Name = page.Name,

                SubCategories = page.PlaceholderComponentsMain.SelectMany(c => c.Blocks).Select(b => b.ToUIModel()).ToList()
            };
        }

        private static UI_Model_Category ToUIModel(this BlockComponent block)
        {
            return new UI_Model_Category
            {
                ID = block.PageId,
                ImageURL = block.GetImageUrl()
            };
        }

        public static UI_Model_Product ToUIModel(this PageProductModel product)
        {
            return new UI_Model_Product
            {
                ID = product.Id,
                Name = product.Name,
                Price = product.GetPrice(),
                OldPrice = product.GetOldPrice(),
                Promotion = product.Recommended ? UI_Model_ProductPromotion.Recommended : product.SpecialPrice.HasValue ? UI_Model_ProductPromotion.Action : UI_Model_ProductPromotion.None,
                IconURL = product.PreviewImageUrl,
                SiteURL = product.Url,
                Information = product.GetShortDescription()
            };
        }

        private static string GetPrice(this PageProductModel product)
        {
            if (product.Prices.Count() > 0)
            {
                return product.Prices[0];
            }

            return product.Price;
        }

        private static string GetOldPrice(this PageProductModel product)
        {
            if (product.Prices.Count() > 1)
            {
                return product.Prices[1];
            }

            return null;
        }

        private static string GetShortDescription(this PageProductModel product)
        {
            var descriptionBuilder = new StringBuilder();

            foreach (var variation in product.Variations)
            {
                descriptionBuilder.AppendFormat("- {0} {1} beschikbaar", variation.Products.Count, variation.Name);
                descriptionBuilder.AppendLine();
            }

            return descriptionBuilder.ToString();
        }

        private static string GetImageUrl(this BlockComponent block)
        {
            if (!string.IsNullOrEmpty(block.Size) && block.Media.Versions != null)
            {
                var blockX1Key = string.Format("block_{0}", block.Size);
                if (block.Media.Versions.ContainsKey(blockX1Key))
                {
                    return block.Media.Versions[blockX1Key].Source;
                }
            }

            return block.Media.Source;
        }

        private static string GetImageUrl(this VersionedMediaModel<ImageVersionsModel> media)
        {
            if (media.Versions != null)
            {
                if (media.Versions.Preview2X != null && !string.IsNullOrEmpty(media.Versions.Preview2X.Source))
                {
                    return media.Versions.Preview2X.Source;
                }
                else if (media.Versions.Default != null && !string.IsNullOrEmpty(media.Versions.Default.Source))
                {
                    return media.Versions.Default.Source;
                }
            }

            return media.Source;
        }

        private static string GetPreviewUrl(this VersionedMediaModel<ImageVersionsModel> media)
        {
            if (media.Versions != null && media.Versions.Preview != null && !string.IsNullOrEmpty(media.Versions.Preview.Source))
            {
                return media.Versions.Preview.Source;
            }

            return media.GetImageUrl();
        }

        private static List<UI_ColorOption> GetColorOptions(List<ColorableMaterial> colorableMaterials)
        {
            List<UI_ColorOption> result = new List<UI_ColorOption>(colorableMaterials.Count);
            
            for (int i = 0; i < colorableMaterials.Count; i++)
            {
                // Parse colors
                List<UI_ColorSelection> selection = new List<UI_ColorSelection>();
                for (int j = 0; j < colorableMaterials[i].Colors.Count; j++)
                {
                    if (colorableMaterials[i].Colors[j] == null)
                    {
                        continue;
                    }

                    UnityEngine.Color c = UnityEngine.Color.blue;
                    UnityEngine.ColorUtility.TryParseHtmlString(colorableMaterials[i].Colors[j].Hex, out c);

                    selection.Add(new UI_ColorSelection()
                    {
                        ColorName = colorableMaterials[i].Colors[j].Name,
                        Color = c,
                        Selected = j == 0
                    });
                }

                if (selection.Count > 0)
                {
                    // Create container object.
                    result.Add(new UI_ColorOption()
                    {
                        Name = colorableMaterials[i].DisplayName,
                        MaterialNames = colorableMaterials[i].MaterialNames,
                        MultiSelect = colorableMaterials[i].Randomize,
                        Selection = selection
                    });
                }
            }

            return result;
        }
    }
}