﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSceneWithKey : MonoBehaviour {
    KeyCode key = KeyCode.R;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(key))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
	}
}
