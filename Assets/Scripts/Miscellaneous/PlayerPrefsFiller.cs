﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsFiller : MonoBehaviour
{
    public bool enabled;

    [SerializeField]
    private Pair[] pairs;

	private int currentIndex;

	void Awake ()
    {
        if (!enabled)
        {
            return;
        }
		currentIndex = 0;

        PlayerPrefs.DeleteAll();

        for (int i = 0; i < pairs.Length; i++)
        {
			for (int j = 0; j < pairs [i].productID.Count; j++) {
				PlayerPrefs.SetString (pairs [i].productID[j], pairs [i].fileName);
			}
        }
	}

	public string Pop()
	{
        if (!enabled)
        {
            return "";
        }

        string result = pairs [currentIndex].productID[0];

		currentIndex = (currentIndex + 1) % pairs.Length;

		return result;
	}

    [System.Serializable]
    private struct Pair
    {
        public string fileName;
        public List<string> productID;
    }
}
