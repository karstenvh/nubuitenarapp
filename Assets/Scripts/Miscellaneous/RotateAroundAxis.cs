﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundAxis : MonoBehaviour {
    public Vector3 axis = Vector3.up;
    public float speed = 120;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(axis, Time.deltaTime * speed);
	}
}
