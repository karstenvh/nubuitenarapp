﻿using System.Collections;

namespace VROwl.Miscellaneous
{
    /// <summary>
    /// Represents an action which can be started as a Unity coroutine.
    /// </summary>
    public interface ICoroutine
    {
        /// <summary>
        /// Starts the coroutine.
        /// </summary>
        IEnumerator Start();
    }
}