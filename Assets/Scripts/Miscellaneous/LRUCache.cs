﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;

namespace Assets.Scripts.Miscellaneous
{
    public class LRUCache<TKey, TValue>
    {
        private int capacity;

        private LinkedList<KeyValuePair<TKey, TValue>> cacheList;
        private Dictionary<TKey, LinkedListNode<KeyValuePair<TKey, TValue>>> cacheDictionary;

        public LRUCache(int capacity)
        {
            this.capacity = capacity;

            cacheList = new LinkedList<KeyValuePair<TKey, TValue>>();
            cacheDictionary = new Dictionary<TKey, LinkedListNode<KeyValuePair<TKey, TValue>>>(capacity);
        }

        public bool TryGet(TKey key, out TValue value)
        {
            if (cacheDictionary.ContainsKey(key))
            {
                LinkedListNode<KeyValuePair<TKey, TValue>> link = cacheDictionary[key];
                cacheList.Remove(link);
                cacheList.AddFirst(link);

                value = link.Value.Value;

                return true;
            }

            value = default(TValue);
            return false;
        }

        public void Add(TKey key, TValue value)
        {
            TValue oldValue;
            if(TryGet(key, out oldValue))
            {
                var link = cacheDictionary[key];
                cacheList.Remove(link);
                var newLink =  new LinkedListNode<KeyValuePair<TKey, TValue>>(new KeyValuePair<TKey, TValue>(key, value));
                cacheDictionary[key] = newLink;
                cacheList.AddFirst(newLink);
            }
            else
            {
                LinkedListNode<KeyValuePair<TKey, TValue>> link;
                if(cacheList.Count >= capacity)
                {
                    link = cacheList.Last;
                    cacheList.Remove(link);
                    cacheDictionary.Remove(link.Value.Key);
                }
                else
                {
                    link = new LinkedListNode<KeyValuePair<TKey, TValue>>(new KeyValuePair<TKey, TValue>(key, value));
                }

                cacheDictionary[key] = link;
                cacheList.AddFirst(link);
            }
        }
    }
}