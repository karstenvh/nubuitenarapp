﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour
{
    public static Globals instance;

    public Material mat_fakeShadow;

    public Material mat_rotateQuad;

    public Material mat_FenceHider;

    public Material dummyDoorMaterial;
    public Material dummyFrameMaterial;

    public string[] gltfs;

    public int currentGLTF;

    public DuplicateButton duplicationButtonPrefab;

    public ulong maxVertexCount = 2000000;

    public float selectedHoverHeight = 0.4f;

    public float maxPlacementRayDistance = 8;

    public float minimumDistanceInFrontOfCamera = 0.3f;

    public float rotateQuadOverflowFactor = 1.2f;

    public float rotateQuadYOffset = -0.3f;

    public Vector2 duplicateButtonOffset = new Vector2(0.4f, 1.5f);

    /// <summary>
    /// The layer(s) which contains world collision geometry.
    /// </summary>
    [SerializeField]
    private LayerMask planeLayer = 1 << 10;  //ARKitPlane layer

    /// <summary>
    /// The layer(s) which contains placed (and thus selectable) objects.
    /// </summary>
    [SerializeField]
    private LayerMask placeableLayer = 1 << 11;  //ARPlacable layer

    /// <summary>
    /// The layer(s) which contains planes to drag on.
    /// </summary>
    [SerializeField]
    private LayerMask dragLayer = 1 << 12;  //DragPlane layer

    /// <summary>
    /// The layer(s) which contains measuring tapes.
    /// </summary>
    [SerializeField]
    private LayerMask tapeLayer = 1 << 13;  //MeasuringTape layer

    /// <summary>
    /// The layer(s) which contains duplicate buttons.
    /// </summary>
    [SerializeField]
    private LayerMask duplicateButtonLayer = 1 << 14;  //DuplicateButton layer

    public Vector3 modelOrientationCompensation = Vector3.zero;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }
        else
        {
            instance = this;
        }
    }

    public bool HitTestFloorPlanes(Vector2 screenLocation, out RaycastHit hit)
    {
        return RaycastDirectOrProjectedMaxDistance(screenLocation, out hit, maxPlacementRayDistance, planeLayer);
    }

    public bool HitTestDragPlanes(Vector2 screenLocation, out RaycastHit hit)
    {
        return RaycastDirectOrProjectedMaxDistance(screenLocation, out hit, maxPlacementRayDistance, dragLayer);
    }

    public bool HitTestPlaceables(Vector2 screenLocation, out RaycastHit hit)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenLocation);

        return Physics.Raycast(ray, out hit, float.MaxValue, placeableLayer);
    }

    public bool HitTestMeasuringTapes(Vector2 screenLocation, out RaycastHit hit)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenLocation);

        return Physics.Raycast(ray, out hit, float.MaxValue, tapeLayer);
    }

    public bool HitTestMeasuringTapes_All(Vector2 screenLocation, out RaycastHit[] hits)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenLocation);

        hits = Physics.RaycastAll(ray, float.MaxValue, tapeLayer);

        return hits.Length > 0;
    }

    public bool HitTestDuplicateButtons(Vector2 screenLocation, out RaycastHit hit)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenLocation);

        return Physics.Raycast(ray, out hit, float.MaxValue, duplicateButtonLayer);
    }

    private bool RaycastDirectOrProjectedMaxDistance(Vector2 screenLocation, out RaycastHit hit, float maxDistance, LayerMask layer)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenLocation);

        // First try direct raycast.
        if (Physics.Raycast(ray, out hit, maxDistance, layer))
        {
            return true;
        }
        else
        {
            // Direct raycast did not hit, too far? Try raycast up/down to project the max distance.
            Vector3 projectionStart = ray.origin + ray.direction * maxDistance;

            // First try to cast down.
            Ray downRay = new Ray(projectionStart, Vector3.down);
            if (Physics.Raycast(downRay, out hit, float.MaxValue, layer))
            {
                return true;
            }
            else
            {
                // Otherwise cast up.
                Ray upRay = new Ray(projectionStart, Vector3.up);
                if (Physics.Raycast(upRay, out hit, float.MaxValue, layer))
                {
                    return true;
                }
            }
        }

        return false;
    }

}