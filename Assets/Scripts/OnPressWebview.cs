﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VoxelBusters.NativePlugins;
using VROwl.UI;

public class OnPressWebview : MonoBehaviour {
    [SerializeField] private WebView webView;
    [SerializeField] private string webURL = "http://www.google.com";
    [SerializeField] private string endURL = "http://www.google.com";

    [SerializeField] private string[] allowedUrl;

    public void OnPressOpenWebView() {
        if (!string.IsNullOrEmpty(webURL)) {
            webView.ClearCache();

            webView.LoadRequest(webURL);
            webView.SetFullScreenFrame();
        }
    }

    public void OnPressOpenForgotPassword(string url) {
        if (!string.IsNullOrEmpty(url)) {
            webView.ClearCache();

            webView.LoadRequest(url);
            webView.SetFullScreenFrame();
        }
    }


    private void OnEnable() {
        // Registering for event
        WebView.DidStartLoadEvent += OnDidStartLoadEvent;
        WebView.DidFinishLoadEvent += OnDidFinishLoadEvent;
        WebView.DidFailLoadWithErrorEvent += OnDidFailLoadWithErrorEvent;
        WebView.DidReceiveMessageEvent += OnDidReceiveMessageEvent;
    }

    private void OnDisable() {
        // Unregistering event
        WebView.DidStartLoadEvent -= OnDidStartLoadEvent;
        WebView.DidFinishLoadEvent -= OnDidFinishLoadEvent;
        WebView.DidFailLoadWithErrorEvent -= OnDidFailLoadWithErrorEvent;
        WebView.DidReceiveMessageEvent -= OnDidReceiveMessageEvent;
    }
    private void OnDidReceiveMessageEvent(WebView _webview, WebViewMessage _message) {
        if (this.webView == _webview) {
            Debug.Log("[WEBVIEW] Webview received message: " + _message.URL + "  -  " + _message.Arguments);
        }
    }

    private void OnDidStartLoadEvent(WebView _webview) {
        if (this.webView == _webview) {
            Debug.Log("[WEBVIEW] Webview did start loading request.");
        }
    }

    private void CheckOnPagePermissions(WebView _webview) {
        bool allowed = false;
        if (_webview.URL.Equals("https://nubuiten.nl/account/overview")) {
            webView.LoadRequest("https://nubuiten.nl/account/logout");
            allowed = true;

        }
        else if (endURL.Equals(_webview.URL)) {
            UI_Manager.instance.OpenOkPopup(null, "Registratie geslaagd, u kunt nu inloggen.");
        }
        else {
            foreach (string s in allowedUrl) {
                if (s.Equals(_webview.URL)) {
                    allowed = true;
                    break;
                }
            }
        }
        if (!allowed) {
            Debug.Log("[WEBVIEW] Illegal navigation!");
            webView.StopLoading();
            webView.Hide();
        }
    }

    private void OnDidFinishLoadEvent(WebView _webview) {
        webView.Show();
        if (this.webView == _webview) {
            Debug.Log("[WEBVIEW] Webview did finish loading request. At " + _webview.URL);
            CheckOnPagePermissions(_webview);
        }
    }
    private void OnDidFailLoadWithErrorEvent(WebView _webview, string _error) {
        if (this.webView == _webview) {
            Debug.Log("[WEBVIEW] Webview did fail with error: " + _error);
        }
    }

    
}
