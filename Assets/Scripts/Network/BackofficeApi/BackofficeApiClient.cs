﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VROwl.Network.BackofficeApi.Models;
using VROwl.Network.Security;

namespace VROwl.Network.BackofficeApi
{
    public class BackofficeApiClient : MonoBehaviour, IBackofficeApiClient
    {
        private class WishlistPostObject
        {
            public string name;
        }

        private class EntryPostObject
        {
            public int product_id;
            public int amount;
        }

        private class SingleSignInToken
        {
            public string token;
        }

        private static readonly string LOGIN_RESOURCE = "/customer/login";
        private static readonly string WISHLIST_RESOURCE = "/wishlist";
        private static readonly string BASKET_RESOURCE = "/basket";
        private static readonly string ENTRY_RESOURCE = "/entry";
        private static readonly string SINGLE_SIGN_IN_RESOURCE = "/single-sign-in";

        [SerializeField]
        private string backendApiEndpoint;

        [SerializeField]
        private RequestAuthorizer authorizer;

        public bool LoggedIn
        {
            get { return User != null; }
        }

        public UserModel User
        {
            get;  set;
        }

        public void LogIn(string username, string password, ApiCallback<UserModel> onLoginSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, LOGIN_RESOURCE);
            string loginData = string.Format("email={0}&password={1}", WWW.EscapeURL(username), WWW.EscapeURL(password));

            var request = RestRequest.Post<UserModelWrapper>(requestUri, loginData, new Dictionary<string, string> { { "Content-Type", "application/x-www-form-urlencoded" } });
            request.Completed += wrapper =>
            {
                User = wrapper.Customer;

                if (onLoginSuccess != null)
                {
                    onLoginSuccess(User);
                }
            };
            request.Failed += onError;

            StartCoroutine(request.Start());
        }

        public void LogOut()
        {
            User = null;
        }

        public void GetWishlists(ApiCallback<WishlistModelWrapper> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, WISHLIST_RESOURCE);

            StartCoroutine(AuthorizedGet(requestUri, new RestRequestCompleteHandler<WishlistModelWrapper>(onSuccess), onError));
        }

        public void GetWishlist(int wishlistId, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}/{2}", backendApiEndpoint, WISHLIST_RESOURCE, wishlistId);

            StartCoroutine(AuthorizedGet(requestUri, new RestRequestCompleteHandler<WishlistModel>(onSuccess), onError));
        }

        public void CreateWishlist(string name, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, WISHLIST_RESOURCE);
            var wishlist = new WishlistPostObject { name = name };

            StartCoroutine(AuthorizedPost(requestUri, wishlist, new RestRequestCompleteHandler<WishlistModel>(onSuccess), onError));
        }

        public void DeleteWishlist(int wishlistId, ApiCallback onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}/{2}", backendApiEndpoint, WISHLIST_RESOURCE, wishlistId);

            StartCoroutine(AuthorizedDelete(requestUri, new RestRequestCompleteHandler(onSuccess), onError));
        }

        public void UpdateWishlist(int wishlistId, string name, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}/{2}", backendApiEndpoint, WISHLIST_RESOURCE, wishlistId);
            var wishlist = new WishlistPostObject { name = name };

            StartCoroutine(AuthorizedPut(requestUri, wishlist, new RestRequestCompleteHandler<WishlistModel>(onSuccess), onError));
        }

        public void AddWishlistEntry(int wishlistId, int productId, int amount, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}/{2}", backendApiEndpoint, WISHLIST_RESOURCE, wishlistId);
            var wishlistEntry = new EntryPostObject { product_id = productId, amount = amount };

            StartCoroutine(AuthorizedPost<WishlistModel>(requestUri, wishlistEntry, wishlist => GetWishlist(wishlist.Id, new ApiCallback<WishlistModel>(onSuccess), onError), onError));
        }

        public void DeleteWishListEntry(int wishlistId, int entryId, ApiCallback onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}/{2}/{3}", backendApiEndpoint, WISHLIST_RESOURCE, wishlistId, entryId);

            StartCoroutine(AuthorizedDelete(requestUri, new RestRequestCompleteHandler(onSuccess), onError));
        }

        public void GetBasket(ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, BASKET_RESOURCE);

            StartCoroutine(AuthorizedGet(requestUri, new RestRequestCompleteHandler<BasketModel>(onSuccess), onError));
        }

        public void ClearBasket(ApiCallback onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, BASKET_RESOURCE);

            StartCoroutine(AuthorizedDelete(requestUri, new RestRequestCompleteHandler(onSuccess), onError));
        }

        public void AddBasketEntry(ProductEntryPostObject entry, ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}{2}", backendApiEndpoint, BASKET_RESOURCE, ENTRY_RESOURCE);

            StartCoroutine(AuthorizedPost(requestUri, entry, new RestRequestCompleteHandler<BasketModel>(onSuccess), onError));
        }

        public void UpdateBasketEntry(int entryId, ProductEntryPostObject entry, ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}{2}/{3}", backendApiEndpoint, BASKET_RESOURCE, ENTRY_RESOURCE, entryId);

            StartCoroutine(AuthorizedPut(requestUri, entry, new RestRequestCompleteHandler<BasketModel>(onSuccess), onError));
        }

        public void DeleteBasketEntry(int entryId, ApiCallback onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}{2}/{3}", backendApiEndpoint, BASKET_RESOURCE, ENTRY_RESOURCE, entryId);

            StartCoroutine(AuthorizedDelete(requestUri, new RestRequestCompleteHandler(onSuccess), onError));
        }

        public void GetSSOToken(ApiCallback<string> onSuccess, RequestFailedHandler onError)
        {
            string requestUri = string.Format("{0}{1}", backendApiEndpoint, SINGLE_SIGN_IN_RESOURCE);

            StartCoroutine(AuthorizedPost<SingleSignInToken>(requestUri, null, t => onSuccess(t.token), onError));
        }

        private IEnumerator AuthorizedGet<T>(string requestUri, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (LoggedIn)
            {
                var request = RestRequest.Get<T>(requestUri, GetAuthorizationHeaders());
                request.Completed += new RestRequestCompleteHandler<T>(onCompleted);
                request.Failed += onError;

                yield return request.Start();
            }
            else if (onError != null)
            {
                onError(401, "Not logged in");
                yield break;
            }
        }

        private IEnumerator AuthorizedPut<T>(string requestUri, object requestObject, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (LoggedIn)
            {
                var request = RestRequest.Put<T>(requestUri, requestObject, GetAuthorizationHeaders());
                request.Completed += new RestRequestCompleteHandler<T>(onCompleted);
                request.Failed += onError;

                yield return request.Start();
            }
            else if (onError != null)
            {
                onError(401, "Not logged in");
                yield break;
            }
        }

        private IEnumerator AuthorizedPost<T>(string requestUri, object requestObject, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (LoggedIn)
            {
                var request = RestRequest.Post<T>(requestUri, requestObject, GetAuthorizationHeaders());
                request.Completed += new RestRequestCompleteHandler<T>(onCompleted);
                request.Failed += onError;

                yield return request.Start();
            }
            else if (onError != null)
            {
                onError(401, "Not logged in");
                yield break;
            }
        }

        private IEnumerator AuthorizedDelete(string requestUri, RestRequestCompleteHandler onCompleted, RequestFailedHandler onError)
        {
            if (LoggedIn)
            {
                var request = UnityWebRequest.Delete(requestUri);
                foreach (var header in GetAuthorizationHeaders())
                {
                    request.SetRequestHeader(header.Key, header.Value);
                }

                yield return request.SendWebRequest();

                if (request.isHttpError || request.isNetworkError)
                {
                    if (onError != null)
                    {
                        onError((int)request.responseCode, request.error);
                    }
                }
                else
                {
                    if (onCompleted != null)
                    {
                        onCompleted();
                    }
                }
            }
            else if (onError != null)
            {
                onError(401, "Not logged in");
                yield break;
            }
        }

        private Dictionary<string, string> GetAuthorizationHeaders()
        {
            return new Dictionary<string, string>
            {
                { "pnct-id", User.Id.ToString() },
                { "pnct-token", User.Token }
            };
        }
    }
}