﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.BackofficeApi.Models
{
    public class BasketModel
    {
        [fsProperty("total")]
        public double Total
        {
            get; private set;
        }

        [fsProperty("discount")]
        public string Discount
        {
            get; private set;
        }

        [fsProperty("entries")]
        public List<ProductEntryModel> Entries
        {
            get; private set;
        }

        public BasketModel()
        {
            Entries = new List<ProductEntryModel>();
        }
    }
}