﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.BackofficeApi.Models
{
    public class WishlistModelWrapper
    {
        [fsProperty("data")]
        public List<WishlistModel> Data
        { get; private set; }

        public WishlistModelWrapper()
        {
            Data = new List<WishlistModel>();
        }
    }

    public class WishlistModel
    {
        [fsProperty("id")]
        public int Id
        {
            get; private set;
        }

        [fsProperty("name")]
        public string Name
        {
            get; private set;
        }

        [fsProperty("total")]
        public string Total
        {
            get; private set;
        }

        [fsProperty("discount")]
        public string Discount
        {
            get; private set;
        }

        [fsProperty("entries")]
        public List<ProductEntryModel> Entries
        {
            get; private set;
        }

        public WishlistModel()
        {
            Entries = new List<ProductEntryModel>();
        }
    }
}