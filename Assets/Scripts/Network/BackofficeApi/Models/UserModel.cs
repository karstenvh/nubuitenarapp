﻿// copyright Uil VR Solutions B.V.

using FullSerializer;

namespace VROwl.Network.BackofficeApi.Models
{
    public class UserModelWrapper
    {
        [fsProperty("customer")]
        public UserModel Customer
        {
            get; private set;
        }
    }

    public class UserModel
    {
        [fsProperty("firstname")]
        public string FirstName
        {
            get; set;
        }

        [fsProperty("lastname")]
        public string LastName
        {
            get; set;
        }

        [fsProperty("email")]
        public string Email
        {
            get; set;
        }

        [fsProperty("id")]
        public int Id
        {
            get; set;
        }
        public int id { get; internal set; }
        [fsProperty("token")]
        public string Token
        {
            get; set;
        }
    }

    [System.Serializable]
    public class UserModelPrefs {
        public string firstName;
        public string lastName;
        public string email;
        public int id;
        public string token;
    }
}