﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;
using VROwl.Miscellaneous.Converters;
using VROwl.Network.ContentDeliveryApi.Models;

namespace VROwl.Network.BackofficeApi.Models
{
    public class ProductEntryModel
    {
        public class ProductEntryOptions
        {
            [fsProperty("option_id")]
            public int OptionId
            { get; private set; }

            [fsProperty("option_value")]
            public int OptionValue
            { get; private set; }
        }

        public class ProductEntryDetails
        {
            [fsProperty("id")]
            public int Id
            { get; private set; }

            [fsProperty("specialprice")]
            public string SpecialPrice
            { get; private set; }

            [fsProperty("price")]
            public string Price
            { get; private set; }

            [fsProperty("name")]
            public string Name
            { get; private set; }

            [fsProperty("meta_description")]
            public string Description
            { get; private set; }

            [fsProperty("image")]
            public string ImageUrl
            { get; private set; }

            [fsProperty("featured_image")]
            public FeaturedImage Image
            { get; private set; }

            [fsProperty("url")]
            public string Url
            { get; private set; }

            [fsProperty("recommended", Converter = typeof(BoolConverter))]
            public bool IsRecommended
            { get; private set; }

            public class FeaturedImage {
                [fsProperty("id")]
                public int id;
                [fsProperty("src")]
                public string src;
            }
        }

        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("product_id")]
        public int ProductId
        { get; private set; }

        [fsProperty("amount")]
        public int Amount
        { get; private set; }

        [fsProperty("options")]
        public List<ProductEntryOptions> Options
        { get; private set; }

        [fsProperty("product")]
        public ProductEntryDetails Product
        { get; private set; }

        public ProductEntryModel()
        {
            Options = new List<ProductEntryOptions>();
        }
    }

    public class ProductEntryPostObject
    {
        public class ProductEntryOptions
        {
            [fsProperty("option_id")]
            public int OptionId
            { get; set; }

            [fsProperty("option_value")]
            public int OptionValue
            { get; set; }
        }

        [fsProperty("product_id")]
        public int ProductId
        { get; set; }

        [fsProperty("amount")]
        public int Amount
        { get; set; }

        [fsProperty("options")]
        public List<ProductEntryOptions> Options
        { get; set; }

        public ProductEntryPostObject()
        {
            Options = new List<ProductEntryOptions>();
        }
    }
}