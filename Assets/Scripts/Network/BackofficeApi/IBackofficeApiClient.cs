﻿using System.Collections.Generic;
using VROwl.Network.BackofficeApi.Models;

namespace VROwl.Network.BackofficeApi
{
    public interface IBackofficeApiClient
    {
        bool LoggedIn { get; }
        UserModel User { get; }

        void LogIn(string username, string password, ApiCallback<UserModel> onLoginSuccess, RequestFailedHandler onError);

        void LogOut();

        void GetWishlists(ApiCallback<WishlistModelWrapper> onSuccess, RequestFailedHandler onError);
        void GetWishlist(int wishlistId, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError);
        void CreateWishlist(string name, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError);
        void UpdateWishlist(int wishlistId, string name, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError);
        void DeleteWishlist(int wishlistId, ApiCallback onSuccess, RequestFailedHandler onError);

        void AddWishlistEntry(int wishlistId, int productId, int ammount, ApiCallback<WishlistModel> onSuccess, RequestFailedHandler onError);
        void DeleteWishListEntry(int wishlistId, int entryId, ApiCallback onSuccess, RequestFailedHandler onError);

        void GetBasket(ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError);

        void AddBasketEntry(ProductEntryPostObject entry, ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError);
        void UpdateBasketEntry(int entryId, ProductEntryPostObject entry, ApiCallback<BasketModel> onSuccess, RequestFailedHandler onError);
        void DeleteBasketEntry(int entryId, ApiCallback onSuccess, RequestFailedHandler onError);
    }
}