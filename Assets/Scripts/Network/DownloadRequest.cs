﻿namespace VROwl.Network
{
    /// <summary>
    /// Represents the parameters for a download request.
    /// </summary>
    public class DownloadRequest
    {
        /// <summary>
        /// Gets the download URL.
        /// </summary>
        /// <value>
        /// The download URL.
        /// </value>
        public string DownloadUrl
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the destination file path.
        /// </summary>
        /// <value>
        /// The destination file path.
        /// </value>
        public string DestinationFilePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadRequest" /> class.
        /// </summary>
        /// <param name="downloadUrl">The download URL.</param>
        /// <param name="destinationFilePath">The destination file path.</param>
        public DownloadRequest(string downloadUrl, string destinationFilePath)
        {
            this.DownloadUrl = downloadUrl;
            this.DestinationFilePath = destinationFilePath;
        }
    }
}