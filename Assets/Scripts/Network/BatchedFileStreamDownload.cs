﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Miscellaneous;
using UnityEngine;

namespace VROwl.Network
{
    /// <summary>
    /// The BatchedFileStreamDownload will download multiple files in batches, limiting the amount of
    /// downloads which can be active at the same time.
    /// </summary>
    /// <seealso cref="ROVR.Networking.IDownload"/>
    public class BatchedFileStreamDownload : IDownload
    {
        private static int DEFAULT_BATCH_SIZE = 10;

        private readonly AsyncRunner asyncRunner;
        private readonly List<DownloadRequest> requests;

        private readonly List<DownloadResult> completedDownloads;

        private readonly int batchSize;

        private readonly bool useDownloadParameters;
        private readonly int downloadBufferSize;
        private readonly int downloadRetryCount;

        private int requestIndex;
        private int completedRequests;

        /// <summary>
        /// Occurs when the download has completed.
        /// </summary>
        public event DownloadCompleteHandler DownloadCompleted;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchedFileStreamDownload"/> class.
        /// </summary>
        /// <param name="asyncRunner">The AsyncRunner used to asynchronously run the download.</param>
        /// <param name="requests">The requests.</param>
        public BatchedFileStreamDownload(AsyncRunner asyncRunner, List<DownloadRequest> requests)
            : this(asyncRunner, requests, DEFAULT_BATCH_SIZE)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchedFileStreamDownload"/> class.
        /// </summary>
        /// <param name="asyncRunner">The AsyncRunner used to asynchronously run the download.</param>
        /// <param name="requests">The requests.</param>
        /// <param name="batchSize">The amount of downloads in a batch.</param>
        public BatchedFileStreamDownload(AsyncRunner asyncRunner, List<DownloadRequest> requests, int batchSize)
        {
            this.asyncRunner = asyncRunner;
            this.requests = requests;
            this.completedDownloads = new List<DownloadResult>(requests.Count);

            this.batchSize = batchSize;

            this.requestIndex = 0;
            this.completedRequests = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchedFileStreamDownload"/> class.
        /// </summary>
        /// <param name="asyncRunner">The AsyncRunner used to asynchronously run the download.</param>
        /// <param name="requests">The requests.</param>
        /// <param name="batchSize">The amount of downloads in a batch.</param>
        /// <param name="downloadBufferSize">Size of the download buffer in bytes.</param>
        /// <param name="downloadRetryCount">The maximum amount of retries on a failed download.</param>
        public BatchedFileStreamDownload(AsyncRunner asyncRunner, List<DownloadRequest> requests, int batchSize, int downloadBufferSize, int downloadRetryCount)
            : this(asyncRunner, requests, batchSize)
        {
            this.useDownloadParameters = true;

            this.downloadBufferSize = downloadBufferSize;
            this.downloadRetryCount = downloadRetryCount;
        }

        /// <summary>
        /// Starts the download.
        /// </summary>
        /// <returns></returns>
        public IEnumerator Start()
        {
            int initialBatchSize = Math.Min(batchSize, requests.Count);
            Debug.LogFormat("Starting first batch of {0} downloads", initialBatchSize);

            for (; requestIndex < initialBatchSize; requestIndex++)
            {
                IDownload download;

                if (useDownloadParameters)
                {
                    download = new FileStreamDownload(requests[requestIndex], downloadBufferSize, downloadRetryCount);
                }
                else
                {
                    download = new FileStreamDownload(requests[requestIndex]);
                }

                download.DownloadCompleted += OnDownloadFinish;

                asyncRunner.StartCoroutine(download.Start());
            }

            yield break;
        }

        /// <summary>
        /// Callback called when a single download has completed. Will start the next download if
        /// there are still unprocessed requests in the queue or signal the DownloadCompleted event
        /// if all downloads have finished.
        /// </summary>
        /// <param name="completedDownloads">The completed downloads.</param>
        private void OnDownloadFinish(List<DownloadResult> completedDownloads)
        {
            this.completedDownloads.AddRange(completedDownloads);
            completedRequests++;

            if (requestIndex < requests.Count)
            {
                var download = new FileStreamDownload(requests[requestIndex++]);
                download.DownloadCompleted += OnDownloadFinish;

                asyncRunner.StartCoroutine(download.Start());
            }
            else if (completedRequests >= requests.Count)
            {
                if (DownloadCompleted != null)
                {
                    DownloadCompleted(this.completedDownloads);
                }
            }
        }
    }
}