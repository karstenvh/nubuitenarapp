﻿// copyright Uil VR Solutions B.V.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Miscellaneous;
using UnityEngine;

namespace VROwl.Network
{
    public delegate void DownloadCompleteHandler(List<DownloadResult> completedDownloads);

    /// <summary>
    /// This class manages downloads.
    /// </summary>
    public class DownloadService
    {
        private static readonly string BASE_DOWNLOAD_FOLDER = Path.Combine(Application.persistentDataPath, "cache");

        private readonly AsyncRunner asyncRunner;

        private readonly int batchSize;
        private readonly int maxRetries;
        private readonly int fixedDownloadBufferSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadService"/> class.
        /// </summary>
        /// <param name="settings">The settings for this <see cref="DownloadService"/>.</param>
        /// <param name="asyncRunner">
        /// The <see cref="AsyncRunner"/> instance used to run the downloads asynchronously.
        /// </param>
        public DownloadService(Settings settings, AsyncRunner asyncRunner)
        {
            this.batchSize = settings.BatchSize;
            this.maxRetries = settings.MaxRetries;
            this.fixedDownloadBufferSize = settings.FixedDownloadBufferSize;

            this.asyncRunner = asyncRunner;
        }

        /// <summary>
        /// Gets the path to the root of the downloads folder.
        /// </summary>
        public static string BaseDownloadsFolder
        {
            get
            {
                return BASE_DOWNLOAD_FOLDER;
            }
        }

        /// <summary>
        /// Downloads the specified resource. Any file which has already been downloaded will
        /// automatically be skipped.
        /// </summary>
        /// <param name="resource">The requested resource.</param>
        /// <param name="destinationPath">The destination path.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="forceDownload">if set to <c>true</c> [force download].</param>
        public void Download(string resource, string destinationPath, DownloadCompleteHandler handler, bool forceDownload = false)
        {
            Download(new[] { new DownloadRequest(resource, destinationPath) }, handler, forceDownload);
        }

        /// <summary>
        /// Downloads the specified resources. Any file which has already been downloaded will
        /// automatically be skipped.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="handler">The handler.</param>
        public void Download(IList<DownloadRequest> requests, DownloadCompleteHandler handler, bool forceDownload = false)
        {
            List<DownloadRequest> finishedRequests = new List<DownloadRequest>();
            List<DownloadRequest> newRequests;

            if (forceDownload)
            {
                newRequests = new List<DownloadRequest>(requests);
            }
            else
            {
                newRequests = new List<DownloadRequest>();

                foreach (DownloadRequest request in requests)
                {
                    if (File.Exists(request.DestinationFilePath))
                    {
                        finishedRequests.Add(request);
                        // The has already been downloaded before, no need to download it again.
                        Debug.LogFormat("File at [{0}] already exists. Skipping download.", request.DestinationFilePath);
                    }
                    else
                    {
                        newRequests.Add(request);
                    }
                }
            }

            CreateAndStartDownloads(newRequests, results =>
            {
                results.AddRange(finishedRequests.Select(r => new DownloadResult(r, 200)));

                if(handler != null)
                {
                    handler(results);
                }
            });
        }

        /// <summary>
        /// Downloads the specified resource from the service endpoint. Any file which has already
        /// been downloaded will automatically be skipped.
        /// </summary>
        /// <param name="serviceEndpoint">
        /// The service endpoint. This can be a REST service endpoint without a resource specified.
        /// </param>
        /// <param name="resource">The resource to download.</param>
        /// <param name="handler">Callback for when the download is complete.</param>
        public void Download(Uri serviceEndpoint, string resource, DownloadCompleteHandler handler)
        {
            string uri = new Uri(serviceEndpoint, resource).AbsoluteUri;
            string filePath = Path.Combine(BaseDownloadsFolder, resource);

            var request = new DownloadRequest(uri, filePath);

            Download(new[] { request }, handler);
        }

        /// <summary>
        /// Downloads the specified resources from the service endpoint. Any file which has already
        /// been downloaded will automatically be skipped.
        /// </summary>
        /// <param name="serviceEndpoint">The service endpoint.</param>
        /// <param name="resources">The resources.</param>
        /// <param name="handler">Callback for when the download is complete.</param>
        public void Download(Uri serviceEndpoint, IList<string> resources, DownloadCompleteHandler handler)
        {
            var requests = resources.Select(resource =>
            {
                string uri = new Uri(serviceEndpoint, resource).AbsoluteUri;
                string filePath = Path.Combine(BaseDownloadsFolder, resource);

                return new DownloadRequest(uri, filePath);
            }).ToList();

            Download(requests, handler);
        }

        /// <summary>
        /// Creates and starts the downloads.
        /// </summary>
        /// <param name="resourceToUrlMap">The resource to URL map.</param>
        /// <param name="handler">Callback for when the download is complete.</param>
        private void CreateAndStartDownloads(List<DownloadRequest> requests, DownloadCompleteHandler handler)
        {
            if (requests.Count > 0)
            {
                IDownload download = null;

                if (requests.Count == 1)
                {
                    download = new FileStreamDownload(requests[0], fixedDownloadBufferSize, maxRetries);
                }
                else if (requests.Count > 1)
                {
                    download = new BatchedFileStreamDownload(asyncRunner, requests, batchSize, fixedDownloadBufferSize, maxRetries);
                }

                download.DownloadCompleted += handler;
                asyncRunner.StartCoroutine(download.Start());
            }
            else if (handler != null)
            {
                handler(new List<DownloadResult>());
            }
        }

        /// <summary>
        /// Settings for the <see cref="DownloadService"/> class.
        /// </summary>
        [Serializable]
        public class Settings
        {
            [Tooltip("The maximum amount of concurrently active downloads in a batch.")]
            [SerializeField]
            private readonly int batchSize = 10;

            [Tooltip("The maximum amount of reattempts to perform before failing a download.")]
            [SerializeField]
            private readonly int maxRetries = 3;

            [Tooltip("The size in bytes of the fixed-size download buffer.")]
            [SerializeField]
            private readonly int fixedDownloadBufferSize = 64 * 1024;

            /// <summary>
            /// Gets the maximum amount of concurrently active downloads in a batch.
            /// </summary>
            /// <value>The maximum amount of concurrently active downloads in a batch..</value>
            public int BatchSize
            {
                get
                {
                    return batchSize;
                }
            }

            /// <summary>
            /// Gets the maximum amount of reattempts to perform before failing a download.
            /// </summary>
            /// <value>The maximum amount of reattempts to perform before failing a download.</value>
            public int MaxRetries
            {
                get
                {
                    return maxRetries;
                }
            }

            /// <summary>
            /// Gets the size in bytes of the fixed-size download buffer.
            /// </summary>
            /// <value>The size in bytes of the fixed-size download buffer.</value>
            public int FixedDownloadBufferSize
            {
                get
                {
                    return fixedDownloadBufferSize;
                }
            }
        }
    }
}