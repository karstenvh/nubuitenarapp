﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace VROwl.Network
{
    /// <summary>
    /// Downloads a file and streams it to disk. Uses a fixed-size buffer to retrieve data from the network.
    /// </summary>
    /// <seealso cref="ROVR.Networking.IDownload"/>
    public class FileStreamDownload : RetryingRequest, IDownload
    {
        private static readonly int DEFAULT_BUFFER_SIZE = 64 * 1024;
        private static readonly int DEFAULT_MAX_RETRY_COUNT = 3;

        private DownloadRequest downloadRequest;

        private byte[] buffer;

        /// <summary>
        /// Occurs when the download has completed.
        /// </summary>
        public event DownloadCompleteHandler DownloadCompleted;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStreamDownload"/> class using the
        /// default buffer size and max retry count.
        /// </summary>
        /// <param name="request">The request object containing the download parameters.</param>
        public FileStreamDownload(DownloadRequest request) : this(request, DEFAULT_BUFFER_SIZE, DEFAULT_MAX_RETRY_COUNT)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStreamDownload"/> class.
        /// </summary>
        /// <param name="downloadRequest">The request object containing the download parameters.</param>
        /// <param name="bufferSize">The size of the download buffer to use in bytes.</param>
        /// <param name="maxRetryCount">
        /// The maximum amount of retries which should be attempted if the download fails. 0 if
        /// downloads should not be retried.
        /// </param>
        public FileStreamDownload(DownloadRequest downloadRequest, int bufferSize, int maxRetryCount) : base(maxRetryCount)
        {
            this.downloadRequest = downloadRequest;

            this.buffer = new byte[bufferSize];
        }

        protected override void OnRequestCompleted()
        {
            var downloadResult = new DownloadResult(downloadRequest, Request.responseCode, HasError, Request.error);
            if (downloadResult.HasError && File.Exists(downloadRequest.DestinationFilePath))
            {
                // An error was encountered, but we still downloaded something. This is usually some
                // kind of error message or error page. Lets log the content and delete the file!
                string fileContent = File.ReadAllText(downloadRequest.DestinationFilePath);

                Debug.LogWarningFormat("Unexpected file downloaded:\n{0}", fileContent);

                File.Delete(downloadRequest.DestinationFilePath);
            }

            if (DownloadCompleted != null)
            {
                DownloadCompleted(new List<DownloadResult>() { downloadResult });
            }
        }

        protected override UnityWebRequest CreateRequestObject()
        {
            var webRequest = new UnityWebRequest(downloadRequest.DownloadUrl);
            webRequest.downloadHandler = new FileStreamDownloadHandler(downloadRequest.DestinationFilePath, buffer);

            return webRequest;
        }
    }
}