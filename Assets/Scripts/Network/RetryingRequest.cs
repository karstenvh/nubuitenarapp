﻿// copyright Uil VR Solutions B.V.

using System.Collections;
using UnityEngine;

namespace VROwl.Network
{
    /// <summary>
    /// An extension on the abstract BaseRequest class which will automatically retry a failed request.
    /// </summary>
    /// <seealso cref="ROVR.Networking.BaseRequest"/>
    public abstract class RetryingRequest : BaseRequest
    {
        private int maxRetryCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="RetryingRequest"/> class.
        /// </summary>
        /// <param name="maxRetryCount">The maximum retry count.</param>
        public RetryingRequest(int maxRetryCount)
        {
            this.maxRetryCount = maxRetryCount;
        }

        /// <summary>
        /// Starts this request as a coroutine.
        /// </summary>
        /// <returns>The IEnumerator representing the coroutine.</returns>
        public override IEnumerator Start()
        {
            yield return Request.SendWebRequest();

            int retryCount = 0;
            // Only retry on system and server errors. Client errors can not be fixed with a retry.
            while ((Request.isHttpError || Request.isNetworkError) && retryCount < maxRetryCount)
            {
                Debug.LogWarningFormat("Error while communicating with URI [{0}], retrying {1} more time(s). Reason: ResponseCode {2} Message [{3}]",
                    Request.url, (maxRetryCount - retryCount), Request.responseCode, Request.error);

                CreateAndSetRequest();

                yield return Request.SendWebRequest();

                retryCount++;
            }

            OnRequestCompleted();
        }
    }
}