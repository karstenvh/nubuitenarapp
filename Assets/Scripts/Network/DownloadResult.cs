﻿// copyright Uil VR Solutions B.V.

namespace VROwl.Network
{
    /// <summary>
    /// Represents the result of a download process.
    /// </summary>
    public class DownloadResult
    {
        /// <summary>
        /// Gets the original request.
        /// </summary>
        /// <value>The original request.</value>
        public DownloadRequest OriginalRequest
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public long StatusCode
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether an error has occurred.
        /// </summary>
        /// <value><c>true</c> if this instance has error; otherwise, <c>false</c>.</value>
        public bool HasError
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadResult"/> class.
        /// </summary>
        /// <param name="originalRequest">The original request.</param>
        /// <param name="statusCode">The status code.</param>
        /// <param name="hasError">Flag indicating if an error has occurred.</param>
        /// <param name="errorMessage">The error message or null.</param>
        public DownloadResult(DownloadRequest originalRequest, long statusCode, bool hasError = false, string errorMessage = null)
        {
            this.OriginalRequest = originalRequest;
            this.StatusCode = statusCode;
            this.HasError = hasError;
            this.ErrorMessage = errorMessage;
        }
    }
}