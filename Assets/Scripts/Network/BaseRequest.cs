﻿using System.Collections;
using UnityEngine.Networking;
using VROwl.Miscellaneous;

namespace VROwl.Network
{
    /// <summary>
    /// An abstract base class representing a web request. Wraps a UnityWebRequest object to carry out the actual request.
    /// </summary>
    /// <seealso cref="ICoroutine" />
    public abstract class BaseRequest : ICoroutine
    {
        private UnityWebRequest request;

        /// <summary>
        /// Gets a value indicating whether the underlying request encountered a system error or received a HTTP response code indicating an error..
        /// </summary>
        /// <value>
        ///   <c>true</c> if the underlying request has an error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError
        {
            get
            {
                if (request == null)
                {
                    return false;
                }

                return request.isNetworkError || request.isHttpError;
            }
        }

        /// <summary>
        /// Gets the underlying request object. A new one will be created using the CreateRequestObject method
        /// if no request object currently exists.
        /// </summary>
        /// <remarks>
        /// This property cannot be directly assigned. Overwrite the CreateRequestObject method to influence the
        /// creation of the request object.
        /// </remarks>
        /// <value>
        /// The underlying request object.
        /// </value>
        protected UnityWebRequest Request
        {
            get
            {
                if (request == null)
                {
                    request = CreateRequestObject();
                }

                return request;
            }
            private set
            {
                request = value;
            }
        }

        /// <summary>
        /// Starts this request as a coroutine.
        /// </summary>
        /// <returns>The IEnumerator representing the coroutine.</returns>
        public virtual IEnumerator Start()
        {
            yield return Request.SendWebRequest();

            OnRequestCompleted();
        }

        /// <summary>
        /// Callback, invoked when the underlying request has completed.
        /// </summary>
        protected abstract void OnRequestCompleted();

        /// <summary>
        /// Creates the request object. This method is used to create and recreate the underlying request object.
        /// </summary>
        /// <returns>The newly created request object.</returns>
        protected abstract UnityWebRequest CreateRequestObject();

        /// <summary>
        /// Recreates the underlying request object using the CreateRequestObject method.
        /// </summary>
        protected void CreateAndSetRequest()
        {
            Request = CreateRequestObject();
        }
    }
}