﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;
using VROwl.Miscellaneous.Converters;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class PageProductVariationsModel
    {
        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("products", Converter = typeof(ProductVariationConverter))]
        public List<object> Products
        { get; private set; }

        public PageProductVariationsModel()
        {
            Products = new List<object>();
        }
    }
}