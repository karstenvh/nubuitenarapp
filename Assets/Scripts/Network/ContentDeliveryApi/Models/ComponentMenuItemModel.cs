﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ComponentMenuItemModel
    {
        [fsProperty("id")]
        public int Id
        {
            get; private set;
        }

        [fsProperty("name")]
        public string Name
        {
            get; private set;
        }

        [fsProperty("link")]
        public string Link
        {
            get; private set;
        }

        [fsProperty("image_url")]
        public string ImageUrl
        {
            get; private set;
        }

        [fsProperty("collapsed")]
        public bool Collapsed
        {
            get; private set;
        }

        [fsProperty("sort_order")]
        public int SortOrder
        {
            get; private set;
        }

        [fsProperty("page_id")]
        public int PageId
        {
            get; private set;
        }

        [fsProperty("submenus")]
        public List<ComponentModel> SubMenus
        {
            get; private set;
        }
    }
}