﻿using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ProductModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("url")]
        public string Url
        { get; private set; }

        [fsProperty("description")]
        public string Description
        { get; private set; }

        [fsProperty("price")]
        public string Price
        { get; private set; }

        [fsProperty("price_exclusive")]
        public string PriceExclusive
        { get; private set; }

        [fsProperty("specialprice")]
        public string SpecialPrice
        { get; private set; }
        [fsProperty("prices")]
        public string[] Prices
        { get; private set; }

        [fsProperty("model_colors")]
        public List<ColorableMaterial> ColorableMaterials
        { get; private set; }

        [fsProperty("media")]
        public List<VersionedMediaModel<ImageVersionsModel>> Media
        { get; private set; }

        [fsProperty("properties")]
        public List<ProductPropertyModel> Properties
        { get; private set; }

        [fsProperty("variables")]
        public List<OptionsModel> Variables
        { get; private set; }

        [fsProperty("files")]
        public List<MediaModel> Files
        { get; private set; }

        [fsProperty("brand")]
        public BrandModel Brand
        { get; private set; }

        [fsProperty("shipping_time")]
        public ShippingTimeModel ShippingTime
        { get; private set; }

        [fsProperty("recommended")]
        public bool IsRecommended
        { get; private set; }
    }
}