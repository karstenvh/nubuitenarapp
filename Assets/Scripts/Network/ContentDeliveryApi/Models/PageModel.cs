﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class PageModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("title")]
        public string Title
        { get; private set; }

        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("placeholder_components_main")]
        public List<PlaceholderComponent> PlaceholderComponentsMain
        { get; private set; }
    }
}