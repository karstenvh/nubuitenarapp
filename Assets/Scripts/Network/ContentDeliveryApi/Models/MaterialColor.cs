﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;

public class MaterialColor
{
    [fsProperty("name")]
    public string Name
    { get; private set; }

    [fsProperty("hex")]
    public string Hex
    { get; private set; }
}
