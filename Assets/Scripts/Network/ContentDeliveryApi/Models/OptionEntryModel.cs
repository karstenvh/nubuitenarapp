﻿using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class OptionEntryModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("price")]
        public string Price
        { get; private set; }
    }
}