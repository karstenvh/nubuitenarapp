﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;

public class ColorableMaterial
{

    [fsProperty("display_name")]
    public string DisplayName
    { get; private set; }

    [fsProperty("material_names")]
    public List<string> MaterialNames
    { get; private set; }

    [fsProperty("colors")]
    public List<MaterialColor> Colors
    { get; private set; }

    [fsProperty("randomize")]
    public bool Randomize
    { get; private set; }
}
