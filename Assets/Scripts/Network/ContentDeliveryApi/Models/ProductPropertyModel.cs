﻿using FullSerializer;
using VROwl.Miscellaneous.Converters;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ProductPropertyModel
    {
        [fsProperty("name")]
        public string Name
        {
            get; private set;
        }

        [fsProperty("value", Converter = typeof(StringValueConverter))]
        public string Value
        {
            get; private set;
        }

        [fsProperty("show_in_specifications", Converter = typeof(BoolConverter))]
        public bool ShowInSpecifications
        {
            get; private set;
        }
    }
}