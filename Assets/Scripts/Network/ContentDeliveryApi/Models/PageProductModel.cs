﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class PageProductModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("price")]
        public string Price
        { get; private set; }

        [fsProperty("specialprice")]
        public float? SpecialPrice
        { get; private set; }

        [fsProperty("prices")]
        public string[] Prices
        { get; private set; }

        [fsProperty("url")]
        public string Url
        { get; private set; }

        [fsProperty("recommended")]
        public bool Recommended
        { get; private set; }

        [fsProperty("variations")]
        public List<PageProductVariationsModel> Variations
        { get; private set; }

        [fsProperty("preview_image")]
        public string PreviewImageUrl
        { get; private set; }
    }
}