﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class BlockComponent
    {
        [fsProperty("page_id")]
        public int PageId
        { get; private set; }

        [fsProperty("url")]
        public string Url
        { get; private set; }

        [fsProperty("size")]
        public string Size
        { get; private set; }

        [fsProperty("media")]
        public VersionedMediaModel<Dictionary<string,MediaModel>> Media
        { get; private set; }
    }
}