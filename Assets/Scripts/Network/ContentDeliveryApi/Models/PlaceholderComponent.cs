﻿// copyright Uil VR Solutions B.V.

using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class PlaceholderComponent
    {
        [fsProperty("blocks")]
        public List<BlockComponent> Blocks
        { get; private set; }
    }
}