﻿using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ImageVersionsModel
    {
        [fsProperty("preview")]
        public MediaModel Preview
        { get; private set; }

        [fsProperty("preview_2x")]
        public MediaModel Preview2X
        { get; private set; }

        [fsProperty("default")]
        public MediaModel Default
        { get; private set; }

        [fsProperty("thumbnail")]
        public MediaModel Thumbnail
        { get; private set; }
    }
}