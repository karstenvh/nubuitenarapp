﻿using System.Collections.Generic;
using FullSerializer;
using VROwl.Miscellaneous.Converters;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class OptionsModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("required", Converter = typeof(BoolConverter))]
        public bool Required
        { get; private set; }

        [fsProperty("options")]
        public List<OptionEntryModel> Options
        { get; private set; }
    }
}