﻿using System.Collections.Generic;
using FullSerializer;
using VROwl.Miscellaneous.Converters;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class BrandModel
    {
        [fsProperty("name")]
        public string Name
        { get; private set; }

        [fsProperty("media")]
        public List<VersionedMediaModel<List<MediaModel>>> Media
        { get; private set; }

        [fsProperty("show_in_overview", Converter = typeof(BoolConverter))]
        public bool ShowInOverview
        { get; private set; }
    }
}