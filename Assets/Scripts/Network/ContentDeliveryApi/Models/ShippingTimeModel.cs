﻿using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ShippingTimeModel
    {
        [fsProperty("name")]
        public string Name
        { get; private set; }
    }
}