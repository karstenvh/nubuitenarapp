﻿using System.Collections.Generic;
using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class ComponentModel
    {
        [fsProperty("id")]
        public int Id
        {
            get; private set;
        }

        [fsProperty("store_id")]
        public int StoreId
        {
            get; private set;
        }

        [fsProperty("page_id")]
        public int PageId
        {
            get; private set;
        }

        [fsProperty("name")]
        public string Name
        {
            get; private set;
        }

        [fsProperty("menuitems")]
        public List<ComponentMenuItemModel> MenuItems
        {
            get; private set;
        }

        [fsProperty("sort_order")]
        public int SortOrder
        {
            get; private set;
        }
    }
}