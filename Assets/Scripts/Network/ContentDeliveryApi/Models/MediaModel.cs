﻿using FullSerializer;
using VROwl.Miscellaneous.Converters;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class VersionedMediaModel<T> : MediaModel
    {
        [fsProperty("versions")]
        public T Versions
        { get; private set; }
    }

    public class MediaModel
    {
        [fsProperty("id")]
        public int Id
        { get; private set; }

        [fsProperty("metadata")]
        public MediaMetadataModel Metadata
        { get; private set; }

        [fsProperty("src")]
        public string Source
        { get; private set; }

        [fsProperty("type_id")]
        public int TypeId
        { get; private set; }

        [fsProperty("disabled", Converter = typeof(BoolConverter))]
        public bool Disabled
        { get; private set; }

        [fsProperty("hash")]
        public string Hash
        { get; private set; }
    }
}