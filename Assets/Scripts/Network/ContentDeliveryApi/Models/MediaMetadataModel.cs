﻿using FullSerializer;

namespace VROwl.Network.ContentDeliveryApi.Models
{
    public class MediaMetadataModel
    {
        [fsProperty("filename")]
        public string FileName
        { get; private set; }

        [fsProperty("mimeType")]
        public string MimeType
        { get; private set; }
    }
}