﻿// copyright Uil VR Solutions B.V.

using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace VROwl.Network
{
    /// <summary>
    /// A DownloadHandler which streams the downloaded data to a file.
    /// </summary>
    /// <seealso cref="UnityEngine.Networking.DownloadHandlerScript"/>
    public class FileStreamDownloadHandler : DownloadHandlerScript
    {
        private const string TEMP_SUFFIX = ".tmp";

        private FileStream _fileStream;

        private FileStream FileStream
        {
            get
            {
                // Lazily create the filestream in case a system error occurs and we don't need it.
                if (_fileStream == null)
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(FilePath));
                    _fileStream = new FileStream(TempFilePath, FileMode.Create, FileAccess.Write);
                }

                return _fileStream;
            }
        }

        /// <summary>
        /// Gets the path to the file where the completed downloaded data is stored.
        /// </summary>
        public string FilePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the path to the file where the data is stored during the download process.
        /// </summary>
        public string TempFilePath
        {
            get
            {
                return FilePath + TEMP_SUFFIX;
            }
        }

        /// <summary>
        /// Gets the 'Aborted' status.
        /// </summary>
        public bool Aborted
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the amount of expected bytes.
        /// </summary>
        public int ExpectedBytesCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the amount of received bytes.
        /// </summary>
        public int ReceivedBytesCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates a DownloadHandler which will stream the downloaded data to a file. Reuses a
        /// preallocated buffer to pass data to callbacks.
        /// </summary>
        /// <param name="filePath">
        /// A relative or absolute path to a file location where the data will be stored. Will create
        /// or replace the file.
        /// </param>
        /// <param name="preallocatedBuffer">
        /// A byte buffer into which data will be copied, for use by DownloadHandler.ReceiveData.
        /// </param>
        public FileStreamDownloadHandler(string filePath, byte[] preallocatedBuffer) : base(preallocatedBuffer)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Callback, invoked with a Content-Length header is received.
        /// </summary>
        /// <param name="contentLength">The value of the received Content-Length header.</param>
        protected override void ReceiveContentLength(int contentLength)
        {
            ExpectedBytesCount = contentLength;
        }

        /// <summary>
        /// Callback, invoked as data is received from the remote server.
        /// </summary>
        /// <param name="data">A buffer containing unprocessed data, received from the remote server.</param>
        /// <param name="dataLength">The number of bytes in data which are new.</param>
        /// <returns>True if the download should continue, false to abort.</returns>
        protected override bool ReceiveData(byte[] data, int dataLength)
        {
            if (!Aborted && dataLength > 0)
            {
                try
                {
                    ReceivedBytesCount += dataLength;
                    FileStream.Write(data, 0, dataLength);

                    return true;
                }
                catch
                {
                    // Could not write to file stream. Abort downloading and signal to abort the UnityWebRequest.
                    Debug.LogError("Failed to write data, aborting download!");
                    AbortDownload();
                    return false;
                }
            }

            AbortDownload();
            return false;
        }

        /// <summary>
        /// Callback, invoked when all data has been received from the remote server.
        /// </summary>
        protected override void CompleteContent()
        {
            FileStream.Close();

            try
            {
                if (ReceivedBytesCount > 0)
                {
                    // File is complete: move it to it's destination path.
                    Debug.LogFormat("Download complete - Moving from temp location [{0}] to storage location [{1}]", TempFilePath, FilePath);
                    File.Move(TempFilePath, FilePath);
                }
                else
                {
                    // No data was received, delete the temp file.
                    Debug.LogWarningFormat("Download complete - No data received! Deleting temp file [{0}]", TempFilePath);
                    File.Delete(TempFilePath);
                }
            }
            catch
            {
                // Could not move file
                Debug.LogWarningFormat("Could not move temp file at [{0}]", TempFilePath);
                throw;
            }
        }

        /// <summary>
        /// Callback, invoked when UnityWebRequest.downloadProgress is accessed.
        /// </summary>
        /// <returns>The return value for UnityWebRequest.downloadProgress.</returns>
        protected override float GetProgress()
        {
            if (ExpectedBytesCount > 0)
            {
                return (float)ReceivedBytesCount / ExpectedBytesCount;
            }

            return 0;
        }

        /// <summary>
        /// Aborts the download process. Removes received data from disk.
        /// </summary>
        public void AbortDownload()
        {
            if (!Aborted)
            {
                Debug.Log("Aborting download");

                Aborted = true;

                FileStream.Close();

                try
                {
                    File.Delete(TempFilePath);
                }
                catch
                {
                    // Could not delete partially downloaded file!
                    Debug.LogWarningFormat("Could not remove temp file at [{0}]", TempFilePath);
                    throw;
                }
            }
        }
    }
}