﻿using System;
using System.Collections.Generic;
using System.Text;
using FullSerializer;
using UnityEngine.Networking;

namespace VROwl.Network
{
    public delegate void ApiCallback<T>(T apiResult);
    public delegate void ApiCallback();

    public delegate void RestRequestCompleteHandler<T>(T result);
    public delegate void RestRequestCompleteHandler();

    public delegate void RequestFailedHandler(int statusCode, string errorMessage);

    /// <summary>
    /// Enumerates the different HTTP request types.
    /// </summary>
    public enum RequestType
    {
        /// <summary>
        /// An HTTP GET request.
        /// </summary>
        GET,

        /// <summary>
        /// An HTTP PUT request.
        /// </summary>
        PUT,

        /// <summary>
        /// An HTTP POST request.
        /// </summary>
        POST,

        /// <summary>
        /// An HTTP DELETE request.
        /// </summary>
        DELETE
    }

    /// <summary>
    /// Represents a REST request to a remote RESTful API.
    /// </summary>
    public static class RestRequest
    {
        private static readonly fsSerializer serializer = new fsSerializer();

        /// <summary>
        /// Creates a new RestRequest to get data from the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <returns></returns>
        public static RestRequest<T> Get<T>(string requestUri)
        {
            return Get<T>(requestUri, null);
        }

        /// <summary>
        /// Creates a new RestRequest to get data from the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="headers">The request headers.</param>
        /// <returns></returns>
        public static RestRequest<T> Get<T>(string requestUri, IDictionary<string, string> headers)
        {
            return new RestRequest<T>(requestUri, RequestType.GET, null, headers);
        }

        /// <summary>
        /// Creates a new RestRequest to put data at the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <returns></returns>
        public static RestRequest<T> Put<T>(string requestUri, string requestData)
        {
            return Put<T>(requestUri, requestData, null);
        }

        /// <summary>
        /// Creates a new RestRequest to put data at the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <param name="headers">The request headers.</param>
        /// <returns></returns>
        public static RestRequest<T> Put<T>(string requestUri, string requestData, IDictionary<string, string> headers)
        {
            return new RestRequest<T>(requestUri, RequestType.PUT, Encoding.UTF8.GetBytes(requestData), headers);
        }

        /// <summary>
        /// Creates a new RestRequest to put data at the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <param name="headers">The request headers.</param>
        /// <returns></returns>
        public static RestRequest<T> Put<T>(string requestUri, object requestData, IDictionary<string, string> headers)
        {
            if (headers == null)
            {
                headers = new Dictionary<string, string>();
            }

            headers.Add("Content-Type", "application/json");
            fsData data;
            serializer.TrySerialize(requestData.GetType(), requestData, out data).AssertSuccessWithoutWarnings();
            string dataJson = fsJsonPrinter.CompressedJson(data);

            return new RestRequest<T>(requestUri, RequestType.PUT, Encoding.UTF8.GetBytes(dataJson), headers);
        }

        /// <summary>
        /// Creates a new RestRequest to post data to the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <returns></returns>
        public static RestRequest<T> Post<T>(string requestUri, string requestData)
        {
            return Post<T>(requestUri, requestData, null);
        }

        /// <summary>
        /// Creates a new RestRequest to post data to the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <param name="headers">The headers.</param>
        /// <returns></returns>
        public static RestRequest<T> Post<T>(string requestUri, string requestData, IDictionary<string, string> headers)
        {
            return new RestRequest<T>(requestUri, RequestType.POST, Encoding.UTF8.GetBytes(requestData), headers);
        }

        /// <summary>
        /// Creates a new RestRequest to post data to the specified URI.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestData">The request data.</param>
        /// <param name="headers">The headers.</param>
        /// <returns></returns>
        public static RestRequest<T> Post<T>(string requestUri, object requestData, IDictionary<string, string> headers)
        {
            byte[] body = null;

            if (requestData != null)
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, string>();
                }

                headers.Add("Content-Type", "application/json");
                fsData data;
                serializer.TrySerialize(requestData.GetType(), requestData, out data).AssertSuccessWithoutWarnings();
                string dataJson = fsJsonPrinter.CompressedJson(data);
                body = Encoding.UTF8.GetBytes(dataJson);
            }

            return new RestRequest<T>(requestUri, RequestType.POST, body, headers);
        }
    }

    /// <summary>
    /// Represents a REST request to a remote RESTful API.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="ROVR.Networking.BaseRequest" />
    public class RestRequest<T> : BaseRequest
    {
        public event RestRequestCompleteHandler<T> Completed;

        public event RequestFailedHandler Failed;

        private static readonly fsSerializer serializer = new fsSerializer();

        private string requestUri;
        private RequestType requestType;
        private IDictionary<string, string> headers;
        private byte[] requestData;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestRequest{T}"/> class.
        /// </summary>
        /// <param name="requestUri">The request URI.</param>
        /// <param name="requestType">Type of the request.</param>
        /// <param name="requestData">The request data.</param>
        /// <param name="headers">The request headers.</param>
        public RestRequest(string requestUri, RequestType requestType, byte[] requestData, IDictionary<string, string> headers)
        {
            this.requestUri = requestUri;
            this.requestType = requestType;
            this.requestData = requestData;
            this.headers = headers;

            if(this.headers == null)
            {
                this.headers = new Dictionary<string, string>();
            }

            this.headers.Add("Accept", "application/json");
        }

        /// <summary>
        /// Callback, invoked when the underlying request has completed.
        /// </summary>
        protected override void OnRequestCompleted()
        {
            if (HasError)
            {
                OnFailed();
            }
            else
            {
                string receivedData = Request.downloadHandler.text;
                T result = ConvertReceivedData(receivedData);

                OnCompleted(result);
            }
        }

        /// <summary>
        /// Callback, invoked when data is received and needs to be converted to the desired type.
        /// </summary>
        /// <param name="receivedData">The received data.</param>
        /// <returns>The converted object.</returns>
        protected virtual T ConvertReceivedData(string receivedData)
        {
            T result = default(T);
            fsData data;
            if (fsJsonParser.Parse(receivedData, out data).Succeeded && serializer.TryDeserialize(data, ref result).Succeeded)
            {
                return result;
            }

            return default(T);
        }

        /// <summary>
        /// Called when [completed].
        /// </summary>
        /// <param name="result">The result.</param>
        protected virtual void OnCompleted(T result)
        {
            if (Completed != null)
            {
                Completed(result);
            }
        }

        /// <summary>
        /// Called when [failed].
        /// </summary>
        protected virtual void OnFailed()
        {
            if (Failed != null)
            {
                Failed((int)Request.responseCode, Request.downloadHandler.text ?? Request.error);
            }
        }

        /// <summary>
        /// Creates the request object. This method is used to create and recreate the underlying request object.
        /// </summary>
        /// <returns>
        /// The newly created request object.
        /// </returns>
        protected override UnityWebRequest CreateRequestObject()
        {
            UnityWebRequest request;

            switch (requestType)
            {
                case RequestType.GET:
                    request = UnityWebRequest.Get(requestUri);
                    break;

                case RequestType.PUT:
                    request = new UnityWebRequest(requestUri, UnityWebRequest.kHttpVerbPUT);
                    request.downloadHandler = new DownloadHandlerBuffer();
                    break;

                case RequestType.POST:
                    request = new UnityWebRequest(requestUri, UnityWebRequest.kHttpVerbPOST);
                    request.downloadHandler = new DownloadHandlerBuffer();
                    break;

                case RequestType.DELETE:
                    request = UnityWebRequest.Delete(requestUri);
                    break;

                default:
                    throw new InvalidOperationException("The provided web request type is unsupported: " + requestType);
            }

            if (requestData != null && requestData.Length > 0)
            {
                request.uploadHandler = new UploadHandlerRaw(requestData);
            }

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> kvp in headers)
                {
                    request.SetRequestHeader(kvp.Key, kvp.Value);
                }
            }

            return request;
        }
    }
}