﻿using FullSerializer;

namespace VROwl.Network.Security
{
    /// <summary>
    /// Represents the Authorization/Authentication token used for Access Management.
    /// </summary>
    public class AuthToken
    {
        /// <summary>
        /// Gets the type of the token.
        /// </summary>
        /// <value>
        /// The type of the token.
        /// </value>
        [fsProperty("token_type")]
        public string TokenType
        {
            get; private set;
        }

        /// <summary>
        /// Gets the amount of seconds this token was valid at the time of creation.
        /// </summary>
        /// <value>
        /// The amount of seconds this token was valid at the time of creation.
        /// </value>
        [fsProperty("expires_in")]
        public int ExpiresIn
        {
            get; private set;
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <value>
        /// The token used to get access to restricted resources.
        /// </value>
        [fsProperty("access_token")]
        public string AccessToken
        {
            get; private set;
        }

        /// <summary>
        /// Gets the refresh token.
        /// </summary>
        /// <value>
        /// The token used to renew this <see cref="AuthToken"/> without reauthenticating.
        /// </value>
        [fsProperty("refresh_token")]
        public string RefreshToken
        {
            get; private set;
        }
    }
}