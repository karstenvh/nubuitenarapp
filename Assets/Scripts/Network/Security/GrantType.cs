﻿namespace VROwl.Network.Security
{
    public enum GrantType
    {
        Password,
        ClientCredentials,
        Refresh
    }
}