﻿// copyright Uil VR Solutions B.V.

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace VROwl.Network.Security
{
    /// <summary>
    /// This service provides the base methods for authorizing client actions.
    /// </summary>
    public class BaseAuthorizationService : MonoBehaviour
    {
        [SerializeField]
        private string accessTokenUrl;

        [SerializeField]
        private OAuth2Parameters baseOAuth2Parameters;

        [SerializeField]
        private bool authorizeOnAwake;

        [SerializeField]
        private int retryAmount = 5;

        private Coroutine scheduledRefresh;

        /// <summary>
        /// Gets a value indicating whether a user is currently logged in.
        /// </summary>
        /// <value><c>true</c> if a user is logged in; otherwise, <c>false</c>.</value>
        public bool Authorized
        {
            get
            {
                return AuthToken != null;
            }
        }

        protected bool Authorizing { get; private set; }

        /// <summary>
        /// Gets the authentication token.
        /// </summary>
        /// <value>The authentication token.</value>
        protected AuthToken AuthToken
        {
            get; private set;
        }

        private void Awake()
        {
            if (authorizeOnAwake)
            {
                Authorize(
                    () => Debug.Log("Authorization successful!"),
                    (code, error) => Debug.LogWarningFormat("Could not authorize: {0}: {1}", code, error)
                );
            }
        }

        /// <summary>
        /// Starts the authorization process.
        /// </summary>
        /// <param name="onSuccess">This callback will be called if the authorization process succeeds.</param>
        /// <param name="onError">This callback will be called if the authorization process failles.</param>
        public void Authorize(RestRequestCompleteHandler onSuccess = null, RequestFailedHandler onError = null)
        {
            StartCoroutine(AuthorizeAsync(baseOAuth2Parameters, onSuccess, onError));
        }

        public void Authorize(string username, string password, RestRequestCompleteHandler onSuccess = null, RequestFailedHandler onError = null)
        {
            StartCoroutine(AuthorizeAsync(new OAuth2Parameters
            {
                grantType = GrantType.Password,
                clientId = baseOAuth2Parameters.clientId,
                clientSecret = baseOAuth2Parameters.clientSecret,
                username = username,
                password = password,
                scope = baseOAuth2Parameters.scope
            }, onSuccess, onError));
        }

        /// <summary>
        /// Resets authorization information.
        /// </summary>
        public void ResetAuthorization()
        {
            if (Authorized)
            {
                AuthToken = null;
            }
            else
            {
                // Already logged out, do nothing
            }
        }

        /// <summary>
        /// Starts the process to refresh the OAuth2 token without the need to reauthenticate.
        /// </summary>
        /// <param name="onSuccess">The on success.</param>
        /// <param name="onError">The on error.</param>
        private void RefreshToken(RestRequestCompleteHandler onSuccess = null, RequestFailedHandler onError = null)
        {
            StartCoroutine(RefreshTokenAsync(onSuccess, onError));
        }

        private IEnumerator AuthorizeAsync(OAuth2Parameters oAuth2Parameters, RestRequestCompleteHandler onSucces, RequestFailedHandler onError)
        {
            if (Authorizing)
            {
                yield return new WaitWhile(() => Authorizing);

                if (Authorized)
                {
                    if (onSucces != null)
                    {
                        onSucces();
                    }
                }
                else
                {
                    if (onError != null)
                    {
                        onError(401, "Not Authorized");
                    }
                }

                yield break;
            }

            Debug.Log("Authorizing...");

            StringBuilder requestBodyBuilder = new StringBuilder();

            switch (oAuth2Parameters.grantType)
            {
                case GrantType.Password:
                    requestBodyBuilder.Append("grant_type=password");
                    break;

                case GrantType.ClientCredentials:
                    requestBodyBuilder.Append("grant_type=client_credentials");
                    break;

                case GrantType.Refresh:
                    requestBodyBuilder.Append("grant_type=refresh_token");
                    break;

                default:
                    Debug.LogErrorFormat("Unrecognized grant type: {0}", oAuth2Parameters.grantType);
                    if (onError != null)
                    {
                        onError(400, "Unrecognized grant_type");
                    }

                    yield break;
            }

            if (!string.IsNullOrEmpty(oAuth2Parameters.clientId))
            {
                requestBodyBuilder.AppendFormat("&client_id={0}", oAuth2Parameters.clientId);
            }

            if (!string.IsNullOrEmpty(oAuth2Parameters.clientSecret))
            {
                requestBodyBuilder.AppendFormat("&client_secret={0}", oAuth2Parameters.clientSecret);
            }

            if (oAuth2Parameters.grantType == GrantType.Refresh)
            {
                if (AuthToken == null || string.IsNullOrEmpty(AuthToken.RefreshToken))
                {
                    if (onError != null)
                    {
                        onError(400, "No refresh token");
                    }
                    yield break;
                }

                requestBodyBuilder.AppendFormat("&refresh_token={0}", AuthToken.RefreshToken);
            }
            else
            {
                if (!string.IsNullOrEmpty(oAuth2Parameters.username))
                {
                    requestBodyBuilder.AppendFormat("&username={0}", oAuth2Parameters.username);
                }

                if (!string.IsNullOrEmpty(oAuth2Parameters.password))
                {
                    requestBodyBuilder.AppendFormat("&password={0}", oAuth2Parameters.password);
                }
            }

            requestBodyBuilder.AppendFormat("&scope={0}", oAuth2Parameters.scope);

            yield return RequestAuthToken(requestBodyBuilder.ToString(), onSucces, onError);
        }

        private IEnumerator RefreshTokenAsync(RestRequestCompleteHandler onSucces, RequestFailedHandler onError)
        {
            yield return AuthorizeAsync(new OAuth2Parameters
            {
                grantType = GrantType.Refresh,
                clientId = baseOAuth2Parameters.clientId,
                clientSecret = baseOAuth2Parameters.clientSecret,
                scope = baseOAuth2Parameters.scope
            }, onSucces, onError);
        }

        private IEnumerator RequestAuthToken(string accessRequestBody, RestRequestCompleteHandler onSucces, RequestFailedHandler onError)
        {
            Authorizing = true;

            int errorCode = 0;
            string errorMessage = string.Empty;

            int retryCount = retryAmount;
            while (!Authorized && retryCount --> 0)
            {
                var request = RestRequest.Post<AuthToken>(accessTokenUrl, accessRequestBody, new Dictionary<string, string> { { "Content-Type", "application/x-www-form-urlencoded" } });
                request.Failed += (code, msg) => { errorCode = code; errorMessage = msg; };

                request.Completed += token =>
                {
                    AuthToken = token;

                    if (scheduledRefresh != null)
                    {
                        StopCoroutine(scheduledRefresh);
                    }

                    scheduledRefresh = StartCoroutine(ScheduleRefresh(token.ExpiresIn));

                    OnTokenReceived();

                    if (onSucces != null)
                    {
                        onSucces();
                    }
                };

                yield return request.Start();

                if (!Authorized)
                {
                    int count = retryAmount - retryCount;

                    Debug.LogWarningFormat("Authorization attempt {0} failed!", count);
                    yield return new WaitForSeconds(2 * count);
                }
            }

            if (!Authorized)
            {
                onError(errorCode, errorMessage);
            }

            Authorizing = false;
        }

        private IEnumerator ScheduleRefresh(int expireInSeconds)
        {
            int expireTimeout = expireInSeconds - 10;
            if (expireTimeout < 0)
            {
                yield break;
            }

            yield return new WaitForSeconds(expireTimeout);

            yield return RefreshTokenAsync(null, null);
        }

        protected virtual void OnTokenReceived()
        {
            Debug.LogFormat("Token received. Valid for {0} seconds", AuthToken.ExpiresIn);
        }
    }
}