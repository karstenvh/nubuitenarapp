﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace VROwl.Network.Security
{
    public class RequestAuthorizer : BaseAuthorizationService
    {
        public IEnumerator AuthorizedGet<T>(string requestUri, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            return AuthorizedGet(requestUri, null, onCompleted, onError);
        }

        public IEnumerator AuthorizedGet<T>(string requestUri, IDictionary<string, string> headers, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (Authorizing)
            {
                yield return new WaitWhile(() => Authorizing);
            }

            if (Authorized)
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, string>();
                }

                headers.Add("Authorization", string.Format("{0} {1}", AuthToken.TokenType, AuthToken.AccessToken));

                var request = new RestRequest<T>(requestUri, RequestType.GET, null, headers);
                request.Completed += onCompleted;
                request.Failed += onError;

                yield return request.Start();
            }
            else
            {
                if (onError != null)
                {
                    onError(401, "Request was not authorized");
                }
            }
        }

        public IEnumerator AuthorizedPut<T>(string requestUri, string requestData, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            return AuthorizedPut(requestUri, requestData, null, onCompleted, onError);
        }

        public IEnumerator AuthorizedPut<T>(string requestUri, string requestData, IDictionary<string, string> headers, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (Authorizing)
            {
                yield return new WaitWhile(() => Authorizing);
            }

            if (Authorized)
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, string>();
                }

                headers.Add("Authorization", string.Format("{0} {1}", AuthToken.TokenType, AuthToken.AccessToken));

                var request = new RestRequest<T>(requestUri, RequestType.PUT, Encoding.UTF8.GetBytes(requestData), headers);
                request.Completed += onCompleted;
                request.Failed += onError;

                yield return request.Start();
            }
            else
            {
                if (onError != null)
                {
                    onError(401, "Request was not authorized");
                }
            }
        }

        public IEnumerator AuthorizedPost<T>(string requestUri, object requestObject, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            return AuthorizedPost(requestUri, requestObject, null, onCompleted, onError);
        }

        public IEnumerator AuthorizedPost<T>(string requestUri, object requestObject, IDictionary<string, string> headers, RestRequestCompleteHandler<T> onCompleted, RequestFailedHandler onError)
        {
            if (Authorizing)
            {
                yield return new WaitWhile(() => Authorizing);
            }

            if (Authorized)
            {
                if (headers == null)
                {
                    headers = new Dictionary<string, string>();
                }

                headers.Add("Authorization", string.Format("{0} {1}", AuthToken.TokenType, AuthToken.AccessToken));

                var request = RestRequest.Post<T>(requestUri, requestObject, headers);
                request.Completed += onCompleted;
                request.Failed += onError;

                yield return request.Start();
            }
            else
            {
                if (onError != null)
                {
                    onError(401, "Request was not authorized");
                }
            }
        }
    }
}