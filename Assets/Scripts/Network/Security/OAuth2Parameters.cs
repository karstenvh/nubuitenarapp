﻿using System;

namespace VROwl.Network.Security
{
    [Serializable]
    public class OAuth2Parameters
    {
        public GrantType grantType;
        public string clientId;
        public string clientSecret;
        public string username;
        public string password;
        public string scope;
    }
}