﻿// copyright Uil VR Solutions B.V.

using VROwl.Miscellaneous;

namespace VROwl.Network
{
    /// <summary>
    /// Represents a download process which can be started as a coroutine and which will raise an
    /// event when finished.
    /// </summary>
    public interface IDownload : ICoroutine
    {
        /// <summary>
        /// Occurs when the download has completed.
        /// </summary>
        event DownloadCompleteHandler DownloadCompleted;
    }
}