﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGLTF;

public class SetGLTFComponentToStreamingAssetPath : MonoBehaviour
{
    [SerializeField]
    private string file;

	void Start ()
    {
		if (GetComponent<GLTFComponent>() != null)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            GetComponent<GLTFComponent>().GLTFUri = Application.dataPath + "/StreamingAssets/" + file;
#elif UNITY_IOS
            GetComponent<GLTFComponent>().GLTFUri = Application.dataPath + "/Raw/" + file;
#endif
        }
	}
}
